import React from 'react';
import DashboardWidgets from './src/dashboard-widgets';
import DashboardWidget from './src/dashboard-widget';

export default {
  title: 'Components/Dashboard Widgets',
  component: DashboardWidgets,
};

export const Default: any = (args: any) => (
  <DashboardWidgets {...args}>
    <DashboardWidget label="Dashboard Widget" value="12" />
    <DashboardWidget color="primary" label="Dashboard Widget" value="12" />
    <DashboardWidget color="danger" label="Dashboard Widget" value="12" />
  </DashboardWidgets>
);

export const Two: any = (args: any) => (
  <DashboardWidgets {...args}>
    <DashboardWidget label="Dashboard Widget" value="12" />
    <DashboardWidget color="danger" label="Dashboard Widget" value="12" />
  </DashboardWidgets>
);

export const One: any = (args: any) => (
  <DashboardWidgets {...args}>
    <DashboardWidget label="Dashboard Widget" value="12" />
  </DashboardWidgets>
);
