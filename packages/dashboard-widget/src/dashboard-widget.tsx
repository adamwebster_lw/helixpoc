import React, { HtmlHTMLAttributes } from "react";
import { StyledDashboardWidget } from "./dashboard-widget.styles";

export type BadgeSizes = "medium" | "small" | "tiny";
export type BadgeStyles = "default" | "inverted";

export interface DashboardWidgetProps
  extends HtmlHTMLAttributes<HTMLDivElement> {
  label: string;
  value: string;
  color?: "default" | "primary" | "danger" | "warning" | "success";
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

export const DashboardWidget = ({
  label,
  value,
  color = "default",
  dataLwtId,
  className,
  ...rest
}: DashboardWidgetProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledDashboardWidget
      className={`helix-dashboard-widget ${className ? className : ""}`}
      color={color}
      {...optionalProps}
      {...rest}
    >
      <div className="dashboard-widget__value">{value}</div>
      <div className="dashboard-widget__label">{label}</div>
    </StyledDashboardWidget>
  );
};

export default DashboardWidget;
