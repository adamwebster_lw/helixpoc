import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledDashboardWidgets } from "./dashboard-widget.styles";

export interface DashboardWidgetsProps
  extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** if the widgets should not have a gap between them set this to true */
  fused?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

export const DashboardWidgets = ({
  children,
  fused,
  className,
  dataLwtId,
  ...rest
}: DashboardWidgetsProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledDashboardWidgets
      className={`helix-dashboard-widgets ${className ? className : ""}`}
      fused={fused}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledDashboardWidgets>
  );
};

export default DashboardWidgets;
