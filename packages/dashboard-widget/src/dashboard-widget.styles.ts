import styled, { css } from 'styled-components';

export interface SDWSProps {
  fused?: boolean;
}

export const StyledDashboardWidgets = styled.div<SDWSProps>`
  display: grid;
  gap: 8px;
  width: 100%;
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  @media (max-width: ${({ theme }) => theme.breakPoints!.small}) {
    grid-template-columns: 1fr;
    grid-template-rows: repeat(auto-fit, 1fr);
  }
  @media (max-width: ${({ theme }) => theme.breakPoints!.small}) {
    grid-template-columns: 1fr;
    grid-template-rows: repeat(auto-fit, 1fr);
  }

  ${({ fused }) =>
    fused &&
    css`
      gap: 0;
      @media (min-width: ${({ theme }) => theme.breakPoints!.small}) {
        .dashboard-widget:first-child {
          border-top-right-radius: 0;
          border-bottom-right-radius: 0;
        }
        .dashboard-widget:not(:first-child):not(:last-child):not(:only-child) {
          border-radius: 0;
          border-left: none;
          border-right: none;
        }
        .dashboard-widget:last-child {
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
        }
      }
      @media (max-width: ${({ theme }) => theme.breakPoints!.small}) {
        .dashboard-widget:first-child {
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .dashboard-widget:not(:first-child):not(:last-child):not(:only-child) {
          border-radius: 0;
          border-bottom: none;
          border-top: none;
        }
        .dashboard-widget:last-child {
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      }
    `}
`;

interface SDWProps {
  color: 'default' | 'primary' | 'danger' | 'warning' | 'success';
}

export const StyledDashboardWidget = styled.div<SDWProps>`
  display: flex;
  justify-content: center;
  flex-flow: column;
  align-items: center;
  background-color: ${({ theme }) => theme.dashboardWidget!.backgroundColor};
  border-radius: 6px;
  box-shadow: ${({ theme }) => theme.dashboardWidget!.boxShadow};
  padding: 16px;

  .dashboard-widget__label {
    font-size: 14px;
    color: ${({ theme }) => theme.dashboardWidget?.label!.textColor};
  }
  .dashboard-widget__value {
    font-size: 20px;
    color: ${({ theme, color }) => theme.dashboardWidget![color]?.value?.textColor};
    font-weight: 600;
    margin-bottom: 8px;
  }
`;
