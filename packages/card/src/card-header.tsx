import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardHeader } from "./card.styles";

interface CardHeaderProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardHeader = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardHeaderProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledCardHeader
      className={`lwt-helix-card-header ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardHeader>
  );
};

CardHeader.displayName = "CardHeader";

export default CardHeader;
