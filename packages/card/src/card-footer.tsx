import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardHeader } from "./card.styles";

interface CardFooterProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardFooter = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardFooterProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCardHeader
      className={`lwt-helix-card-header ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardHeader>
  );
};

CardFooter.displayName = "CardFooter";

export default CardFooter;
