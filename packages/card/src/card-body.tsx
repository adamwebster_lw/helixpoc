import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardBody } from "./card.styles";

interface CardBodyProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardBody = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardBodyProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCardBody
      className={`lwt-helix-card-body ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardBody>
  );
};

CardBody.displayName = "CardBody";

export default CardBody;
