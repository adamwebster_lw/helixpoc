import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardImage } from "./card.styles";

interface CardImageProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardImage = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardImageProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCardImage
      className={`lwt-helix-card-image ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardImage>
  );
};

CardImage.displayName = "CardImage";

export default CardImage;
