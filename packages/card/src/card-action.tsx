import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardAction } from "./card.styles";

interface CardActionProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardAction = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardActionProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCardAction
      className={`lwt-helix-card-action ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardAction>
  );
};

CardAction.displayName = "CardAction";

export default CardAction;
