import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCardActions } from "./card.styles";

interface CardActionsProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const CardActions = ({
  children,
  dataLwtId,
  className,
  ...rest
}: CardActionsProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCardActions
      className={`lwt-helix-card-actions ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCardActions>
  );
};

CardActions.displayName = "CardActions";

export default CardActions;
