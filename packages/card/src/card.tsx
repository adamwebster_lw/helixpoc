import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledCard } from "./card.styles";

interface CardProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  dataLwtId?: string;
}
const Card = ({ children, dataLwtId, className, ...rest }: CardProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledCard
      className={`lwt-helix-card ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledCard>
  );
};

Card.displayName = "Card";

export default Card;
