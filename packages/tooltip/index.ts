export {
  default as Tooltip,
  TooltipProps,
  TooltipRefProps,
} from "./src/tooltip";
