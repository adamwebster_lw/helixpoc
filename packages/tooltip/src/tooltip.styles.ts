import styled, { css } from "styled-components";

export const StyledArrow = styled.div`
  // css arrow
  position: absolute;
  width: 0;
  height: 0;
  &:before {
    content: "";
    position: absolute;
    border-style: solid;
    border-width: 6px 6px 6px 0;
    border-color: transparent ${({ theme }) => theme.tooltip?.backgroundColor}
      transparent transparent;
  }
`;

interface StyledTooltipProps {
  isOpening?: boolean;
}
export const StyledTooltip = styled.div<StyledTooltipProps>`
  background-color: ${({ theme }) => theme.tooltip?.backgroundColor};
  color: ${({ theme }) => theme.tooltip?.textColor};
  padding: 6px;
  border-radius: 4px;
  max-width: 300px;
  font-weight: 400;
  line-height: 16px;
  font-size: 12px;
  opacity: 0;
  transition: opacity 0.2s ease-in-out;
  ${({ isOpening }) =>
    isOpening &&
    css`
      opacity: 1;
    `}

  // arrow positioning
  &[data-popper-placement^="top"] > {
    ${StyledArrow} {
      bottom: 0px;
      &::before {
        border-width: 0 6px 6px 6px;
        left: -5px;
        border-color: transparent transparent
          ${({ theme }) => theme.tooltip?.backgroundColor} transparent;
        rotate: 180deg;
      }
    }
  }
  &[data-popper-placement^="bottom"] > {
    ${StyledArrow} {
      top: -6px;
      &::before {
        left: -5px;
        border-width: 0 6px 6px 6px;
        border-color: transparent transparent
          ${({ theme }) => theme.tooltip?.backgroundColor} transparent;
      }
    }
  }
  &[data-popper-placement^="left"] > {
    ${StyledArrow} {
      right: 0px;
      &::before {
        border-width: 6px 0 6px 6px;
        top: -5px;
        border-color: transparent transparent transparent
          ${({ theme }) => theme.tooltip?.backgroundColor};
      }
    }
  }
  &[data-popper-placement^="right"] > {
    ${StyledArrow} {
      left: -6px;
      &::before {
        border-width: 6px 6px 6px 0;
        top: -5px;
        border-color: transparent
          ${({ theme }) => theme.tooltip?.backgroundColor} transparent
          transparent;
      }
    }
  }
  // arrow visibility
  [data-popper-reference-hidden="true"] > {
    ${StyledArrow} {
      visibility: hidden;
    }
  }
`;
