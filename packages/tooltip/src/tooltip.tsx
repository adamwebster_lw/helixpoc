import React, {
  useState,
  ReactNode,
  useEffect,
  MutableRefObject,
  useImperativeHandle,
  forwardRef,
  useRef,
} from "react";
import { createPortal } from "react-dom";
import type { Placement, StrictModifiers } from "@popperjs/core";
import { usePopper } from "react-popper";
import { StyledArrow, StyledTooltip } from "./tooltip.styles";
import { DefaultTheme } from "styled-components";
export interface TooltipRefProps {
  api: {
    open: () => void;
    close: () => void;
    toggle: () => void;
  };
}

export interface TooltipProps {
  placement?: Placement;
  target: MutableRefObject<any> | HTMLElement | string;
  children: ReactNode;
  autoHide?: boolean;
  popperModifiers?: StrictModifiers[];
  theme?: DefaultTheme;
  popperOptions?: {
    strategy?: "absolute" | "fixed";
  };
  usePortal?: boolean;
}

const Popover = forwardRef<TooltipRefProps, TooltipProps>(
  (
    {
      placement = "auto",
      children,
      target,
      autoHide = true,
      popperModifiers,
      theme,
      popperOptions = {
        strategy: "absolute",
      },
      usePortal,
      ...rest
    },
    ref
  ) => {
    const [referenceElement, setReferenceElement] =
      useState<HTMLElement | null>(null);
    const [popperElement, setPopperElement] =
      useState<HTMLElement | null>(null);
    const [arrowElement, setArrowElement] = useState<HTMLElement | null>(null);
    const [open, setOpen] = useState(false);
    const [isOpening, setIsOpening] = useState(false);

    const tooltipHovered = useRef(false);

    const getPopperModifiers = () => {
      return popperModifiers && popperModifiers.length > 0
        ? popperModifiers
        : [];
    };
    const { styles, attributes } = usePopper(referenceElement, popperElement, {
      placement,
      ...popperOptions,
      modifiers: [
        { name: "offset", options: { offset: [0, 8] } },
        {
          name: "arrow",
          options: {
            element: arrowElement,
          },
        },
        ...getPopperModifiers(),
      ],
    });

    const handleMouseEnterPopover = () => {
      if (autoHide) return;
      tooltipHovered.current = true;
    };

    const handleMouseLeavePopover = () => {
      if (autoHide) return;
      setIsOpening(false);
      tooltipHovered.current = false;
      setTimeout(() => {
        setOpen(false);
      }, 200);
    };

    const handleMouseEnter = () => {
      setOpen(true);
      setIsOpening(true);
    };

    const handleMouseLeave = () => {
      setTimeout(() => {
        if (!tooltipHovered.current) {
          setIsOpening(false);
          setTimeout(() => {
            setOpen(false);
          }, 200);
        }
      }, 100);
    };

    useEffect(() => {
      if (target) {
        if (typeof target === "object") {
          const targetElement = target as MutableRefObject<any>;
          if (targetElement.current) {
            setReferenceElement(targetElement.current);
            targetElement.current.addEventListener("click", () => {
              setOpen(!open);
            });
            return () => {
              targetElement.current.removeEventListener("click", () => {
                setOpen(!open);
              });
            };
          }
        } else if (typeof target === "string") {
          const targetElement = document.getElementById(
            target as string
          ) as HTMLElement;
          setReferenceElement(targetElement as HTMLElement);
          if (targetElement) {
            // add mouse enter and mouse leave event listeners
            targetElement.addEventListener("mouseenter", handleMouseEnter);
            targetElement.addEventListener("mouseleave", handleMouseLeave);
            return () => {
              targetElement.removeEventListener("mouseenter", handleMouseEnter);
              targetElement.removeEventListener("mouseleave", handleMouseLeave);
            };
          }
        }
      }
    }, [target]);

    useImperativeHandle(ref, () => ({
      api: {
        open: () => {
          setOpen(true);
        },
        close: () => {
          setOpen(false);
        },
        toggle: () => {
          setOpen(!open);
        },
      },
    }));

    const tooltipElement = (
      <StyledTooltip
        ref={setPopperElement}
        style={styles.popper}
        isOpening={isOpening}
        onMouseEnter={() => handleMouseEnterPopover()}
        onMouseLeave={() => handleMouseLeavePopover()}
        theme={theme}
        {...attributes.popper}
        {...rest}
      >
        {children}
        <StyledArrow ref={setArrowElement} style={styles.arrow} />
      </StyledTooltip>
    );
    return (
      <>
        {open &&
          (usePortal
            ? createPortal(tooltipElement, document.body)
            : tooltipElement)}
      </>
    );
  }
);

Popover.displayName = "Popover";
export default Popover;
