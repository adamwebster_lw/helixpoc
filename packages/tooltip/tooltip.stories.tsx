import React from "react";
import { Tooltip } from ".";
import { Small } from "@lwt-helix-nextgen/typography";
export default {
  title: "Components/Tooltip",
  component: Tooltip,
};

const Template = (args: any) => {
  return (
    <>
      {/* div with content centered both verically and horizontally */}
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <span id="target">Hover Here</span>
        <Tooltip
          target="target"
          popperModifiers={[
            {
              name: "offset",
              options: {
                offset: [0, 8],
              },
            },
          ]}
          {...args}
        >
          This is a tip to let say what this thing does
        </Tooltip>
      </div>
    </>
  );
};

export const Default: any = Template.bind({});

Default.args = {};

export const AutoHideFalse: any = Template.bind({});

AutoHideFalse.args = {
  autoHide: false,
};
