import React, { HtmlHTMLAttributes } from "react";
import { Button } from "@lwt-helix-nextgen/button";
import { Badge } from "@lwt-helix-nextgen/badge";
import { StyledClientSwitcher } from "./client-switcher.styles";

export interface Client {
  dataLwtId?: string;
  /** Internal client id, e.g. ROY500 */
  id: string;
  /** Human-readable brokerage name, e.g. Royal LePage R.E. Services Ltd.. */
  name: string;
  /** Optional flag indicating if the client is a demo one. */
  demo?: boolean;
  /** Optional flag indicating if the client is flagged as a support client. */
  support?: boolean;
}

export interface ClientSwitcherProps
  extends HtmlHTMLAttributes<HTMLDivElement> {
  dataLwtId?: string;
  /** List of clients to display to the user. */
  clients: readonly Client[];
  /**
   * Run when a "Log In" button is clicked.
   * Callback must accept a single string, which will be the new client id to log in as.
   */
  setClient: (newClient: string) => void;
  /** If supplied, this class is applied to the table container. */
}

const ClientSwitcher = ({
  className = "",
  clients,
  dataLwtId,
  setClient,
  ...rest
}: ClientSwitcherProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledClientSwitcher
      className={`helix-checkbox ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {clients.map((client: Client) => (
        <div
          className="helix-client-switcher__item"
          key={client.id}
          data-client-id={client.id}
          data-lwt-id={client.dataLwtId}
        >
          <div className="helix-client-switcher__item-name">
            {client.name}
            {client.demo && (
              <Badge
                className="helix-client-switcher__client-badge"
                size="tiny"
              >
                Demo
              </Badge>
            )}
            {client.support && (
              <Badge
                className="helix-client-switcher__client-badge"
                size="tiny"
                badgeStyle="default"
              >
                Support
              </Badge>
            )}
            <div className="helix-client-switcher__client-id">
              {client.id}
            </div>
          </div>
          <div className="helix-client-switcher__item-action">
            <Button
              size="small"
              color="secondary"
              dataLwtId={`${dataLwtId}-button`}
              className="float-right"
              onClick={() => setClient(client.id)}
            >
              Log In
            </Button>
          </div>
        </div>
      ))}
    </StyledClientSwitcher>
  );
};

ClientSwitcher.displayName = "ClientSwitcher";

export default ClientSwitcher;
