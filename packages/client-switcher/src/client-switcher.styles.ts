import styled from 'styled-components';

export const StyledClientSwitcher = styled.div`
  display: flex;
  width: 100%;
  max-width: 100%;
  margin-bottom: 1rem;
  background-color: transparent;
  flex-direction: column;
  .helix-client-switcher__item {
    display: flex;
    flex: 1 1;
    align-items: center;
    padding: 16px 0px 16px 8px;
    box-shadow: inset 0px -1px 0px ${({ theme }) => theme.clientSwitcher?.rowBoxShadowColor};
    .helix-client-switcher__client-badge {
      margin-left: 6px;
    }
    .helix-client-switcher__item-name {
      flex: 1 1;
      padding-right: 16px;
    }
    .helix-client-switcher__client-id {
      color: ${({ theme }) => theme.clientSwitcher?.clientIdTextColor};
      font-size: 13px;
      font-weight: 400;
    }
  }
`;
