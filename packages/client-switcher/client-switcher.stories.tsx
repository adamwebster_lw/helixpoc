import React from 'react';
import { ClientSwitcher } from '.';

export default {
  title: 'Components/Client Switcher',
  component: ClientSwitcher,
};

const Template = (args: any) => (
  <ClientSwitcher
    clients={[
      { id: 'LWT001', name: 'Lone Wolf Technologies' },
      { id: 'DEMO01', name: 'LoneWolf Technologies Demo Client', demo: true },
      { id: 'SUP001', name: 'Lone Wolf Technologies', support: true },
    ]}
    setClient={(client) => console.log(client)}
  />
);

export const Default = Template.bind({});
