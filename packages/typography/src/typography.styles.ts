import styled, { css } from "styled-components";

interface TypographyProps {
  mobile?: boolean;
}

export const DisplayXLarge = styled.span<TypographyProps>`
  font-size: 48px;
  font-weight: 600;
  line-height: 52px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 32px;
      line-height: 35px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 32px;
    line-height: 35px;
  }
`;

DisplayXLarge.displayName = "DisplayXLarge";

export const DisplayLarge = styled.span<TypographyProps>`
  font-size: 36px;
  font-weight: 600;
  line-height: 44px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 24px;
      line-height: 29px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 24px;
    line-height: 29px;
  }
`;

DisplayLarge.displayName = "DisplayLarge";

export const DisplayMedium = styled.span<TypographyProps>`
  font-size: 30px;
  font-weight: 600;
  line-height: 36px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 20px;
      line-height: 24px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 20px;
    line-height: 24px;
  }
`;

DisplayMedium.displayName = "DisplayMedium";

export const DisplaySmall = styled.span<TypographyProps>`
  font-size: 24px;
  font-weight: 600;
  line-height: 30px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 16px;
      line-height: 20px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 16px;
    line-height: 20px;
  }
`;

DisplaySmall.displayName = "DisplaySmall";

export const DisplayXSmall = styled.span<TypographyProps>`
  font-size: 20px;
  font-weight: 600;
  line-height: 26px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 15px;
      line-height: 18px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 15px;
    line-height: 18px;
  }
`;

DisplayXSmall.displayName = "DisplayXSmall";

export const Heading = styled.span<TypographyProps>`
  font-size: 16px;
  font-weight: 600;
  line-height: 22px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 17px;
      line-height: 24px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 17px;
    line-height: 24px;
  }
`;

Heading.displayName = "Heading";

export const SubHeading = styled.span<TypographyProps>`
  font-size: 12px;
  font-weight: 600;
  line-height: 16px;
  text-transform: uppercase;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 13px;
      line-height: 17px;
    `}

  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 13px;
    line-height: 17px;
  }
`;

SubHeading.displayName = "SubHeading";

export const Body = styled.span<TypographyProps>`
  font-size: 14px;
  font-weight: normal;
  line-height: 20px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 15px;
      line-height: 20px;
    `}

  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 15px;
    line-height: 20px;
  }
`;

Body.displayName = "Body";

export const BodyStrong = styled.span<TypographyProps>`
  font-size: 14px;
  font-weight: 600;
  line-height: 20px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 15px;
      line-height: 20px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 15px;
    line-height: 20px;
  }
`;

BodyStrong.displayName = "BodyStrong";

export const Number = styled.span<TypographyProps>`
  font-size: 14px;
  font-weight: normal;
  line-height: 20px;
  font-feature-settings: "tnum" on, "lnum" on;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 15px;
      line-height: 20px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 15px;
    line-height: 20px;
  }
`;

Number.displayName = "Number";

export const NumberStrong = styled.span<TypographyProps>`
  font-size: 14px;
  font-weight: 600;
  line-height: 20px;
  font-feature-settings: "tnum" on, "lnum" on;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 15px;
      line-height: 20px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 15px;
    line-height: 20px;
  }
`;

NumberStrong.displayName = "NumberStrong";

export const Small = styled.span<TypographyProps>`
  font-size: 12px;
  font-weight: normal;
  line-height: 16px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 13px;
      line-height: 17px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 13px;
    line-height: 17px;
  }
`;

Small.displayName = "Small";

export const SmallStrong = styled.span<TypographyProps>`
  font-size: 12px;
  font-weight: 600;
  line-height: 16px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 13px;
      line-height: 17px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 13px;
    line-height: 17px;
  }
`;

SmallStrong.displayName = "SmallStrong";

export const Code = styled.span<TypographyProps>`
  font-size: 12px;
  font-weight: 500;
  line-height: 10px;
  ${({ mobile }) =>
    mobile &&
    css`
      font-size: 13px;
      line-height: 20px;
    `}
  // media query screen less then 768px
    @media (max-width: 768px) {
    font-size: 13px;
    line-height: 20px;
  }
`;

Code.displayName = "Code";
