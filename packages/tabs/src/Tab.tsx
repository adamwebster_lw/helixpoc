import React, { useContext } from "react";
import { TabContext } from "./TabContext";
import { StyledTab } from "./Tabs.styles";
interface TabProps extends React.HTMLAttributes<HTMLDivElement> {
  dataLwtId?: string;
  children?: React.ReactNode;
  eventKey: string;
}

const Tab = ({
  dataLwtId,
  children,
  className,
  eventKey,
  onClick,
  ...rest
}: TabProps) => {
  const { state, dispatch } = useContext(TabContext);

  const handleClick = (e) => {
    dispatch({ type: "SET_ACTIVE_KEY", payload: eventKey });
    if (onClick) {
      onClick(e);
    }
  };

  const handleKeydown = (e) => {
    if (e.key === "Enter") {
      dispatch({ type: "SET_ACTIVE_KEY", payload: eventKey });
      onClick && onClick(e);
    }
  };

  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledTab
      className={`helix-tab ${className ? className : ""}`}
      onClick={handleClick}
      active={state.activeKey === eventKey}
      tabIndex={0}
      size={state.size}
      onKeyDown={handleKeydown}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledTab>
  );
};

Tab.displayName = "Tab";

export default Tab;
