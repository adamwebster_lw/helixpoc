import React, { HtmlHTMLAttributes, useContext } from "react";
import { TabContext } from "./TabContext";
import { StyledTabPane } from "./Tabs.styles";

interface TabPaneProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  dataLwtId?: string;
  eventKey: string;
}

const TabPane = ({
  children,
  dataLwtId,
  className,
  eventKey,
  ...rest
}: TabPaneProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  const { state } = useContext(TabContext);
  return (
    <StyledTabPane
      active={state.activeKey === eventKey}
      className={`helix-tab-pane ${className ? className : ""}`}
      {...optionalProps}
    >
      {children}
    </StyledTabPane>
  );
};

TabPane.displayName = "TabPane";

export default TabPane;
