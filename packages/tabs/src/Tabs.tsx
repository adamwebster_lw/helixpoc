import React, { useEffect, useContext } from "react";
import { HtmlHTMLAttributes, ReactNode } from "react";
import { TabSizes } from "./Tab.types";
import { TabContextProvider, TabContext } from "./TabContext";
import { TabsContainer } from "./Tabs.styles";

export interface TabsProps extends HtmlHTMLAttributes<HTMLDivElement> {
  dataLwtId?: string;
  children?: ReactNode;
  defaultActiveKey: string;
  size?: TabSizes;
}

const Tabs = ({ children, ...rest }: TabsProps) => {
  return (
    <TabContextProvider>
      <TabsInner {...rest}>{children}</TabsInner>
    </TabContextProvider>
  );
};

export const TabsInner = ({
  dataLwtId,
  children,
  className,
  defaultActiveKey,
  size,
  ...rest
}: TabsProps) => {
  const { state, dispatch } = useContext(TabContext);

  useEffect(() => {
    dispatch({ type: "SET_ACTIVE_KEY", payload: defaultActiveKey });
  }, [defaultActiveKey]);

  useEffect(() => {
    dispatch({ type: "SET_SIZE", payload: size });
  }, [size]);
  
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <TabsContainer
      className={`helix-tabs ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </TabsContainer>
  );
};

Tabs.displayName = "Tabs";

export default Tabs;
