import styled, { css } from "styled-components";
import { TabSizes } from "./Tab.types";

export const TabsContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

interface StyledTabProps {
  active?: boolean;
  disabled?: boolean;
  size?: TabSizes;
}

export const StyledTab = styled.div<StyledTabProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 3px 6px 12px 6px;
  width: fit-content;
  border-bottom: 2px solid transparent;
  font-weight: 700;
  font-size: 14px;
  color: ${({ theme }) => theme.tabs?.textColor};
  ${({ active, theme }) =>
    active &&
    css`
      border-bottom: 2px solid ${theme.tabs?.activeBorderColor};
      color: ${theme.tabs?.activeTextColor};
    `}

  ${({ active }) =>
    !active &&
    css`
      &:hover {
        color: ${({ theme }) => theme.tabs?.hoverTextColor};
        cursor: pointer;
      }
    `}
  ${({ disabled }) =>
    disabled &&
    css`
      cursor: not-allowed;
    `}
    + .helix-tab {
    margin-left: 28px;
  }
  ${({ size }) =>
    size === "large" &&
    css`
      font-size: 18px;
    `}
`;

interface StyledTabPaneProps {
  active: boolean;
}

export const StyledTabPane = styled.div<StyledTabPaneProps>`
  display: none;
  flex: 1 1 100%;
  margin-top: 12px;
  ${({ active }) =>
    active &&
    css`
      display: block;
    `}
`;
