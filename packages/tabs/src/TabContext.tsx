import React, { createContext } from "react";
import { TabSizes } from "./Tab.types";

interface InitialStateProps {
  activeKey: string;
  size: TabSizes;
}

const initialState: InitialStateProps = {
  activeKey: "",
  size: "medium",
};

interface dispatchValuesProps {
  type: "SET_ACTIVE_KEY" | "SET_ON_SELECT" | "SET_SIZE";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  payload?: any;
}

export const TabContext = createContext({
  state: initialState,
  dispatch: (value: dispatchValuesProps | void) => value,
});

// reducer for tab context
const tabContextReducer = (
  state: InitialStateProps,
  action: dispatchValuesProps
) => {
  switch (action.type) {
    case "SET_ACTIVE_KEY":
      return {
        ...state,
        activeKey: action.payload,
      };
    case "SET_SIZE":
      return {
        ...state,
        size: action.payload,
      };
    default:
      return state;
  }
};

export const TabContextConsumer = TabContext.Consumer;

export const TabContextProvider = ({ children }: any) => {
  const [state, dispatch] = React.useReducer(tabContextReducer, initialState);
  return (
    <TabContext.Provider value={{ state, dispatch }}>
      {children}
    </TabContext.Provider>
  );
};
