import React from "react";
import { Tabs, Tab, TabPane } from "./index";

export default {
  title: "Components/Tabs",
  component: Tabs,
  argTypes: {
    disabled: {
      type: "boolean",
    },
  },
};

const Template = (args: any) => (
  <Tabs {...args}>
    <Tab eventKey="1" data-lwt-id="tab-1">
      Tab 1
    </Tab>
    <Tab eventKey="2" data-lwt-id="tab-2">
      Tab 2
    </Tab>
    <Tab eventKey="3" data-lwt-id="tab-3">
      Tab 3
    </Tab>
    <TabPane eventKey="1" data-lwt-id="tab-pane-1">
      Tab Pane 1
    </TabPane>
    <TabPane eventKey="2" data-lwt-id="tab-pane-2">
      Tab Pane 2
    </TabPane>
    <TabPane eventKey="3" data-lwt-id="tab-pane-3">
      Tab Pane 3
    </TabPane>
  </Tabs>
);

export const Default = Template.bind({});

Default.args = {
  defaultActiveKey: "1",
  size: "medium",
};
