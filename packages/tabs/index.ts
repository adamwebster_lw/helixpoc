export { default as Tabs } from './src/Tabs';
export { default as TabPane } from './src/TabPane';
export { default as Tab } from './src/Tab';