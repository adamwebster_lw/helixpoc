import React from "react";
import { ButtonHTMLAttributes, ReactNode } from "react";
import { HelixButtonSizes } from "./button.types";
import { HelixButtonColors } from "../../types";
import { StyledButton } from "./Button.styles";
import { DefaultTheme } from "styled-components";
export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  /** The color of the button */
  color?: HelixButtonColors;
  children: ReactNode;
  /** If the button should be displayed in its active state */
  active?: boolean;
  /** The icon for the button */
  icon?: ReactNode;
  /** The size of the button */
  size?: HelixButtonSizes;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** If the button should be displayed in a outline style */
  outline?: boolean;
  theme?: DefaultTheme;
}

export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  (
    {
      color = "tertiary",
      children,
      icon,
      active = false,
      size = "medium",
      dataLwtId,
      outline,
      className,
      ...rest
    },
    ref
  ) => {
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }
    return (
      <StyledButton
        active={active}
        color={color}
        size={size}
        ref={ref}
        outline={outline}
        className={`helix-button ${className ? className : ""}`}
        {...optionalProps}
        {...rest}
      >
        {children} {icon && <span className="helix-button-icon">{icon}</span>}
      </StyledButton>
    );
  }
);

Button.displayName = "Button";

export default Button;
