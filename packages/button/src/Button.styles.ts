import styled, { css } from "styled-components";
import { HelixButtonSizes } from "./button.types";
import { HelixButtonColors } from "../../types";

interface StyledButtonProps {
  color: HelixButtonColors;
  active?: boolean;
  size: HelixButtonSizes;
  outline?: boolean;
}

const getButtonPadding = (size: HelixButtonSizes) => {
  switch (size) {
    case "small":
      return "4px 8px";
    case "large":
      return "12px 24px";
    case "medium":
    default:
      return "8px 16px";
  }
};
export const StyledButton = styled.button<StyledButtonProps>`
  background-color: ${({ theme, color }) =>
    theme.button![color]?.backgroundColor};
  padding: ${({ size }) => getButtonPadding(size)};
  border-radius: 4px;
  color: ${({ theme, color }) => theme.button![color]?.textColor};
  border: none;
  cursor: pointer;
  font-size: 16px;
  min-height: 36px;
  ${({ color, theme }) =>
    color === "secondary" &&
    css`
      border: solid 1px ${theme.button?.secondary?.borderColor};
      &:hover:not(:disabled):not(.disabled) {
        border: solid 1px ${theme.button?.secondary?.borderHoverColor};
      }
    `}
  .helix-button-icon {
    margin-left: 8px;
  }
  ${({ outline, color, theme }) =>
    !outline &&
    css`
      &:hover:not(:disabled):not(.disabled) {
        background-color: ${theme.button![color]!.hoverBackgroundColor};
        color: ${theme.button![color]?.hoverTextColor};
      }
    `}
  &:disabled,
  .disabled {
    opacity: 0.5;
    cursor: default !important;
  }
  &:active {
    background-color: ${({ color, theme }) =>
      theme.button![color]?.activeBackgroundColor};
  }

  ${({ active, color, theme }) =>
    active &&
    css`
      background-color: ${theme.button![color]?.activeBackgroundColor};
    `};

  &:focus {
    outline: ${({ theme }) => theme.colors?.blue![600]} solid 2px;
    ${({ color }) =>
      color === "primary" &&
      css`
        outline-offset: 1px;
      `}
  }
  ${({ outline, color, theme }) =>
    outline &&
    color !== "secondary" &&
    color !== "ghost" &&
    css`
      background-color: transparent;  
      border: solid 1px ${theme.button![color]?.outlineBorderColor}; };
      color: ${theme.button![color]?.outlineTextColor};
      &:hover:not(:disabled):not(.disabled) {
        border-color: ${theme.button![color]?.outlineHoverBorderColor};
        color: ${theme.button![color]?.outlineHoverTextColor};
      }
    `}
`;
