import React from "react";
import { Button } from "./src/Button";

export default {
  title: "Components/Button",
  component: Button,
  argTypes: {
    disabled: {
      type: "boolean",
    },
  },
};

const Template = (args: any) => <Button {...args}>Button</Button>;

export const Default = Template.bind({});

export const Primary: any = Template.bind({});
Primary.args = {
  color: "primary",
};

export const PrimaryOutline: any = Template.bind({});
PrimaryOutline.args = {
  color: "primary",
  outline: true,
};
