import babel from "@rollup/plugin-babel";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";

const packageJson = require("./package.json");
const extensions = [".js", ".jsx", ".ts", ".tsx"];

const globals = {
  react: "React",
  "react-dom": "ReactDOM",
  "prop-types": "PropTypes",
  "styled-components": "styled",
  "react-datepicker": "*",
  classnames: "*",
  "@lwt-helix-nextgen/input": "*",
  "date-fns": "*",
  "date-fns/locale": "*",
  "react-datepicker/dist/react-datepicker.css": "*",
  "react-text-mask": "*",
};

const globalModules = Object.keys(globals);

const sourceMap = true;

export default {
  input: "index.ts",
  output: [
    {
      file: packageJson.main,
      format: "cjs",
      sourcemap: true,
    },
    {
      dir: "build",
      format: "esm",
      preserveModules: true,
      sourcemap: true,
    },
  ],
  plugins: [
    resolve({ extensions }),
    commonjs({
      include: "**/node_modules/**",
    }),
    typescript({
      typescript: require("typescript"),
      useTsconfigDeclarationDir: true,
      tsconfigOverride: {
        exclude: [""],
      },
    }),
    babel({
      sourceMap,
      extensions,
      include: ["src/**/*"],
      exclude: ["node_modules/**", "**/*.css"],
    }),
  ],
  // toggle comment below if using npm/yarn link you might not need the second option
  external: (id) => globalModules.includes(id) || /core-js/.test(id),
  // external: id => globalModules.includes(id),
};
