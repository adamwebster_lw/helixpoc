import styled from "styled-components";

export const StyledDatePicker = styled.div`
  .react-datepicker__close-icon {
    display: block !important;
  }

  .react-datepicker__close-icon::after {
    line-height: 0.75 !important;
    color: black !important;
    background-color: transparent !important;
    font-size: 16px !important;
    top: 15px;
    right: 10px;
    content: "" !important;
    height: 20px;
    background-image: url("data:image/svg+xml,%3Csvg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M10 16C13.3137 16 16 13.3137 16 10C16 6.68629 13.3137 4 10 4C6.68629 4 4 6.68629 4 10C4 13.3137 6.68629 16 10 16ZM8.54881 7.47082C8.25656 7.17729 7.78168 7.17626 7.48815 7.46852C7.19463 7.76078 7.19359 8.23565 7.48585 8.52918L8.94164 9.9913L7.46852 11.4708C7.17626 11.7644 7.17729 12.2392 7.47082 12.5315C7.76435 12.8237 8.23922 12.8227 8.53148 12.5292L10 11.0543L11.4685 12.5292C11.7608 12.8227 12.2356 12.8237 12.5292 12.5315C12.8227 12.2392 12.8237 11.7644 12.5315 11.4708L11.0584 9.9913L12.5141 8.52918C12.8064 8.23565 12.8054 7.76078 12.5118 7.46852C12.2183 7.17626 11.7434 7.17729 11.4512 7.47082L10 8.92833L8.54881 7.47082Z' fill='%234B5563'/%3E%3C/svg%3E%0A") !important;
  }

  .react-datepicker__input-container > input[type="text"]::-ms-clear {
    display: none;
  }

  .react-datepicker-full-width .react-datepicker-wrapper {
    width: 100%;
  }

  .react-datepicker-full-width .react-datepicker__input-container {
    width: 100%;
  }

  .react-datepicker {
    padding: 12px !important;
    border: none !important;
    box-shadow: ${({ theme }) => theme.datePicker?.dropdownMenuBoxShadow};
    border-radius: 6px !important;
  }
  .react-datepicker__header {
    background-color: ${({ theme }) =>
      theme.datePicker?.dropdownMenuBackgroundColor}!important;
    border-bottom: none !important;
    padding-top: 0 !important;
  }

  .react-datepicker__navigation {
    top: 16px !important;
  }
  .react-datepicker__day,
  .react-datepicker__day-name {
    margin: 4px !important;
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--keyboard-selected {
    background-color: ${({ theme }) =>
      theme.datePicker?.dateSelectedColor} !important;
  }
  .react-datepicker-popper[data-placement^="bottom"] {
    margin-top: 0 !important;
  }

  .react-datepicker__day:hover,
  .react-datepicker__month-text:hover {
    background-color: ${({ theme }) =>
      theme.datePicker?.dateHoverColor} !important;
  }

  .react-datepicker-popper[data-placement^="bottom"]
    .react-datepicker__triangle {
    border-bottom-color: #fff !important;
    display: none;
  }

  .react-datepicker__navigation--next {
    border-left-color: #4b5563 !important;
  }

  .react-datepicker__navigation--previous {
    border-right-color: #4b5563 !important;
  }

  .react-datepicker__month {
    margin: 0px !important;
  }
  .react-datepicker__day-names {
    box-shadow: inset 0px -1px 0px rgba(0, 0, 0, 0.05);
  }

  .react-datepicker__day--outside-month {
    opacity: 0.4;
  }
`;
