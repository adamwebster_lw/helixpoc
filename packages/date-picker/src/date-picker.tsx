import React, { Component, FormEvent } from "react";
// import portal from react-dom
import { createPortal } from "react-dom";
import ReactDatePicker, { registerLocale } from "react-datepicker";
import { format, isValid } from "date-fns";
import classNames from "classnames";
import MaskedTextInput from "react-text-mask";
import "react-datepicker/dist/react-datepicker.css";
import { StyledDatePicker } from "./date-picker.styles";
import * as Locales from "date-fns/locale";
import { Locale } from "date-fns";
import { Input } from "@lwt-helix-nextgen/input";

const LocalesArray = Object.values(Locales);
export interface DatePickerProps {
  /** current date value as a string (see the backEndFormat prop) */
  value?: string;
  /** placeholder text value (defaults to be the same as displayFormat) */
  placeholderText?: string;
  /** css class name to use for the component */
  className?: string;
  /** the format in which the date is displayed (defaults to 'MM/DD/YYYY') */
  displayFormat?: string;
  /** the format in which the date is passed into and out of the component (defaults to 'yyyy-MM-dd') */
  backEndFormat?: string;
  /** used to assign the data attribute 'data-lwt-id' used for automation detection */
  dataLwtId?: string;
  /** used to determine whether or not to disable the date picker */
  readOnly?: boolean;
  /** lengthens the component to take up the entire width available */
  fullWidth?: boolean;
  /** determines whether to use a masked input */
  mask?: boolean;
  /** the character separator for mask input */
  maskCharacter?: string;
  /** sets the localization */
  locale?: string;
  /** callback for when the date value is updated */
  onChange?(updatedDate: string | undefined): void;
  /** callback for when the field looses focus */
  onBlur?(event: React.FocusEvent<HTMLInputElement>): void;
  /** callback for when the field gains focus */
  onFocus?(event: React.FocusEvent<HTMLInputElement>): void;
  /** callback for when the field is typed in */
  onKeyDown?(event: React.KeyboardEvent<HTMLInputElement>): void;
  onCalendarClose?(): void;
  usePortal?: boolean;
}

export interface DatePickerState {
  selectedDate: Date | null;
  placeholderText: string;
  classNameContainer: string;
  className: string;
  displayFormat: string;
  backEndFormat: string;
  locale: string;
}

export default class DatePicker extends Component<
  DatePickerProps,
  DatePickerState
> {
  datePickerRef: any;
  constructor(props: DatePickerProps) {
    super(props);
    this.datePickerRef = React.createRef();
    const value = this.props.value && DatePicker.formatDate(this.props.value);
    this.state = {
      selectedDate: value
        ? new Date(value.includes("T") ? value.replace(/T.+/, "") : value)
        : null,
      placeholderText: this.props.placeholderText
        ? this.props.placeholderText
        : this.props.displayFormat
        ? this.props.displayFormat
        : "MM/DD/YYYY",
      className: this.props.className ? this.props.className : "form-control",
      classNameContainer: this.props.fullWidth
        ? "react-datepicker-full-width"
        : "",
      displayFormat: this.props.displayFormat
        ? this.props.displayFormat
        : "MM/dd/yyyy",
      backEndFormat: this.props.backEndFormat
        ? this.props.backEndFormat
        : "yyyy-MM-dd",
      locale: this.props.locale ? this.props.locale : "en-US",
    };
    DatePicker.registerLocalization(this.props.locale);
  }

  static formatDate(date: string): string {
    return date.replace(/-/g, "/").replace(/\./g, "/");
  }

  static isYearGreaterThan10000(date: Date): boolean {
    return Math.round(date.getFullYear() / 10000) > 0;
  }

  static registerLocalization(code: string | undefined) {
    if (code) {
      const locale = LocalesArray.find((l) => l.code === code) as Locale;
      if (locale && locale.code) {
        registerLocale(locale.code, locale);
      }
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: DatePickerProps) {
    this.setState((prevState) => {
      const value = nextProps.value && DatePicker.formatDate(nextProps.value);
      const date =
        (value &&
          new Date(value.includes("T") ? value.replace(/T.+/, "") : value)) ||
        undefined;

      const className = classNames(
        nextProps.className,
        prevState.className.indexOf("form-control") !== -1 && "form-control"
      );

      if (nextProps.locale !== prevState.locale) {
        DatePicker.registerLocalization(nextProps.locale);
      }

      return {
        ...prevState,
        selectedDate:
          date !== undefined && !isNaN(date.getTime()) ? date : null,
        className,
        classNameContainer: this.props.fullWidth
          ? "react-datepicker-full-width"
          : "",
      };
    });
  }

  handleDivChange = (
    event: FormEvent<HTMLDivElement> | null | undefined
  ): void => {
    let selectedDate;
    let backEndDateValue;
    if (event && event.target) {
      const inputEvent: HTMLInputElement = event.target as HTMLInputElement;
      const displayDate = new Date(inputEvent.value);
      if (
        !isValid(displayDate) ||
        DatePicker.isYearGreaterThan10000(displayDate)
      ) {
        event.preventDefault();
        return;
      }
      if (displayDate) {
        if (!isNaN(displayDate.getTime())) {
          selectedDate = displayDate;
          backEndDateValue = format(selectedDate, this.state.backEndFormat);
        } else {
          selectedDate = this.state.selectedDate;
          backEndDateValue =
            this.state.selectedDate &&
            format(this.state.selectedDate, this.state.backEndFormat);
        }
      } else {
        selectedDate = null;
      }
      this.changeState(selectedDate, backEndDateValue);
    }
  };

  handleChange = (date: Date | null): void => {
    if (date && DatePicker.isYearGreaterThan10000(date)) {
      return;
    }
    const backEndDateValue = date
      ? format(date as Date, this.state.backEndFormat)
      : undefined;

    this.changeState(date, backEndDateValue);
  };

  handleRefChange = (ref) => {
    if (ref) {
      this.datePickerRef = ref;
    }
  };

  handleKeyDown = (e: any) => {
    if (e.key === "Tab") {
      // close the calendar if tab is pressed
      this.datePickerRef.setOpen(false);
    }
    this.props.onKeyDown && this.props.onKeyDown(e);
  };

  handleCalendarClose = () => {
    // Work around for input not blurring when calendar is closed
    // This is a know issue with react-datepicker and can be removed when it is fixed
    // https://github.com/Hacker0x01/react-datepicker/issues/2028
    if (this.datePickerRef.input && this.datePickerRef.input.focus) {
      this.datePickerRef.input.focus();
      this.datePickerRef.input.blur();
    }

    if (
      this.datePickerRef.input.inputElement &&
      this.datePickerRef.input.inputElement.focus
    ) {
      this.datePickerRef.input.inputElement.focus();
      this.datePickerRef.input.inputElement.blur();
    }

    this.props.onCalendarClose && this.props.onCalendarClose();
  };

  render() {
    const {
      value,
      placeholderText,
      displayFormat,
      className,
      backEndFormat,
      locale,
      usePortal,
      dataLwtId,
      readOnly,
      onChange,
      onBlur,
      onFocus,
      maskCharacter = this.props.maskCharacter ?? "/",
      mask,
      onKeyDown,
      onCalendarClose,
      ...restProps
    } = this.props;

    // const maskCharacter = this.props.maskCharacter ?? '/';
    const maskCollection: (string | RegExp)[] = [];
    if (mask && displayFormat) {
      let maskSeparator;
      for (let i = 0; i < displayFormat.length; i++) {
        const char = displayFormat.charAt(i);
        switch (char) {
          case "M":
          case "d":
          case "y":
            maskCollection.push(/\d/);
            break;
          default:
            if (maskCollection.length > 0) {
              if (!maskSeparator) {
                maskSeparator = char;
              }
              if (char === maskSeparator) {
                maskCollection.push(maskCharacter);
              }
            }
            break;
        }
      }
    }

    const optionalProps: any = {};
    if (usePortal) {
      optionalProps.popperContainer = (props) =>
        createPortal(
          <StyledDatePicker
            className={this.state.classNameContainer}
            onChange={this.handleDivChange}
            onClick={(e) => e.stopPropagation()}
            {...optionalLwtIdProps}
          >
            {props.children}
          </StyledDatePicker>,
          document.body
        );
      optionalProps.popperModifiers = {
        preventOverflow: {
          enabled: true,
        },
      };
    }

    const optionalLwtIdProps: any = {};
    if (dataLwtId) {
      optionalLwtIdProps["data-lwt-id"] = dataLwtId;
    }

    return (
      <>
        <StyledDatePicker
          className={this.state.classNameContainer}
          onChange={this.handleDivChange}
          onClick={(e) => e.stopPropagation()}
          {...optionalLwtIdProps}
        >
          <ReactDatePicker
            className={this.state.className}
            selected={this.state.selectedDate}
            onChange={this.handleChange}
            isClearable={!readOnly}
            dateFormat={this.state.displayFormat}
            placeholderText={this.state.placeholderText}
            onBlur={onBlur}
            onFocus={onFocus}
            onKeyDown={(e) => this.handleKeyDown(e)}
            onCalendarClose={() => this.handleCalendarClose()}
            readOnly={readOnly}
            locale={locale}
            customInput={
              mask ? (
                <MaskedTextInput
                  type="text"
                  mask={maskCollection}
                  render={(ref, props) => <Input ref={ref} {...props} />}
                />
              ) : (
                <Input />
              )
            }
            {...optionalProps}
            {...restProps}
            ref={this.handleRefChange}
          />
        </StyledDatePicker>
      </>
    );
  }

  private changeState = (
    selectedDate: Date | null,
    backEndDateValue?: string
  ): void => {
    this.setState(
      {
        selectedDate: selectedDate,
      },
      () => {
        if (this.props.onChange) {
          this.props.onChange(backEndDateValue);
        }
      }
    );
  };
}
