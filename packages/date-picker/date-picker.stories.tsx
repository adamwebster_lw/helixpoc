import React from "react";
import DatePicker from "./src/date-picker";

export default {
  title: "Components/Date Picker",
  component: DatePicker,
};

const Template = (args: any) => <DatePicker {...args} />;

export const Default = Template.bind({});

export const UsePortal = Template.bind({});
UsePortal.args = {
  usePortal: true,
};

export const Masked = Template.bind({});
Masked.args = {
  maskCharacter: ".",
  displayFormat: "yyyy/dd/MM",
  placeholderText: "Pick a date. Any date!",
  mask: true,
  backEndFormat: "yyyy.MM.dd",
};
