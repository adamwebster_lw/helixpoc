import styled, { css } from "styled-components";

export const StyledButtonGroup = styled.div`
  button + button,
  button + .lwt-helix-dropdown-button,
  .lwt-helix-dropdown-button + button {
    margin-left: 1px;
  }
  button {
    &:first-child:not(:only-child):not(.lwt-helix-dropdown-button button) {
      border-bottom-right-radius: 0;
      border-top-right-radius: 0;
    }
    &:not(:first-child):not(:last-child):not(.lwt-helix-dropdown-button
        button) {
      border-radius: 0;
    }
    &:last-child:not(:only-child):not(.lwt-helix-dropdown-button button) {
      border-bottom-left-radius: 0;
      border-top-left-radius: 0;
    }
  }
  .lwt-helix-dropdown-button {
    &:first-child:not(:only-child) button {
      border-bottom-right-radius: 0;
      border-top-right-radius: 0;
    }
    &:not(:first-child):not(:last-child) button {
      border-radius: 0;
    }
    &:last-child:not(:only-child) button {
      border-bottom-left-radius: 0;
      border-top-left-radius: 0;
    }
  }
`;
