import React, { HtmlHTMLAttributes, ReactElement, ReactNode } from "react";
import { StyledButtonGroup } from "./button-group.styles";

export interface ButtonGroupProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const ButtonGroup = ({
  className,
  children,
  dataLwtId,
  ...rest
}: ButtonGroupProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledButtonGroup
      className={`lwt-helix-button-group ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledButtonGroup>
  );
};

// ButtonGroup.displayName = 'ButtonGroup';

export default ButtonGroup;
