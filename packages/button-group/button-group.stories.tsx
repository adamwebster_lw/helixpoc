import React from "react";
import ButtonGroup from "./src/button-group";
import { Button } from "@lwt-helix-nextgen/button";
import { DropdownButton, DropdownButtonItem } from "@lwt-helix-nextgen/dropdown-button";

export default {
  title: "Components/Button Group",
  component: ButtonGroup,
};

const Template = (args: any) => {
  const { color } = args;
  return (
    <ButtonGroup>
      <Button dataLwtId="button1" color={color}>
        Button
      </Button>
      <Button dataLwtId="button1" color={color}>
        Button2
      </Button>
      <Button dataLwtId="button1" color={color}>
        Button2
      </Button>
      <DropdownButton
        label="Dropdown"
        dataLwtId="dropdown_button"
        color={color}
      >
        <DropdownButtonItem textLabel="item1" id="item1">
          Item 1
        </DropdownButtonItem>
        <DropdownButtonItem textLabel="item2" id="item2">
          Item 2
        </DropdownButtonItem>
        <DropdownButtonItem textLabel="item3" id="item3">
          Item 3
        </DropdownButtonItem>
      </DropdownButton>
    </ButtonGroup>
  );
};

export const Default = Template.bind({});

export const Primary: any = Template.bind({});
Primary.args = {
  color: "primary",
};
