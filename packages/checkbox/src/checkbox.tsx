import React, { InputHTMLAttributes, ReactNode } from "react";
import { StyledCheckbox } from "./checkbox.styles";

interface CheckboxProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: ReactNode;
  /** If the input is checked or not */
  checked?: boolean;
  /** if the checkbox is disabled or not */
  disabled?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const Checkbox = React.forwardRef<HTMLInputElement, CheckboxProps>(
  ({ label, checked, disabled, className, dataLwtId, ...rest }, ref) => {
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }
    return (
      <StyledCheckbox
        className={`helix-checkbox ${className ? className : ""}`}
      >
        <label>
          <input
            checked={checked}
            disabled={disabled}
            ref={ref}
            type="checkbox"
            {...optionalProps}
            {...rest}
          />
          <div className="checkbox-input">
            <svg
              className="check-indicator"
              width="8"
              height="6"
              viewBox="0 0 8 6"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1 3L3 5L7 1"
                stroke="white"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
          <span className="checkbox-label">{label}</span>
        </label>
      </StyledCheckbox>
    );
  }
);

Checkbox.displayName = "Checkbox";
export default Checkbox;
