import styled from "styled-components";

export const StyledCheckbox = styled.div`
  &:focus-within {
    .checkbox-input {
      outline: 3px solid ${({ theme }) => theme.checkbox?.focusColor};
    }
  }
  .checkbox-input {
    width: 16px;
    height: 16px;
    position: relative;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin-right: 11px;
    background-color: ${({ theme }) => theme.checkbox?.backgroundColor};
    border-radius: 4px;
    border: solid 1.5px ${({ theme }) => theme.checkbox?.borderColor};
  }
  .check-indicator {
    color: #fff;
    position: absolute;
    width: 10px;
    height: 10px;
  }
  label {
    display: inline-flex;
    align-items: center;
  }
  input[type="checkbox"] {
    width: 0;
    height: 0;
    -webkit-appearance: none;
  }
  input[type="checkbox"]:checked + .checkbox-input {
    &:before {
      content: "";
      position: absolute;
      width: 16px;
      height: 16px;
      border: solid 1.5px
        ${({ theme }) => theme.checkbox?.checkedBackgroundColor};
      border-radius: 4px;
      background-color: ${({ theme }) =>
        theme.checkbox?.checkedBackgroundColor};
    }
  }

  input[type="checkbox"]:disabled + .checkbox-input,
  input[type="checkbox"]:disabled + .checkbox-input + .checkbox-label {
    filter: grayscale(1) opacity(0.5);
  }
`;
