import { Button } from "@lwt-helix-nextgen/button";
import React, { useRef } from "react";
import {
  TransferList,
  TransferListRefProps,
  TransferListSearch,
} from "./index";
import { Small } from "@lwt-helix-nextgen/typography";
export default {
  title: "Components/Transfer List",
  component: TransferList,
};

const Template = (args: any) => <TransferList {...args} />;
export const Default = Template.bind({});

Default.args = {
  availableItems: [
    { label: "Item 1" },
    { label: "Item 2" },
    { label: "Item 3" },
  ],
  selectedItems: [{ label: "Item 4" }, { label: "Item 5" }],
  onChange: (items: any) => console.log(items),
};

export const ItemFormatter = Template.bind({});
ItemFormatter.args = {
  id: "transfer-list-formatter",
  availableItems: [
    { label: "Item 1", description: "Item 1 Description" },
    { label: "Item 2", description: "Item 2 Description" },
    { label: "Item 3", description: "Item 3 Description" },
    { label: "Item 4", description: "Item 4 Description" },
    { label: "Item 5", description: "Item 5 Description" },
    { label: "Item 6", description: "Item 6 Description" },
    { label: "Item 7", description: "Item 7 Description" },
    { label: "Item 8", description: "Item 8 Description" },
    { label: "Item 9", description: "Item 9 Description" },
    { label: "Item 10", description: "Item 10 Description" },
    { label: "Item 11", description: "Item 11 Description" },
    { label: "Item 12", description: "Item 12 Description" },
    { label: "Item 13", description: "Item 13 Description" },
    { label: "Item 14", description: "Item 14 Description" },
    { label: "Item 15", description: "Item 15 Description" },
    { label: "Item 16", description: "Item 16 Description" },
    { label: "Item 17", description: "Item 17 Description" },
    { label: "Item 18", description: "Item 18 Description" },
  ],
  selectedItems: [
    { label: "Item 19", description: "Item 19 Description" },
    { label: "Item 20", description: "Item 20 Description" },
  ],
  itemFormatter: (item: any) => (
    <div style={{ display: "flex", flexFlow: "column" }}>
      <div style={{ display: "flex" }}> {item.label} </div>
      <div style={{ flex: "1 1" }}>
        <Small>{item.description}</Small>
      </div>
    </div>
  ),
};

export const TransferListSearchExample = (args) => {
  const transferListRef = useRef<any>(null);
  return (
    <>
      <TransferListSearch
        id="transfer-list"
        style={{
          width: "100%",
        }}
        placeholder="Search"
        onChange={(e) =>
          transferListRef &&
          transferListRef.current &&
          transferListRef.current.api.searchLists(e.target.value)
        }
      />
      <Small as="p">
        This is an example of an element that is shown below the search.
      </Small>
      <TransferList {...args} ref={transferListRef} />
    </>
  );
};

TransferListSearchExample.args = {
  availableItems: [
    { label: "Item 1" },
    { label: "Item 2" },
    { label: "Item 3" },
    { label: "Item 4" },
    { label: "Item 5" },
    { label: "Item 6" },
    { label: "Item 7" },
    { label: "Item 8" },
    { label: "Item 9" },
    { label: "Item 10" },
    { label: "Item 11" },
    { label: "Item 12" },
    { label: "Item 13" },
    { label: "Item 14" },
    { label: "Item 15" },
    { label: "Item 16" },
    { label: "Item 17" },
    { label: "Item 18" },
    { label: "Item 19" },
  ],
  selectedItems: [{ label: "Item 20" }, { label: "Item 21" }],
};

export const CustomEmptyStateMessages = (args) => {
  const transferListRef = useRef<any>(null);
  return (
    <>
      <TransferListSearch
        style={{
          width: "100%",
        }}
        placeholder="Search"
        onChange={(e) =>
          transferListRef &&
          transferListRef.current &&
          transferListRef.current.api.searchLists(e.target.value)
        }
      />
      <Small as="p">
        This is an example of an element that is shown below the search.
      </Small>
      <TransferList
        {...args}
        ref={transferListRef}
        itemFormatter={(item) => {
          return (
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "32px 1fr",
                gap: "12px",
                alignItems: "center",
              }}
            >
              {item.avatar && (
                <div
                  style={{
                    width: "32px",
                    height: "32px",
                    borderRadius: "50%",
                    overflow: "hidden",
                  }}
                >
                  <img src={item.avatar} className="avatar" />
                </div>
              )}
              <div>
                {item.label}
                {item.email && (
                  <div>
                    <a href="#">{item.email}</a>
                  </div>
                )}
              </div>
            </div>
          );
        }}
      />
    </>
  );
};

CustomEmptyStateMessages.args = {
  id: "TransferListCustomEmptyState",
  availableItems: [
    {
      label: "John Doe",
      email: "johndoe@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "Jane Doe",
      email: "janedoe@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "John Smith",
      email: "johnsmith@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "Jane Smith",
      email: "janesmith@email.com",
      avatar: "https://via.placeholder.com/32",
    },
  ],
  selectedItems: [
    {
      label: "James Bond",
      email: "jamesbond@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "Miss Moneypenny",
      email: "missmoneypenny@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "Bruce Wayne",
      email: "brucewaye@email.com",
      avatar: "https://via.placeholder.com/32",
    },
    {
      label: "Clark Kent",
      email: "clarkkent@email.com",
      avatar: "https://via.placeholder.com/32",
    },
  ],
  messages: {
    noAvailableItems: (
      <div>
        <span className="font-weight-bold">No available users</span>
        <br />
        <span className="">This is an example of a custom empty state.</span>
      </div>
    ),
    noSelectedItems: (
      <div>
        <span className="font-weight-bold">No selected users</span>
        <br />
        <span className="">This is an example of a custom empty state.</span>
      </div>
    ),
    noSelectedItemsSearch: "No selected items found",
    noAvailableItemsSearch: "No available items found",
  },
  labels: {
    availableItems: "Available Users",
    selectedItems: (
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(2, 1fr)",
          alignItems: "center",
        }}
      >
        <div>Selected Users</div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Button
            dataLwtId="Save"
            color="ghost"
            style={{
              textDecoration: "none",
            }}
          >
            Save as List
          </Button>
        </div>
      </div>
    ),
    add: "Add user",
    remove: "Remove user",
    addAll: "Add all users",
    removeAll: "Remove all users",
  },
};

export const UpdateLocalStateOnChange = (args) => {
  const [availableItems, setAvailableItems] = React.useState([
    {
      label: "Item 1",
      description: "This is a description for Item 1",
    },
    {
      label: "Item 2",
      description: "This is a description for Item 2",
    },
  ]);
  const [selectedItems, setSelectedItems] = React.useState([
    {
      label: "Item 3",
      description: "This is a description for Item 3",
    },
    {
      label: "Item 4",
      description: "This is a description for Item 4",
    },
  ]);
  const transferListRef = useRef<any>(null);

  return (
    <>
      <TransferListSearch
        style={{
          width: "100%",
        }}
        placeholder="Search"
        onChange={(e) =>
          transferListRef &&
          transferListRef.current &&
          transferListRef.current.api.searchLists(e.target.value)
        }
      />
      <Small as="p">
        This is an example of an element that is shown below the search.
      </Small>
      <TransferList
        {...args}
        id="TransferUpdateLocalStateOnChange"
        ref={transferListRef}
        itemFormatter={(item) => {
          return (
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "repeat(2, 1fr)",
                gap: "12px",
              }}
            >
              <div>{item.label}</div>
              <div>
                <Small>{item.description}</Small>
              </div>
            </div>
          );
        }}
        availableItems={availableItems}
        selectedItems={selectedItems}
        onChange={({ availableItems, selectedItems }: any) => {
          setAvailableItems(availableItems);
          setSelectedItems(selectedItems);
        }}
      />

      <div className="mt-3">
        {" "}
        {JSON.stringify({ availableItems, selectedItems })}
      </div>
    </>
  );
};

export const ComponentAPIAccess = (args) => {
  const transferListRef = useRef<TransferListRefProps | null>(null);
  const transferListSearch = useRef<any>(null);
  return (
    <>
      <div className="mb-3">
        <Button
          dataLwtId=""
          onClick={() =>
            transferListSearch.current && transferListSearch.current.focus()
          }
        >
          Focus Search
        </Button>
        <Button
          dataLwtId=""
          onClick={() => {
            transferListRef.current &&
              transferListRef.current.elements.availableList.focus();
          }}
        >
          Focus Available List
        </Button>

        <Button
          dataLwtId=""
          onClick={() => {
            transferListRef.current &&
              transferListRef.current.elements.selectedList.focus();
          }}
        >
          Focus Selected List
        </Button>
      </div>
      <div className="mb-3">
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.elements.addButton.focus()
          }
        >
          Focus Add Button
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.elements.removeButton.focus()
          }
        >
          Focus Remove Button
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.elements.addAllButton.focus()
          }
        >
          Focus Add All Button
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.elements.removeAllButton.focus()
          }
        >
          Focus Remove All Button
        </Button>
      </div>
      <div className="mb-3">
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.selectAllAvailableItems()
          }
        >
          Select All Available Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.selectAllSelectedItems()
          }
        >
          Select All Selected Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.deselectAllAvailableItems()
          }
        >
          Deselect All Available Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.deselectAllSelectedItems()
          }
        >
          Deselect All Selected Items
        </Button>
      </div>
      <div className="mb-3">
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.selectAvailableItems([0, 2])
          }
        >
          Select Available Items 0 and 2
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.selectSelectedItems([0, 1])
          }
        >
          Select Selected Items 0 and 1
        </Button>
        <Button
          dataLwtId=""
          onClick={() => {
            const availableItems =
              transferListRef.current &&
              transferListRef.current.api.getAvailableItems();
            const selectedItems =
              transferListRef.current &&
              transferListRef.current.api.getSelectedItems();

            // get the index of all items that have an odd number in label string
            const availableOddIndexes = availableItems?.reduce(
              (acc: number[], item, index) => {
                // find the index of the item that has an odd number in label string
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                if (
                  item.label.match(/\d+/g) &&
                  parseInt(item.label.match(/\d+/g)![0]) % 2 !== 0
                ) {
                  acc.push(index);
                }

                return acc;
              },
              []
            );

            // get the index of all items that have an odd number in label string
            const selectedOddIndexes = selectedItems?.reduce(
              (acc: number[], item, index) => {
                // find the index of the item that has an odd number in label string
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                if (
                  item.label.match(/\d+/g) &&
                  parseInt(item.label.match(/\d+/g)![0]) % 2 !== 0
                ) {
                  acc.push(index);
                }
                return acc;
              },
              []
            );

            transferListRef.current &&
              transferListRef.current.api.selectAvailableItems(
                availableOddIndexes as number[]
              );
            transferListRef.current &&
              transferListRef.current.api.selectSelectedItems(
                selectedOddIndexes as number[]
              );
          }}
        >
          Select All Items that are odd
        </Button>
        <Button
          dataLwtId=""
          onClick={() => {
            console.log(
              transferListRef.current?.api.getCheckedAvailableItems()
            );
          }}
        >
          Console Log Checked Available Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() => {
            console.log(transferListRef.current?.api.getCheckedSelectedItems());
          }}
        >
          Console Log Checked Selected Items
        </Button>
      </div>
      <div className="mb-3">
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current && transferListRef.current.api.addItems()
          }
        >
          Add Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current && transferListRef.current.api.addAllItems()
          }
        >
          Add All Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current && transferListRef.current.api.removeItems()
          }
        >
          Remove Items
        </Button>
        <Button
          dataLwtId=""
          onClick={() =>
            transferListRef.current &&
            transferListRef.current.api.removeAllItems()
          }
        >
          Remove All Items
        </Button>
      </div>

      <TransferListSearch
        placeholder="Search"
        ref={transferListSearch}
        style={{
          marginBottom: "8px",
          width: "100%",
        }}
      />
      <TransferList
        {...args}
        id="TransferFocusElements"
        ref={transferListRef}
      />
    </>
  );
};

ComponentAPIAccess.args = {
  availableItems: [
    { label: "Item 1" },
    { label: "Item 2" },
    { label: "Item 3" },
  ],
  selectedItems: [
    { label: "Item 4" },
    { label: "Item 5" },
    { label: "Item 6" },
  ],
};
