/* eslint-disable react/prop-types */
import React, { useImperativeHandle, useContext, useEffect, useState, forwardRef } from 'react';
import TransferItemList from './TransferItemList';
import { ItemProps, TransferListProps, TransferListRefProps } from './TransferList';
import {
    StyledButton,
    StyledButtonControlWrapper,
    StyledItemListWrapper,
    StyledListLabel,
    StyledListSeparator,
    StyledTransferIcon,
    StyledTransferList
} from './TransferList.styles';
import { TransferListContext } from './TransferListContext';

const TransferListInner = forwardRef<TransferListRefProps, TransferListProps>(
    (
        {
            availableItems,
            selectedItems,
            itemFormatter,
            useBuiltInFocusStyle = true,
            messages = {
                noSelectedItems: 'No selected items',
                noAvailableItems: 'No available items',
                noSelectedItemsSearch: 'No selected items matching your search',
                noAvailableItemsSearch: 'No available items matching your search'
            },
            labels,
            onChange,
            id
        },
        ref
    ) => {
        const { transferListState, dispatchTransferList } = useContext(TransferListContext);
        const [availableList, setAvailableList] = useState<HTMLDivElement>();
        const [selectedList, setSelectedList] = useState<HTMLDivElement>();
        const [addButton, setAddButton] = useState<HTMLButtonElement>();
        const [removeButton, setRemoveButton] = useState<HTMLButtonElement>();
        const [addAllButton, setAddAllButton] = useState<HTMLButtonElement>();
        const [removeAllButton, setRemoveAllButton] = useState<HTMLButtonElement>();
        const defaultLabels = {
            availableItems: 'Available Items',
            selectedItems: 'Selected Items',
            add: 'Add',
            addAll: 'Add all',
            remove: 'Remove',
            removeAll: 'Remove all'
        };

        const defaultMessages = {
            noSelectedItems: 'No selected items.',
            noAvailableItems: 'No available items',
            noSelectedItemsSearch: 'No selected items matching your search.',
            noAvailableItemsSearch: 'No available items matching your search'
        };
        useImperativeHandle(
            ref,
            () => ({
                api: {
                    addItems: () => {
                        addItems();
                    },
                    removeItems: () => {
                        removeItems();
                    },
                    addAllItems: () => {
                        addAllItems();
                    },
                    removeAllItems: () => {
                        removeAllItems();
                    },
                    selectAllAvailableItems: () => {
                        selectAllAvailableItems();
                    },
                    selectAllSelectedItems: () => {
                        selectAllSelectedItems();
                    },
                    deselectAllAvailableItems: () => {
                        deselectAllAvailableItems();
                    },
                    deselectAllSelectedItems: () => {
                        deselectAllSelectedItems();
                    },
                    selectAvailableItems: (indexes: number[]) => {
                        selectAvailableItems(indexes);
                    },
                    selectSelectedItems: (indexes: number[]) => {
                        selectSelectedItems(indexes);
                    },
                    searchLists: (searchString: string) => {
                        searchLists(searchString);
                    },
                    getAvailableItems: () => {
                        return transferListState.availableItems;
                    },
                    getSelectedItems: () => {
                        return transferListState.selectedItems;
                    },
                    getCheckedAvailableItems: () => {
                        return transferListState.checkedAvailableItems;
                    },
                    getCheckedSelectedItems: () => {
                        return transferListState.checkedSelectedItems;
                    }
                },
                elements: {
                    availableList: availableList as HTMLDivElement,
                    selectedList: selectedList as HTMLDivElement,
                    addButton: addButton as HTMLButtonElement,
                    removeButton: removeButton as HTMLButtonElement,
                    addAllButton: addAllButton as HTMLButtonElement,
                    removeAllButton: removeAllButton as HTMLButtonElement
                }
            }),
            [availableList, selectedList, transferListState]
        );

        const checkSelectedItems = (e: React.ChangeEvent<HTMLInputElement>, item: ItemProps) => {
            e.currentTarget.checked
                ? dispatchTransferList({ type: 'ADD_SELECTED_CHECKED_ITEMS', payload: item })
                : dispatchTransferList({ type: 'REMOVE_SELECTED_CHECKED_ITEMS', payload: item });
        };

        const checkAvailableItems = (e: React.ChangeEvent<HTMLInputElement>, item: ItemProps) => {
            e.currentTarget.checked
                ? dispatchTransferList({ type: 'ADD_AVAILABLE_CHECKED_ITEMS', payload: item })
                : dispatchTransferList({ type: 'REMOVE_AVAILABLE_CHECKED_ITEMS', payload: item });
        };

        const addItems = () => {
            dispatchTransferList({ type: 'ADD_ITEMS', payload: transferListState.checkedAvailableItems });
        };

        const addAllItems = () => {
            dispatchTransferList({ type: 'ADD_ITEMS', payload: transferListState.availableItems });
        };

        const removeItems = () => {
            dispatchTransferList({ type: 'REMOVE_ITEMS', payload: transferListState.checkedSelectedItems });
        };

        const removeAllItems = () => {
            dispatchTransferList({ type: 'REMOVE_ITEMS', payload: transferListState.selectedItems });
        };

        const searchLists = (searchString: string) => {
            dispatchTransferList({ type: 'SET_SEARCH_TEXT', payload: searchString });
        };

        const selectAllAvailableItems = () => {
            dispatchTransferList({ type: 'SELECT_ALL_AVAILABLE_ITEMS' });
        };

        const selectAllSelectedItems = () => {
            dispatchTransferList({ type: 'SELECT_ALL_SELECTED_ITEMS' });
        };

        const deselectAllAvailableItems = () => {
            dispatchTransferList({ type: 'DESELECT_ALL_AVAILABLE_ITEMS' });
        };

        const deselectAllSelectedItems = () => {
            dispatchTransferList({ type: 'DESELECT_ALL_SELECTED_ITEMS' });
        };

        const selectAvailableItems = (indexes: number[]) => {
            dispatchTransferList({ type: 'SELECT_AVAILABLE_ITEMS', payload: indexes });
        };

        const selectSelectedItems = (indexes: number[]) => {
            dispatchTransferList({ type: 'SELECT_SELECTED_ITEMS', payload: indexes });
        };

        const availableItemsList: ItemProps[] =
            transferListState.availableItems && transferListState.availableItems.filter
                ? transferListState.availableItems.filter(
                      (item: ItemProps) =>
                          item.label
                              .split(' ')
                              .some((word: string) =>
                                  word.toLowerCase().startsWith(transferListState.searchText.toLowerCase().trim())
                              ) ||
                          item.label.toLowerCase().startsWith(transferListState.searchText.toLowerCase().trim())
                  )
                : [];

        const selectedItemsList: ItemProps[] =
            transferListState.selectedItems && transferListState.selectedItems.filter
                ? transferListState.selectedItems.filter(
                      (item: ItemProps) =>
                          item.label
                              .split(' ')
                              .some((word: string) =>
                                  word.toLowerCase().startsWith(transferListState.searchText.toLowerCase().trim())
                              ) ||
                          item.label.toLowerCase().startsWith(transferListState.searchText.toLowerCase().trim())
                  )
                : [];

        useEffect(() => {
            dispatchTransferList({ type: 'SET_AVAILABLE_ITEMS', payload: availableItems });
        }, [availableItems]);

        useEffect(() => {
            dispatchTransferList({ type: 'SET_SELECTED_ITEMS', payload: selectedItems });
        }, [selectedItems]);

        useEffect(() => {
            onChange && dispatchTransferList({ type: 'SET_ON_CHANGE', payload: onChange });
        }, [onChange]);

        return (
            <StyledTransferList>
                <StyledItemListWrapper>
                    <StyledListLabel id={`${id}-available-label`}>
                        {labels && labels.availableItems ? labels.availableItems : defaultLabels.availableItems}
                    </StyledListLabel>
                    <TransferItemList
                        ref={(ref) => setAvailableList(ref as HTMLDivElement)}
                        id={id}
                        items={availableItemsList}
                        useBuiltInFocusStyle={useBuiltInFocusStyle}
                        itemFormatter={itemFormatter}
                        messages={messages}
                        defaultMessages={defaultMessages}
                        checkItems={(e, item) => checkAvailableItems(e, item)}
                    />
                    <StyledButtonControlWrapper>
                        <StyledButton
                            ref={(ref) => setAddButton(ref as HTMLButtonElement)}
                            disabled={transferListState.checkedAvailableItems.length === 0}
                            onClick={() => addItems()}
                        >
                            <span>{labels && labels.add ? labels.add : defaultLabels.add}</span>
                            <svg
                                width='20'
                                height='20'
                                viewBox='0 0 20 20'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                            >
                                <path
                                    fillRule='evenodd'
                                    clipRule='evenodd'
                                    // eslint-disable-next-line max-len
                                    d='M7.46967 14.5303C7.17678 14.2374 7.17678 13.7626 7.46967 13.4697L10.9393 10L7.46967 6.53033C7.17678 6.23744 7.17678 5.76256 7.46967 5.46967C7.76256 5.17678 8.23744 5.17678 8.53033 5.46967L12.5303 9.46967C12.671 9.61032 12.75 9.80109 12.75 10C12.75 10.1989 12.671 10.3897 12.5303 10.5303L8.53033 14.5303C8.23744 14.8232 7.76256 14.8232 7.46967 14.5303Z'
                                    fill='#4B5563'
                                />
                            </svg>
                        </StyledButton>
                        <StyledButton
                            ref={(ref) => setAddAllButton(ref as HTMLButtonElement)}
                            disabled={
                                transferListState.availableItems ? transferListState.availableItems.length === 0 : true
                            }
                            onClick={() => addAllItems()}
                        >
                            <span>{labels && labels.addAll ? labels.addAll : defaultLabels.addAll}</span>
                            <svg
                                width='20'
                                height='20'
                                viewBox='0 0 20 20'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                            >
                                <path
                                    fillRule='evenodd'
                                    clipRule='evenodd'
                                    // eslint-disable-next-line max-len
                                    d='M7.46967 14.5303C7.17678 14.2374 7.17678 13.7626 7.46967 13.4697L10.9393 10L7.46967 6.53033C7.17678 6.23744 7.17678 5.76256 7.46967 5.46967C7.76256 5.17678 8.23744 5.17678 8.53033 5.46967L12.5303 9.46967C12.671 9.61032 12.75 9.80109 12.75 10C12.75 10.1989 12.671 10.3897 12.5303 10.5303L8.53033 14.5303C8.23744 14.8232 7.76256 14.8232 7.46967 14.5303Z'
                                    fill='#4B5563'
                                />
                            </svg>
                        </StyledButton>
                    </StyledButtonControlWrapper>
                </StyledItemListWrapper>
                <StyledListSeparator>
                    <StyledTransferIcon>
                        <svg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <path
                                fillRule='evenodd'
                                clipRule='evenodd'
                                // eslint-disable-next-line max-len
                                d='M4.24999 8C4.24999 8.41421 4.58578 8.75 4.99999 8.75H16C16.3033 8.75 16.5768 8.56727 16.6929 8.28701C16.809 8.00676 16.7448 7.68417 16.5303 7.46967L12.5303 3.46967C12.2374 3.17678 11.7626 3.17678 11.4697 3.46967C11.1768 3.76256 11.1768 4.23744 11.4697 4.53033L14.1893 7.25H4.99999C4.58578 7.25 4.24999 7.58579 4.24999 8Z'
                                fill='#4B5563'
                            />
                            <path
                                fillRule='evenodd'
                                clipRule='evenodd'
                                // eslint-disable-next-line max-len
                                d='M5.81065 12.75L15 12.75C15.4142 12.75 15.75 12.4142 15.75 12C15.75 11.5858 15.4142 11.25 15 11.25L3.99999 11.25C3.69664 11.25 3.42317 11.4327 3.30708 11.713C3.191 11.9932 3.25516 12.3158 3.46966 12.5303L7.46966 16.5303C7.76255 16.8232 8.23743 16.8232 8.53032 16.5303C8.82321 16.2374 8.82321 15.7626 8.53032 15.4697L5.81065 12.75Z'
                                fill='#4B5563'
                            />
                        </svg>
                    </StyledTransferIcon>
                </StyledListSeparator>
                <StyledItemListWrapper>
                    <StyledListLabel id={`${id}-selected-label`}>
                        {labels && labels.selectedItems ? labels.selectedItems : defaultLabels.selectedItems}
                    </StyledListLabel>
                    <TransferItemList
                        selectedList
                        ref={(ref) => setSelectedList(ref as HTMLDivElement)}
                        id={id}
                        items={selectedItemsList}
                        useBuiltInFocusStyle={useBuiltInFocusStyle}
                        itemFormatter={itemFormatter}
                        messages={messages}
                        defaultMessages={defaultMessages}
                        checkItems={(e, item) => checkSelectedItems(e, item)}
                    />
                    <StyledButtonControlWrapper>
                        <StyledButton
                            ref={(ref) => setRemoveButton(ref as HTMLButtonElement)}
                            disabled={transferListState.checkedSelectedItems.length === 0}
                            onClick={() => removeItems()}
                        >
                            <svg
                                width='20'
                                height='20'
                                viewBox='0 0 20 20'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                            >
                                <path
                                    fillRule='evenodd'
                                    clipRule='evenodd'
                                    // eslint-disable-next-line max-len
                                    d='M12.5303 5.46967C12.8232 5.76256 12.8232 6.23744 12.5303 6.53033L9.06066 10L12.5303 13.4697C12.8232 13.7626 12.8232 14.2374 12.5303 14.5303C12.2374 14.8232 11.7626 14.8232 11.4697 14.5303L7.46967 10.5303C7.32902 10.3897 7.25 10.1989 7.25 10C7.25 9.80109 7.32902 9.61032 7.46967 9.46967L11.4697 5.46967C11.7626 5.17678 12.2374 5.17678 12.5303 5.46967Z'
                                    fill='#4B5563'
                                />
                            </svg>
                            <span>{labels && labels.remove ? labels.remove : defaultLabels.remove}</span>
                        </StyledButton>
                        <StyledButton
                            ref={(ref) => setRemoveAllButton(ref as HTMLButtonElement)}
                            disabled={
                                transferListState.selectedItems ? transferListState.selectedItems.length === 0 : true
                            }
                            onClick={() => removeAllItems()}
                        >
                            <svg
                                width='20'
                                height='20'
                                viewBox='0 0 20 20'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                            >
                                <path
                                    fillRule='evenodd'
                                    clipRule='evenodd'
                                    // eslint-disable-next-line max-len
                                    d='M12.5303 5.46967C12.8232 5.76256 12.8232 6.23744 12.5303 6.53033L9.06066 10L12.5303 13.4697C12.8232 13.7626 12.8232 14.2374 12.5303 14.5303C12.2374 14.8232 11.7626 14.8232 11.4697 14.5303L7.46967 10.5303C7.32902 10.3897 7.25 10.1989 7.25 10C7.25 9.80109 7.32902 9.61032 7.46967 9.46967L11.4697 5.46967C11.7626 5.17678 12.2374 5.17678 12.5303 5.46967Z'
                                    fill='#4B5563'
                                />
                            </svg>
                            <span>{labels && labels.removeAll ? labels.removeAll : defaultLabels.removeAll}</span>
                        </StyledButton>
                    </StyledButtonControlWrapper>
                </StyledItemListWrapper>
            </StyledTransferList>
        );
    }
);

TransferListInner.displayName = 'TransferListInner';

export default TransferListInner;
