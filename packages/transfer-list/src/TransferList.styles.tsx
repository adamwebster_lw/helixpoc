/* eslint-disable max-len */
import styled, { css } from "styled-components";

export const StyledTransferList = styled.div`
  display: flex;
  width: 100%;
  @media screen and (max-width: 576px) {
    flex-direction: column;
  }
`;

export const StyledItemListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1 1;
`;

interface StyledItemListProps {
  useBuiltInFocusStyle?: boolean;
}
export const StyledItemList = styled.div<StyledItemListProps>`
  display: flex;
  flex-direction: column;
  position: relative;
  flex: 1 1 auto;
  height: 300px;
  overflow: auto;
  padding: 8px;
  border-radius: 6px;
  ${({ useBuiltInFocusStyle }) =>
    useBuiltInFocusStyle &&
    css`
      &:focus {
        outline: none;
        box-shadow: 0 0 0 2px #2563eb;
        border: none;
      }
    `}

  @media screen and (max-width: 576px) {
    height: 200px;
  }
`;

interface StyledItemListItemProps {
  isSelected: boolean;
}

export const StyledListItem = styled.div<StyledItemListItemProps>`
  padding: 6px;
  border-radius: 6px;
  font-size: 14px;
  .helix-checkbox {
    display: flex;
    align-items: center;
    label {
      width: 100%;
    }
  }
  &:hover {
    background-color: #f3f4f6;
  }
  ${({ isSelected }) =>
    isSelected &&
    css`
      background-color: #f3f4f6;
    `}
`;

export const StyledListSeparator = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex: 0 0 20px;
  position: relative;
  margin: 0 12px;
  &::before {
    content: "";
    width: 1px;
    height: calc(50% - 20px);
    background-color: #e0e0e0;
    left: 10px;
    top: 0;
    position: absolute;
  }
  &::after {
    content: "";
    width: 1px;
    height: calc(50% - 20px);
    background-color: #e0e0e0;
    left: 10px;
    bottom: 0;
    position: absolute;
  }
  @media screen and (max-width: 576px) {
    margin: 12px 0;

    svg {
      transform: rotate(90deg);
    }
    &::before,
    &::after {
      height: 1px;
      width: calc(50% - 20px);
    }
    &::before {
      left: 0;
      top: 10px;
    }
    &::after {
      right: 0;
      bottom: 10px;
      left: auto;
    }
  }
`;

export const StyledButtonControlWrapper = styled.div`
  margin-top: 16px;
  display: flex;
  flex-wrap: wrap;
  button + button {
    margin-left: 8px;
  }
`;

export const StyledTransferIcon = styled.div`
  display: flex;
  width: 16px;
  height: 16px;
  align-items: center;
  justify-content: center;
`;

export const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #e5e7eb;
  color: #4b5563;
  font-size: 14px;
  font-weight: 600;
  border: none;
  border-radius: 6px;
  padding: 4px 8px;
  &:hover:not(:disabled) {
    background-color: #f3f4f6;
  }
  &:focus {
    outline: solid 2px #2563eb;
  }
  &:disabled {
    cursor: default !important;
    opacity: 0.5;
  }
  svg:last-child {
    margin-left: 8px;
  }
  svg:first-child {
    margin-right: 8px;
  }
`;

interface StyledTransferListSearchProps {
  useBuiltInFocusStyle?: boolean;
}
export const StyledTransferListSearch = styled.input<StyledTransferListSearchProps>`
  border: 1px solid #d1d5db;
  border-radius: 6px;
  padding: 8px 12px 8px 36px;
  font-size: 14px;
  line-height: 20px;
  max-height: 36px;
  background-image: url("data:image/svg+xml,%3Csvg width='20' height='20' viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M9 3.75C6.10051 3.75 3.75 6.10051 3.75 9C3.75 11.8995 6.10051 14.25 9 14.25C11.8995 14.25 14.25 11.8995 14.25 9C14.25 6.10051 11.8995 3.75 9 3.75ZM2.25 9C2.25 5.27208 5.27208 2.25 9 2.25C12.7279 2.25 15.75 5.27208 15.75 9C15.75 10.5938 15.1976 12.0585 14.2739 13.2133L17.0303 15.9697C17.3232 16.2626 17.3232 16.7374 17.0303 17.0303C16.7374 17.3232 16.2626 17.3232 15.9697 17.0303L13.2133 14.2739C12.0585 15.1976 10.5938 15.75 9 15.75C5.27208 15.75 2.25 12.7279 2.25 9Z' fill='%236B7280'/%3E%3C/svg%3E%0A");
  background-repeat: no-repeat;
  background-position: left 8px center;
  box-sizing: border-box;
  ${({ useBuiltInFocusStyle }) =>
    useBuiltInFocusStyle &&
    css`
      &:focus {
        box-shadow: 0 0 0 2px #2563eb;
        border-radius: 6px;
        outline: none;
      }
    `}
`;

export const StyledEmptyState = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  padding: 16px;
  flex: 1 1;
`;

export const StyledListLabel = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  margin-bottom: 6px;
`;
