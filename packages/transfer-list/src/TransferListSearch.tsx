import React from "react";
import { StyledTransferListSearch } from "./TransferList.styles";

interface TransferListSearchProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  /** @ignore */
  className?: string;
  /** @ignore */
  useBuiltInFocusStyle?: boolean;
}

const TransferListSearch = React.forwardRef<
  HTMLInputElement,
  TransferListSearchProps
>(({ className, useBuiltInFocusStyle = true, ...rest }, ref) => {
  return (
    <StyledTransferListSearch
      useBuiltInFocusStyle={useBuiltInFocusStyle}
      className={`${className}`}
      ref={ref}
      {...rest}
    />
  );
});

TransferListSearch.displayName = "TransferListSearch";
export default TransferListSearch;
