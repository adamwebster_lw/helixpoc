/* eslint-disable react/display-name */
import React, { ReactNode, forwardRef } from 'react';
import { TransferListProvider } from './TransferListContext';
import TransferListInner from './TransferListInner';

export interface ItemProps {
    label: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
}

export interface TransferListRefProps {
    api: {
        addItems: () => void;
        addAllItems: () => void;
        removeItems: () => void;
        removeAllItems: () => void;
        searchLists: (searchString: string) => void;
        selectAllAvailableItems: () => void;
        selectAllSelectedItems: () => void;
        deselectAllAvailableItems: () => void;
        deselectAllSelectedItems: () => void;
        selectAvailableItems: (indexes: number[]) => void;
        selectSelectedItems: (indexes: number[]) => void;
        getAvailableItems: () => ItemProps[];
        getSelectedItems: () => ItemProps[];
        getCheckedAvailableItems: () => void;
        getCheckedSelectedItems: () => void;
    };
    elements: {
        availableList: HTMLDivElement;
        selectedList: HTMLDivElement;
        addButton: HTMLButtonElement;
        removeButton: HTMLButtonElement;
        addAllButton: HTMLButtonElement;
        removeAllButton: HTMLButtonElement;
    };
}

export interface TransferListMessages {
    noSelectedItems?: ReactNode;
    noAvailableItems?: ReactNode;
    noSelectedItemsSearch?: ReactNode;
    noAvailableItemsSearch?: ReactNode;
}

export interface TransferListProps {
    /** Sets the id of the lists and the items.  Should be a unique value.  Required for accessibility. */
    id: string;
    /** Any array of items to show in the available list. */
    availableItems: ItemProps[];
    /** Any array of items to show in the selected list. */
    selectedItems: ItemProps[];
    /** A function that formats the items in the lists. */
    itemFormatter?: (item: ItemProps) => ReactNode;
    /** Customize the default messages shown in the transfer list. */
    messages?: TransferListMessages;
    /** Customize the default labels shown in the transfer list. */
    labels?: {
        availableItems?: ReactNode;
        selectedItems?: ReactNode;
        add?: string;
        addAll?: string;
        remove?: string;
        removeAll?: string;
    };
    onChange?: (values: { availableItems: ItemProps[]; selectedItems: ItemProps[] }) => void;
    useBuiltInFocusStyle?: boolean;
}

const TransferList = forwardRef<TransferListRefProps, TransferListProps>((props, ref) => {
    return (
        <TransferListProvider>
            <TransferListInner ref={ref} {...props} />
        </TransferListProvider>
    );
});

TransferList.displayName = 'TransferList';

export default TransferList;
