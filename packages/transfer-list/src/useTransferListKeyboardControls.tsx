import { useState } from 'react';

const useTransferListKeyboardControls = (list, items) => {
    const [selectedItem, setSelectedItem] = useState(-1);

    const handleBlur = () => {
        setSelectedItem(-1);
    };

    const handleKeyDown = (e) => {
        // filter out nulls from the items
        const filteredItems: HTMLElement[] = items.filter((item) => item);
        switch (e.key) {
            case 'ArrowDown': {
                e.preventDefault();
                // increase selectedAvailableItem by 1
                setSelectedItem(selectedItem + 1);

                // if the next filteredItem is not in view in the containing div down the height of the item
                if (
                    filteredItems[selectedItem + 1] &&
                    filteredItems[selectedItem + 1].offsetTop + filteredItems[selectedItem + 1].offsetHeight >
                        list.scrollTop + list.offsetHeight
                ) {
                    // scroll the containing div down by the height of the item
                    list.scrollTop += filteredItems[selectedItem + 1].offsetHeight;
                }
                // if selectedAvailableItem is greater than the length of availableItemElements, set it to 0
                if (selectedItem >= filteredItems.length - 1) {
                    // scroll to the top of the list
                    setSelectedItem(0);
                    if (list) {
                        list.scrollTop = 0;
                    }
                }

                break;
            }
            case 'ArrowUp': {
                e.preventDefault();
                // decrease selectedAvailableItem by 1
                setSelectedItem(selectedItem - 1);
                // if the previous filteredItem is not in view in the containing div up the height of the item
                if (filteredItems[selectedItem - 1] && filteredItems[selectedItem - 1].offsetTop < list.scrollTop) {
                    // scroll the containing div up by the height of the item
                    list.scrollTop -= filteredItems[selectedItem - 1].offsetHeight;
                }
                // if on the first item, set it to the last item
                if (selectedItem <= 0) {
                    setSelectedItem(filteredItems.length - 1);
                    // scroll to the bottom of the list
                    list.scrollTop = list.scrollHeight;
                }
                break;
            }
            case 'Enter':
            case ' ': {
                e.preventDefault();
                // Find the label element of the selectedAvailableItem
                const selectedAvailableItemLabel = filteredItems[selectedItem].querySelector('label');
                // If the label element exists, click it
                if (selectedAvailableItemLabel) {
                    selectedAvailableItemLabel.click();
                }
                // focus the list again
                list && list.focus();
                setSelectedItem(selectedItem);
                break;
            }
            case 'Home': {
                e.preventDefault();
                // set selectedAvailableItem to 0
                setSelectedItem(0);
                // scroll to the top of the list
                if (list) {
                    list.scrollTop = 0;
                }
                break;
            }
            case 'End': {
                e.preventDefault();
                // set selectedAvailableItem to the length of availableItemElements
                setSelectedItem(filteredItems.length - 1);
                // scroll to the bottom of the list
                if (list) {
                    list.scrollTop = list.scrollHeight;
                }
                break;
            }
            default:
                break;
        }
    };

    return { handleKeyDown, handleBlur, selectedItem, setSelectedItem };
};

export default useTransferListKeyboardControls;
