import React, { useState, useContext, useRef, useEffect } from "react";
import { ItemProps, TransferListMessages } from "./TransferList";
import {
  StyledListItem,
  StyledItemList,
  StyledEmptyState,
} from "./TransferList.styles";
import { Checkbox } from "@lwt-helix-nextgen/checkbox";
import { TransferListContext } from "./TransferListContext";
import useTransferListKeyboardControls from "./useTransferListKeyboardControls";

interface TransferListItemProps {
  items?: ItemProps[];
  id?: string;
  useBuiltInFocusStyle?: boolean;
  itemFormatter?: (item: ItemProps) => React.ReactNode;
  messages?: TransferListMessages;
  defaultMessages?: {
    [key: string]: React.ReactNode;
  };
  checkItems?: (e, items) => void;
  selectedList?: boolean;
}
const TransferItemList = React.forwardRef<
  HTMLDivElement,
  TransferListItemProps
>(
  (
    {
      items = [],
      id,
      useBuiltInFocusStyle,
      itemFormatter,
      messages,
      checkItems,
      selectedList = false,
      defaultMessages,
    },
    ref
  ) => {
    const [itemsList, setItemList] = useState<HTMLDivElement | null>(null);
    const [itemElements, setItemElements] = useState<any[]>([]);
    const [focusTrigger, setFocusTrigger] = useState("mouse");
    const itemsRef = useRef<any[]>([]);
    const { transferListState } = useContext(TransferListContext);

    const { handleKeyDown, selectedItem, handleBlur, setSelectedItem } =
      useTransferListKeyboardControls(itemsList, itemElements);

    React.useImperativeHandle(ref, () => itemsList as HTMLDivElement, [
      itemsList,
    ]);

    const handleFocusList = (e: React.FocusEvent<HTMLDivElement>) => {
      // if not focused by mouse, set the selected item to the first item
      if (focusTrigger === "tab" && e.target === itemsList) {
        setSelectedItem(0);
      }
    };

    const handleMouseDown = (e: MouseEvent) => {
      setFocusTrigger("mouse");
    };
    const handleKeyDownDocument = (e: KeyboardEvent) => {
      if (e.key === "Tab") {
        setFocusTrigger("tab");
      }
    };
    useEffect(() => {
      setItemElements(itemsRef.current);
    }, [itemsRef]);

    useEffect(() => {
      document.addEventListener("mousedown", handleMouseDown);
      document.addEventListener("keydown", handleKeyDownDocument);
      return () => {
        document.removeEventListener("mousedown", handleMouseDown);
        document.removeEventListener("keydown", handleKeyDownDocument);
      };
    }, []);
    return (
      <StyledItemList
        ref={(ref) => setItemList(ref)}
        tabIndex={0}
        onKeyDown={(e) => handleKeyDown(e)}
        onBlur={() => handleBlur()}
        useBuiltInFocusStyle={useBuiltInFocusStyle}
        role="group"
        aria-labelledby={`${id}-${
          selectedList ? "selected" : "available"
        }-label`}
        onFocus={(e) => handleFocusList(e)}
        aria-activedescendant={
          items.length > 0 && items[selectedItem]
            ? `${id}-${
                selectedList ? "selected" : "available"
              }-list-item-${items.indexOf(items[selectedItem])}`
            : undefined
        }
      >
        {items.map((item: ItemProps, index) => (
          <StyledListItem
            onClick={() => {
              setSelectedItem(index);
            }}
            id={`${id}-${
              selectedList ? "selected" : "available"
            }-list-item-${index}`}
            isSelected={selectedItem === index}
            key={item.label}
            aria-checked={transferListState[
              selectedList ? "checkedSelectedItems" : "checkedAvailableItems"
            ].includes(item as never)}
            role="checkbox"
            ref={(ref) => {
              itemsRef.current[index] = ref;
            }}
          >
            <Checkbox
              checked={transferListState[
                selectedList ? "checkedSelectedItems" : "checkedAvailableItems"
              ].includes(item as never)}
              tabIndex={-1}
              onChange={(e) => checkItems && checkItems(e, item)}
              label={itemFormatter ? itemFormatter(item) : item.label}
            />
          </StyledListItem>
        ))}
        {items &&
          items.length === 0 &&
          transferListState[
            selectedList ? "selectedItems" : "availableItems"
          ] &&
          transferListState[selectedList ? "selectedItems" : "availableItems"]
            .length > 0 && (
            <StyledEmptyState>
              {messages &&
              messages[
                selectedList
                  ? "noSelectedItemsSearch"
                  : "noAvailableItemsSearch"
              ]
                ? messages[
                    selectedList
                      ? "noSelectedItemsSearch"
                      : "noAvailableItemsSearch"
                  ]
                : defaultMessages?.[
                    selectedList
                      ? "noSelectedItemsSearch"
                      : "noAvailableItemsSearch"
                  ]}
            </StyledEmptyState>
          )}
        {transferListState[selectedList ? "selectedItems" : "availableItems"] &&
          transferListState[selectedList ? "selectedItems" : "availableItems"]
            .length === 0 && (
            <StyledEmptyState>
              {messages &&
              messages[selectedList ? "noSelectedItems" : "noAvailableItems"]
                ? messages[
                    selectedList ? "noSelectedItems" : "noAvailableItems"
                  ]
                : defaultMessages?.[
                    selectedList ? "noSelectedItems" : "noAvailableItems"
                  ]}
            </StyledEmptyState>
          )}
      </StyledItemList>
    );
  }
);

TransferItemList.displayName = "TransferItemList";

export default TransferItemList;
