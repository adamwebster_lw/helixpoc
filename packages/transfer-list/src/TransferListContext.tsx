import React, { createContext, ReactNode, useReducer } from 'react';

interface initialStateProps {
    availableItems: any;
    selectedItems: any;
    checkedAvailableItems: any;
    checkedSelectedItems: any;
    searchText: string;
    onChange: (items: { availableItems: any; selectedItems: any }) => void;
}

const initialState = {
    availableItems: [],
    selectedItems: [],
    checkedAvailableItems: [],
    checkedSelectedItems: [],
    searchText: '',
    // eslint-disable-next-line @typescript-eslint/no-empty-function, no-empty-pattern
    onChange: ({ availableItems: [], selectedItems: [] }) => {}
};

interface dispatchValuesProps {
    type:
        | 'SET_AVAILABLE_ITEMS'
        | 'SET_SELECTED_ITEMS'
        | 'ADD_ITEMS'
        | 'REMOVE_ITEMS'
        | 'ADD_AVAILABLE_CHECKED_ITEMS'
        | 'REMOVE_AVAILABLE_CHECKED_ITEMS'
        | 'ADD_SELECTED_CHECKED_ITEMS'
        | 'REMOVE_SELECTED_CHECKED_ITEMS'
        | 'SET_SEARCH_TEXT'
        | 'SET_ON_CHANGE'
        | 'SELECT_ALL_AVAILABLE_ITEMS'
        | 'SELECT_ALL_SELECTED_ITEMS'
        | 'DESELECT_ALL_AVAILABLE_ITEMS'
        | 'DESELECT_ALL_SELECTED_ITEMS'
        | 'SELECT_AVAILABLE_ITEMS'
        | 'SELECT_SELECTED_ITEMS';
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    payload?: any;
}

export const TransferListContext = createContext({
    transferListState: initialState,
    dispatchTransferList: (value: dispatchValuesProps | void) => value
});

export const TransferListConsumer = TransferListContext.Consumer;

export const transferListReducer = (state: initialStateProps, action: dispatchValuesProps) => {
    switch (action.type) {
        case 'SET_AVAILABLE_ITEMS':
            return {
                ...state,
                availableItems: action.payload
            };
        case 'SET_SELECTED_ITEMS':
            return {
                ...state,
                selectedItems: action.payload
            };
        case 'ADD_ITEMS': {
            const selectedItems = [...state.selectedItems, ...action.payload];
            const availableItems = state.availableItems.filter((item) => !action.payload.includes(item));
            state.onChange && state.onChange({ availableItems, selectedItems });
            return {
                ...state,
                selectedItems,
                availableItems,
                checkedAvailableItems: []
            };
        }
        case 'REMOVE_ITEMS': {
            const selectedItems = state.selectedItems.filter((item) => !action.payload.includes(item));
            const availableItems = [...state.availableItems, ...action.payload];
            state.onChange && state.onChange({ availableItems, selectedItems });
            return {
                ...state,
                selectedItems,
                availableItems,
                checkedSelectedItems: []
            };
        }
        case 'ADD_AVAILABLE_CHECKED_ITEMS':
            return {
                ...state,
                checkedAvailableItems: [...state.checkedAvailableItems, action.payload]
            };
        case 'REMOVE_AVAILABLE_CHECKED_ITEMS':
            return {
                ...state,
                checkedAvailableItems: state.checkedAvailableItems.filter((item) => action.payload !== item)
            };
        case 'ADD_SELECTED_CHECKED_ITEMS':
            return {
                ...state,
                checkedSelectedItems: [...state.checkedSelectedItems, action.payload]
            };
        case 'REMOVE_SELECTED_CHECKED_ITEMS':
            return {
                ...state,
                checkedSelectedItems: state.checkedSelectedItems.filter((item) => action.payload !== item)
            };
        case 'SET_SEARCH_TEXT':
            return {
                ...state,
                searchText: action.payload
            };
        case 'SET_ON_CHANGE':
            return {
                ...state,
                onChange: action.payload
            };
        case 'SELECT_ALL_AVAILABLE_ITEMS':
            return {
                ...state,
                checkedAvailableItems: state.availableItems
            };

        case 'SELECT_ALL_SELECTED_ITEMS':
            return {
                ...state,
                checkedSelectedItems: state.selectedItems
            };
        case 'DESELECT_ALL_AVAILABLE_ITEMS':
            return {
                ...state,
                checkedAvailableItems: []
            };
        case 'DESELECT_ALL_SELECTED_ITEMS':
            return {
                ...state,
                checkedSelectedItems: []
            };
        case 'SELECT_AVAILABLE_ITEMS': {
            // get the selected items by index
            const availableItems = state.availableItems.map((item, index) => {
                if (action.payload.includes(index)) {
                    return item;
                }
                return null;
            });
            const availableItemsFiltered = availableItems.filter((item) => item !== null);
            return {
                ...state,
                checkedAvailableItems: availableItemsFiltered
            };
        }
        case 'SELECT_SELECTED_ITEMS': {
            // get the selected items by index
            const selectedItems = state.selectedItems.map((item, index) => {
                if (action.payload.includes(index)) {
                    return item;
                }
                return null;
            });

            // filter out nulls
            const selectedItemsFiltered = selectedItems.filter((item) => item !== null);

            return {
                ...state,
                checkedSelectedItems: selectedItemsFiltered
            };
        }
        default:
            return state;
    }
};

interface Props {
    children: ReactNode;
}

export const TransferListProvider = ({ children }: Props) => {
    const [transferListState, dispatchTransferList] = useReducer(transferListReducer, initialState);
    return (
        <TransferListContext.Provider value={{ transferListState, dispatchTransferList }}>
            {children}
        </TransferListContext.Provider>
    );
};
