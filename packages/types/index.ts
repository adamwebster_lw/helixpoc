export type HelixTypeColors = 'primary' | 'tertiary';
export type HelixButtonColors = 'primary' | 'secondary' | 'tertiary' | 'ghost';
