import React from 'react';
import Radio from './src/radio';

export default {
  title: 'Components/Radio',
  component: Radio,
};

const Template = (args: any) => (
  <>
    <Radio name="radio1" label="Radio 1" />
    <Radio name="radio1" label="Radio 2" disabled />
    <Radio name="radio1" label="Radio 2" />
  </>
);

export const Default = Template.bind({});

Default.args = {
  label: 'Radio',
};
