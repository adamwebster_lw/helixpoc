import styled from "styled-components";

export const StyledRadioButton = styled.div`
  &:focus-within {
    .radio-button {
      outline: 3px solid ${({ theme }) => theme.radio?.focusColor};
    }
  }
  .radio-button {
    width: 15px;
    height: 15px;
    position: relative;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin-right: 11px;
    background-color: ${({ theme }) => theme.radio?.backgroundColor};
    border-radius: 50%;
    border: solid 2px ${({ theme }) => theme.radio?.borderColor};
  }
  label {
    display: inline-flex;
    align-items: center;
  }
  input[type="radio"] {
    width: 0;
    height: 0;
    -webkit-appearance: none;
  }
  input[type="radio"]:checked + .radio-button {
    border-color: ${({ theme }) => theme.radio?.checkedBorderColor};
    &:before {
      content: "";
      position: absolute;
      width: 7px;
      height: 7px;
      border-radius: 50%;
      background-color: ${({ theme }) => theme.radio?.checkedBackgroundColor};
    }
  }
  input[type="radio"]:disabled + .radio-button,
  input[type="radio"]:disabled + .radio-button + .radio-label {
    filter: grayscale(1) opacity(0.5);
  }
`;
