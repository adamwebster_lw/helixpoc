import React, { HtmlHTMLAttributes, InputHTMLAttributes } from "react";
import { StyledRadioButton } from "./radio.styles";

interface RadioProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  /** If the input is checked or not */
  checked?: boolean;
  /** if the checkbox is disabled or not */
  disabled?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const Radio = React.forwardRef<HTMLInputElement, RadioProps>(
  ({ label, checked, disabled, dataLwtId, className, ...rest }, ref) => {
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }
    return (
      <StyledRadioButton
        className={`helix-radio ${className ? className : ""}`}
      >
        <label>
          <input
            checked={checked}
            disabled={disabled}
            ref={ref}
            type="radio"
            {...optionalProps}
            {...rest}
          />
          <div className="radio-button" />
          <span className="radio-label"> {label} </span>
        </label>
      </StyledRadioButton>
    );
  }
);

Radio.displayName = "Radio";
export default Radio;
