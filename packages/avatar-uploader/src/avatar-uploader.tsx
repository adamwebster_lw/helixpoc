import React, { useState, useRef } from "react";
import AvatarEditor from "react-avatar-editor";
import Dropzone from "react-dropzone";

const AvatarUploader = () => {
  const [scale, setScale] = useState(1);
  const [rotate, setRotate] = useState(0);
  const [imageScaledToCanvas, setImageScaledToCanvas] = useState<any>("");
  const [image, setImage] = useState("");
  const editorRef = useRef<any>();

  const dropZoneRef = useRef<any>();

  const handleDrop = (dropped) => {
    setImage(dropped[0]);
    setScale(1);
    setRotate(0);
  };

  return (
    <>
      <button onClick={() => dropZoneRef.current.open()}>Choose File</button>
      <Dropzone ref={dropZoneRef} onDrop={handleDrop} noClick noKeyboard>
        {({ getRootProps, getInputProps }) => (
          <div {...getRootProps()}>
            {image ? (
              <AvatarEditor
                image={image}
                width={250}
                height={250}
                border={50}
                color={[255, 255, 255, 0.6]} // RGBA
                scale={scale}
                rotate={rotate}
                ref={editorRef}
                borderRadius={125}
                crossOrigin="anonymous"
              />
            ) : (
              <div
                style={{
                  width: "250px",
                  height: "250px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#f2f2f2",
                  border: "dashed 1px #ccc",
                }}
              >
                Drag Profile Image Here
              </div>
            )}
            <input {...getInputProps()} />
          </div>
        )}
      </Dropzone>
      {image && (
        <>
          <label htmlFor="scale">Scale {(scale * 100).toFixed(0)}%</label>
          <input
            id="scale"
            type="range"
            min={100}
            max={300}
            value={scale * 100}
            onChange={(e) => setScale(parseInt(e.target.value) / 100)}
          />
          <label htmlFor="rotate">Rotate {rotate}&deg;</label>
          <input
            id="rotate"
            type="range"
            value={rotate}
            min={0}
            max={360}
            step={45}
            onChange={(e) => setRotate(parseInt(e.target.value))}
          />
          <button
            onClick={() => {
              console.log(
                editorRef.current!.getImage(),
                editorRef.current.getImageScaledToCanvas()
              );
              const canvas = editorRef.current.getImage().toDataURL();
              let imageURL;
              fetch(canvas)
                .then((res) => res.blob())
                .then((blob) =>
                  setImageScaledToCanvas(window.URL.createObjectURL(blob))
                );
            }}
          >
            Set Photo
          </button>
        </>
      )}
      {imageScaledToCanvas && (
        <div
          className="sample-image"
          style={{
            width: "128px",
            height: "128px",
            borderRadius: "50%",
            overflow: "hidden",
          }}
        >
          <img
            style={{ width: "128px", height: "128px" }}
            src={imageScaledToCanvas}
          />
        </div>
      )}
      <button
        onClick={() => {
          const url = "https:/google.com";
          fetch(url)
            .then((res) => res.text)
            .then((text) => console.log(text))
            .catch((err) => console.log(err));
        }}
      >
        test
      </button>
    </>
  );
};
export default AvatarUploader;
