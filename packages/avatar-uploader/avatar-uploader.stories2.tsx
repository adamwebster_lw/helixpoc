import React from "react";
import AvatarUploader from "./src/avatar-uploader";

export default {
  title: "Components/Avatar Uploader",
  component: AvatarUploader,
};

const Template = (args: any) => <AvatarUploader {...args} />;

export const Default = Template.bind({});
