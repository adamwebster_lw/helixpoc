import React, { HtmlHTMLAttributes } from "react";
import { UseSelectGetItemPropsOptions } from "downshift";

export interface DropdownMenuItemProps
  extends HtmlHTMLAttributes<HTMLLIElement> {
  menuItem: any;
  highlightedIndex: number;
  index: number;
  getItemProps: (props: UseSelectGetItemPropsOptions<any>) => any;
  itemFormatter?: (menuItem: any) => any;
}

const DropdownMenuItem = ({
  menuItem,
  highlightedIndex,
  index,
  getItemProps,
  itemFormatter,
  children,
  ...rest
}: DropdownMenuItemProps) => (
  <li
    className={`${index === highlightedIndex ? "highlighted" : ""}
    ${menuItem.disabled ? " disabled" : ""}
    `}
    {...rest}
    {...getItemProps({
      item: menuItem,
      disabled: menuItem.disabled,
      index,
    })}
  >
    <div className="menu-item-content-wrapper">{children}</div>
  </li>
);

export default DropdownMenuItem;
