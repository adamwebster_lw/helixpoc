import React, { HtmlHTMLAttributes, ReactNode } from 'react';
import { StyledDropdownMenu } from './dropdown-menu.styles';

interface Props extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  isOpen: boolean;
  maxWidth?: string;
  width?: string;
}
const DropdownMenu = React.forwardRef<HTMLDivElement, Props>(({ children, isOpen, maxWidth, width, ...rest }, ref) => {
  return (
    <StyledDropdownMenu ref={ref} isOpen={isOpen} maxWidth={maxWidth} width={width} {...rest}>
      {children}
    </StyledDropdownMenu>
  );
});

export default DropdownMenu;
