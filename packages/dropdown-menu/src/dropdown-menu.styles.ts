import styled, { css } from "styled-components";

interface StyledDropdownMenuProps {
  isOpen: boolean;
  maxWidth?: string;
  width?: string;
}
export const StyledDropdownMenu = styled.div<StyledDropdownMenuProps>`
  position: relative;
  background-color: ${({ theme }) => theme.dropdownMenu?.backgroundColor};
  border-radius: 4px;
  overflow: hidden;
  z-index: 1;
  ${({ width }) =>
    width &&
    css`
      width: ${width}!important;
    `}
  ${({ maxWidth }) =>
    maxWidth &&
    css`
      max-width: ${maxWidth};
    `}
  ${({ isOpen, theme }) =>
    isOpen &&
    css`
      box-shadow: ${({ theme }) => theme.dropdownMenu?.boxShadow};
      padding: 8px;
      li:not(.divider) {
        box-sizing: border-box;
        border-radius: 6px;
        color: inherit;
        display: block;
        text-decoration: none;
        margin-bottom: 2px;
        .menu-item-content-wrapper {
          padding: 8px;
          display: flex;
          align-items: center;
        }
        a {
          box-sizing: border-box;
          border-radius: 6px;
          color: inherit;
          width: 100%;
          display: block;
          text-decoration: none;
        }
        &.highlighted {
          background-color: ${theme.dropdownMenuItem?.highlightColor};
        }
      }
    `}
  &:focus-within {
    outline: none;
  }
  ul {
    padding: 0;
    margin: 0;
    list-style: none;
    &:focus-visible {
      outline: none;
    }
    li {
      &:not(.divider):not(.section-header):not(.disabled) {
        cursor: pointer;
      }
      &.disabled {
        .label,
        .description,
        svg {
          opacity: 0.3;
        }
      }
      &.selected-item {
        outline-width: 0;
        background-color: ${({ theme }) =>
          theme.dropdownMenuItem?.selectedColor};
      }
    }
    .divider {
      padding: 0;
      /* height: 0; */
      margin: 0;
      overflow: hidden;
      border-top: 1px solid ${({ theme }) => theme.dropdownMenu?.borderColor};
    }
    .section-header {
      text-transform: uppercase;
      font-size: 12px;
      color: ${({ theme }) => theme.dropdownMenu?.sectionHeaderColor};
      letter-spacing: 0.1em;
      padding-top: 0;
    }
    .description {
      color: ${({ theme }) => theme.dropdownMenuItem?.descriptionFontColor};
      font-size: 0.75rem;
    }
    + ul {
      margin-left: 16px;
      @media screen and (max-width: ${({ theme }) =>
          theme.breakPoints?.small}) {
        margin-left: 0;
        margin-top: 16px;
      }
    }
  }
  .empty-state {
    padding: 16px;
    text-align: center;
  }
`;
