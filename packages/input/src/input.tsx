import React, { InputHTMLAttributes } from "react";
import { StyledInput } from "./input.styles";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  /** if the input is disabled or not */
  disabled?: boolean;
  /** if the input is invalid or not */
  invalid?: boolean;
  /** the size of the input */
  inputSize?: "medium" | "large";
  /** If the input is valid or not */
  valid?: boolean;
  /** If the input is prefilled with a value */
  prefilled?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      disabled,
      dataLwtId,
      invalid,
      valid,
      prefilled,
      inputSize = "medium",
      className,
      ...rest
    },
    ref
  ) => {
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }

    return (
      <StyledInput
        disabled={disabled}
        prefilled={prefilled}
        className={`lwt-input ${className ? className : ""}`}
        ref={ref}
        inputSize={inputSize}
        invalid={invalid}
        valid={valid}
        {...optionalProps}
        {...rest}
      />
    );
  }
);

Input.displayName = "Input";
export default Input;
