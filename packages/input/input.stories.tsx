import React from 'react';
import Input from './src/input';

export default {
  title: 'Components/Input',
  component: Input,
};

const Template = (args: any) => (
  <>
    <Input {...args} />
  </>
);

export const Default = Template.bind({});

Default.args = {};
