import React from 'react';
import OverflowMenu from './src/overflow-menu';
import OverflowMenuItem from './src/overflow-menu-item';
import { Button } from '@lwt-helix-nextgen/button';
export default {
  title: 'Components/Overflow Menu',
  component: OverflowMenu,
};

const Template = (args: any) => (
  <>
    <OverflowMenu overflowButtonColor="ghost" dataLwtId="dataLwtId">
      <OverflowMenuItem label={1} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          1
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={2} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          2
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={3} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          3
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={4} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          4
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={5} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          5
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={6} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          6
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={7} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          7
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={8} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          8
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={9} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          9
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={10} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          10
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={11} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          11
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={12} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          12
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={13} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          13
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={14} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          14
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={15} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          15
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={16} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          16
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={17} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          17
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={18} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          18
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={19} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          19
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={20} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          20
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={21} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          21
        </Button>
      </OverflowMenuItem>
      <OverflowMenuItem label={22} iconName="add" onClick={() => console.log('clicked')}>
        <Button color="ghost" dataLwtId="button" className="mr-2" onClick={() => console.log('clicked')}>
          22
        </Button>
      </OverflowMenuItem>
    </OverflowMenu>
  </>
);

export const Default = Template.bind({});

Default.args = {};
