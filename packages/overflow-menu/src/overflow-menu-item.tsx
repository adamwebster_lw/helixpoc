import React, { ReactNode, MouseEventHandler } from "react";

export interface OverflowMenuItemProps
  extends React.HTMLAttributes<HTMLElement> {
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** The text to display in the menu item */
  label?: string | number;
  children?: ReactNode;
  /** The function to call when the menu item is clicked */
  onClick?: MouseEventHandler<HTMLButtonElement>;
  /** Set to true if it is in the overflow menu */
  isInOverflowMenu: boolean;
}

const OverflowMenuItem = ({
  children,
  dataLwtId,
  label,
  isInOverflowMenu = false,
  onClick,
}: OverflowMenuItemProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <div onClick={() => onClick}>
      {isInOverflowMenu ? (
        <div
          className="overflow-menu-item-button"
          color="primary"
          {...optionalProps}
        >
          {label}
        </div>
      ) : (
        <>{children}</>
      )}
    </div>
  );
};

OverflowMenuItem.displayName = "OverflowMenuItem";

export default OverflowMenuItem;
