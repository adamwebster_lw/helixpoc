import React, { ReactNode, useEffect, useRef, useState } from "react";
import { Button } from "@lwt-helix-nextgen/button";
// import SlideoutPanel from '@lwt-helix/slideout-panel';
import { HelixButtonColors } from "../../types/index";
import { StyledOverflowMenu } from "./overflow-menu.styles";
import { DropdownMenu, DropdownMenuItem } from "../../dropdown-menu";
import useDropdownMenu from "../../hooks/useDropdownMenu";
import { HelixIcon } from "@lwt-helix/helix-icon";
import {
  chevron_small_down,
  more_horizontal,
} from "@lwt-helix/helix-icon/outlined";
export interface OverflowMenuProps extends React.HTMLAttributes<HTMLElement> {
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  children?: ReactNode;
  /** The color of the button */
  overflowButtonColor?: HelixButtonColors;
}

const OverflowMenu = ({
  dataLwtId,
  children,
  overflowButtonColor = 'ghost',
  className,
  ...rest
}: OverflowMenuProps) => {
  const divRef = useRef({} as any);
  const prevWidth = useRef(0);

  const [toggleSlideout, setToggleSlideout] = useState(true);
  const [menuItems, setMenuItems] = useState([{ label: "" }]);
  const [collections, setCollections] = useState({
    items: React.Children.toArray(children),
    overflowItems: [] as any[],
  });

  const shiftIntoOverlayItems = () => {
    setCollections((prev) => {
      prev.overflowItems.unshift(prev.items[prev.items.length - 1]);
      prev.items.pop();
      return { ...prev };
    });
  };

  const shiftIntoItems = () => {
    setCollections((prev) => {
      prev.items.push(prev.overflowItems[0]);
      prev.overflowItems.shift();
      return { ...prev };
    });
  };

  const isOverflowing = () =>
    divRef.current.offsetWidth < divRef.current.scrollWidth;

  const updateItems = () => {
    if (!divRef || !divRef.current) {
      return;
    }

    if (isOverflowing()) {
      do {
        shiftIntoOverlayItems();
      } while (isOverflowing() && collections.items.length > 0);
      prevWidth.current = divRef.current.offsetWidth;
    } else if (
      collections &&
      collections.overflowItems.length > 0 &&
      prevWidth.current < divRef.current.offsetWidth &&
      collections.overflowItems.length > 0
    ) {
      let count = collections.overflowItems.length;
      for (let index = 0; index < count; index++) {
        shiftIntoItems();
        if (isOverflowing()) {
          shiftIntoOverlayItems();
          break;
        }
      }
      prevWidth.current = divRef.current.offsetWidth;
    }
  };

  const {
    isOpen,
    getToggleButtonProps,
    getMenuProps,
    highlightedIndex,
    getItemProps,
    setReferenceElement,
    setPopperElement,
    attributes,
    styles,
    update,
  } = useDropdownMenu({
    placement: "bottom-end",
    items: menuItems,
    offset: [0, 0],
    itemToString: (item: any) => item.label.toString(),
  });

  useEffect(() => {
    // running in setTimeout to make sure states are updated
    setTimeout(() => updateItems(), 100);

    const listener = () => updateItems();
    window.addEventListener("resize", listener);
    return () => {
      window.removeEventListener("resize", listener);
    };
  });

  useEffect(() => {
    update && update();
  }, [isOpen]);

  useEffect(() => {
    const menuItemsToSet: any = [];
    if (collections.overflowItems.length > 0) {
      collections.overflowItems.map((overflowItem) => {
        menuItemsToSet.push({
          label: overflowItem.props.label,
          onClick: overflowItem.props.onClick,
          icon: overflowItem.props.iconName ? (
            <HelixIcon icon={more_horizontal} />
          ) : null,
        });
      });
    }
    setMenuItems(menuItemsToSet);
  }, [collections]);
  const optionalProps = {};

  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledOverflowMenu
      className={`helix-overflow-menu ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      <div className="helix-overflow-menu-items-container" ref={divRef}>
        {collections.items}
      </div>
      {collections.overflowItems.length > 0 && (
        <div style={{ position: "relative" }} ref={setReferenceElement}>
          <Button
            color={overflowButtonColor}
            dataLwtId="overflow-menu-more-button"
            className="overflow-menu-more-button"
            {...getToggleButtonProps()}
          >
            More <HelixIcon icon={chevron_small_down} />
          </Button>
        </div>
      )}
      <DropdownMenu
        maxWidth="300px"
        width="fit-content"
        ref={setPopperElement}
        isOpen={isOpen}
        style={styles.popper}
        {...attributes.popper}
      >
        <ul {...getMenuProps({})}>
          {isOpen &&
            menuItems.length > 0 &&
            menuItems.map((overflowItem: any, index: number) => (
              <DropdownMenuItem
                highlightedIndex={highlightedIndex}
                index={index}
                menuItem={overflowItem}
                key={`${overflowItem.label}${index}`}
                getItemProps={getItemProps}
                itemFormatter={(item) => (
                  <>
                    {item.label} {item.icon && item.icon}
                  </>
                )}
              />
            ))}
        </ul>
      </DropdownMenu>
      {/* {collections.overflowItems.length > 0 && dropDownProps && (
        
      )} */}
    </StyledOverflowMenu>
  );
};

OverflowMenu.displayName = "OverflowMenu";

export default OverflowMenu;
