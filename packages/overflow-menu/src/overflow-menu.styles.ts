import styled, { css } from 'styled-components';

export const StyledOverflowMenu = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  .helix-overflow-menu-items-container {
    overflow-x: hidden;
    display: flex;
    flex: 1 1;
  }
  .overflow-menu-more-button {
    display: flex;
    align-items: center;
  }
`;
