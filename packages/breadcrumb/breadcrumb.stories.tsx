import React from 'react';
import Breadcrumb from './src/breadcrumb';

export default {
  title: 'Components/Breadcrumb',
  component: Breadcrumb,
};

const Template = (args: any) => (
  <Breadcrumb {...args}>
    <a href="http://google.ca">Item 1</a>
    <a href="http://google.ca">Item 2</a>
    <span>Item 3</span>
  </Breadcrumb>
);

export const Default = Template.bind({});
