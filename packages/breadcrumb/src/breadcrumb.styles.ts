import styled, { css } from "styled-components";

export const StyledBreadcrumb = styled.div`
  display: flex;
  color: ${({ theme }) => theme.breadcrumb!.textColor};
  a {
    text-decoration: none;
    color: ${({ theme }) => theme.breadcrumb!.textColor}!important;
    &:hover {
      color: ${({ theme }) => theme.breadcrumb!.hoverTextColor}!important;
    }
  }
  &:last-child {
    color: ${({ theme }) => theme.breadcrumb!.activeTextColor};
  }
  .helix-breadcrumb__separator {
    margin: 0 16px;
  }
`;
