import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledBreadcrumb } from "./breadcrumb.styles";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { chevron_right } from "@lwt-helix/helix-icon/outlined";
export interface BreadcrumbProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
export const Breadcrumb = ({
  children,
  dataLwtId,
  className,
  ...rest
}: BreadcrumbProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledBreadcrumb
      className={`helix-breadcrumb ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {React.Children.map(children, (child, childIndex) => {
        return (
          <>
            {childIndex > 0 && (
              <span className="helix-breadcrumb__separator">
                <HelixIcon icon={chevron_right} />
              </span>
            )}{" "}
            {child}
          </>
        );
      })}
    </StyledBreadcrumb>
  );
};

export default Breadcrumb;
