import { useEffect, useState } from 'react';

export const useScreenWidth = () => {
  const [screenWidth, setScreenWidth] = useState(0);

  const handleResize = (e: any) => {
    setScreenWidth(e.srcElement.innerWidth);
  };
  useEffect(() => {
    window.addEventListener('resize', handleResize);
    setScreenWidth(window.innerWidth);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  return { screenWidth };
};
