import { useEffect, useMemo, useState } from 'react';
import { usePopper } from 'react-popper';
import { useSelect, UseSelectStateChange, UseSelectStateChangeOptions, UseSelectState } from 'downshift';
import { Placement } from '@popperjs/core';

interface Props {
  items: any[];
  itemToString?: (item: any) => string;
  placement?: Placement;
  offset?: number[];
}
export const useDropdownMenu = ({
  items,
  placement = 'bottom-start',
  itemToString = (item: any) => item,
  offset = [0, 10],
}: Props) => {
  const [referenceElement, setReferenceElement] = useState<HTMLUnknownElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);

  const modifiers: any = useMemo(
    () => [
      {
        name: 'sameWidth',
        enabled: true,
        fn: ({ state }: any) => {
          state.styles.popper.width = `${state.rects.reference.width}px`;
        },
        phase: 'beforeWrite',
        requires: ['computeStyles'],
      },
      {
        name: 'offset',
        options: {
          offset,
        },
      },
    ],
    [],
  );

  const { styles, attributes, update } = usePopper(referenceElement, popperElement, {
    placement,
    modifiers,
  });

  const stateReducer = (state: UseSelectState<any>, actionAndChanges: UseSelectStateChangeOptions<any>) => {
    const { type, changes } = actionAndChanges;
    switch (type) {
      case useSelect.stateChangeTypes.MenuKeyDownArrowDown:
        if (state.highlightedIndex === items.length - 1) {
          return {
            ...changes,
            isOpen: true,
            highlightedIndex: 0,
          };
        }
        return {
          ...changes,
          isOpen: true,
        };
      case useSelect.stateChangeTypes.MenuKeyDownArrowUp:
        if (state.highlightedIndex === 0) {
          return {
            ...changes,
            isOpen: true,
            highlightedIndex: items.length - 1,
          };
        }
        return {
          ...changes,
          isOpen: true,
        };
      default:
        return changes; // otherwise business as usual.
    }
  };

  const handleSelectedItem = (props: UseSelectStateChange<any>) => {
    const { selectedItem } = props;
    if (selectedItem.onClick) selectedItem.onClick();
    if (selectedItem.url) window.location.href = selectedItem.url;
  };

  const { isOpen, getToggleButtonProps, getMenuProps, highlightedIndex, selectedItem, getItemProps } = useSelect({
    items,
    itemToString,
    stateReducer,
    onSelectedItemChange: handleSelectedItem,
  });

  useEffect(() => {
    update && update();
  }, [isOpen]);
  return {
    isOpen,
    getToggleButtonProps,
    getMenuProps,
    highlightedIndex,
    selectedItem,
    getItemProps,
    setReferenceElement,
    setPopperElement,
    attributes,
    styles,
    update,
  };
};

export default useDropdownMenu;
