import React, {
  createContext,
  ReactNode,
  useState,
  Dispatch,
  SetStateAction,
} from "react";

interface InitialContextProps {
  onClose: any;
  setOnClose: Dispatch<SetStateAction<any>>;
  autoFocusCloseButton: boolean;
  setAutoFocusCloseButton: Dispatch<SetStateAction<boolean>>;
}

const initialContext = {
  onClose: () => {},
  setOnClose: () => null,
  autoFocusCloseButton: true,
  setAutoFocusCloseButton: () => null,
};

export const ModalContext = createContext<InitialContextProps>(initialContext);
export const ModalContextConsumer = ModalContext.Consumer;

interface ModalContextProviderProps {
  children: ReactNode;
}

export const ModalContextProvider = ({
  children,
}: ModalContextProviderProps) => {
  const [onClose, setOnClose] = useState(null);
  const [autoFocusCloseButton, setAutoFocusCloseButton] = useState(true);
  return (
    <ModalContext.Provider
      value={{
        onClose,
        setOnClose,
        autoFocusCloseButton,
        setAutoFocusCloseButton,
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};
