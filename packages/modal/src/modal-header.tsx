import React, { ReactNode, useContext, useEffect, useRef } from "react";
import { StyledModalHeader } from "./modal.styles";
import { Button } from "@lwt-helix-nextgen/button";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { x } from "@lwt-helix/helix-icon/outlined";
import { ModalContext } from "./modal-context";

interface ModalHeaderProps {
  children: ReactNode;
}

const ModalHeader = ({ children, ...rest }: ModalHeaderProps) => {
  const closeButtonRef = useRef<HTMLButtonElement>(null);
  const { onClose, autoFocusCloseButton } = useContext(ModalContext);

  useEffect(() => {
    if (autoFocusCloseButton) {
      closeButtonRef.current?.focus();
    }
  }, [autoFocusCloseButton]);

  return (
    <StyledModalHeader {...rest}>
      <div className="lwt-helix-modal__header-content">{children}</div>
      <Button
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        dataLwtId="close-modal"
        aria-label="Close"
        color="ghost"
        onClick={() => onClose && onClose()}
        ref={closeButtonRef}
      >
        <HelixIcon icon={x}></HelixIcon>
      </Button>
    </StyledModalHeader>
  );
};

ModalHeader.displayName = "ModalHeader";

export default ModalHeader;
