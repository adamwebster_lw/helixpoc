import styled, { css } from "styled-components";

interface StyledOverlayProps {
  closing: boolean;
  closed: boolean;
}
export const StyledOverlay = styled.div<StyledOverlayProps>`
  position: fixed;
  z-index: 1051;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.2);
  display: flex;
  align-items: center;
  justify-content: center;
  animation-name: fadeInOverlay;
  animation-duration: 0.6s;
  opacity: ${({ closed }) => (closed ? 0 : 1)};
  ${({ closing }) =>
    closing &&
    css`
      animation-name: fadeOutOverlay;
      animation-duration: 0.6s;
    `}

  @keyframes fadeInOverlay {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  @keyframes fadeOutOverlay {
    from {
      opacity: 1;
    }
    to {
      opacity: 0;
    }
  }
`;

interface StyledModalProps {
  closing: boolean;
  closed: boolean;
}
export const StyledModal = styled.div<StyledModalProps>`
  background: ${({ theme }) => theme.modal?.backgroundColor};
  border-radius: 8px;
  box-shadow: ${({ theme }) => theme.modal?.boxShadow};
  display: flex;
  flex-flow: column;
  animation-name: fadeInModal;
  animation-duration: 0.6s;
  max-width: 500px;
  opacity: ${({ closed }) => (closed ? 0 : 1)};
  ${({ closing }) =>
    closing &&
    css`
      animation-name: fadeOutModal;
      animation-duration: 0.6s;
    `}
  @keyframes fadeInModal {
    from {
      opacity: 0;
      margin-top: -100px;
    }
    to {
      opacity: 1;
      margin-top: 0px;
    }
  }

  @keyframes fadeOutModal {
    from {
      opacity: 1;
      margin-top: 0px;
    }
    to {
      opacity: 0;
      margin-top: -100px;
    }
  }
`;

export const StyledModalHeader = styled.div`
  display: flex;
  flex: 1;
  padding: 32px 34px 16px 34px;
  .lwt-helix-modal__header-content {
    flex: 1 1;
  }
`;

export const StyledModalBody = styled.div`
  flex: 1;
  padding: 16px 34px 24px 34px;
`;

export const StyledModalFooter = styled.div`
  flex: 1;
  padding: 24px 34px;
`;
