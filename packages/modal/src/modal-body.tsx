import React, { ReactNode } from 'react';
import { StyledModalBody } from './modal.styles';

interface ModalBodyProps {
  children: ReactNode;
}

const ModalBody = ({ children, ...rest }: ModalBodyProps) => {
  return <StyledModalBody {...rest}>{children}</StyledModalBody>;
};

ModalBody.displayName = 'ModalBody';

export default ModalBody;
