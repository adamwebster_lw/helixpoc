import React, { ReactNode } from 'react';
import { StyledModalFooter } from './modal.styles';

interface ModalFooterProps {
  children: ReactNode;
}
const ModalFooter = ({ children, ...rest }: ModalFooterProps) => {
  return <StyledModalFooter {...rest}>{children}</StyledModalFooter>;
};

ModalFooter.displayName = 'ModalFooter';

export default ModalFooter;
