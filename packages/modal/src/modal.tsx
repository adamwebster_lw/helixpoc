import React, {
  HtmlHTMLAttributes,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { StyledModal, StyledOverlay } from "./modal.styles";
import { ModalContext, ModalContextProvider } from "./modal-context";
interface ModalProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  show?: boolean;
  /** Call back for when the modal close.  It should at least set open to false */
  onClose: () => void;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** Set this to false to disable auto focusing of close button */
  autoFocusCloseButton?: boolean;
}
const ModalComponent = ({
  children,
  show,
  onClose,
  dataLwtId,
  ...rest
}: ModalProps) => {
  return (
    <>
      <ModalContextProvider>
        <ModalOverlay
          onClose={onClose}
          show={show}
          dataLwtId={dataLwtId}
          {...rest}
        >
          {children}
        </ModalOverlay>
      </ModalContextProvider>
    </>
  );
};

const ModalOverlay = ({
  onClose,
  children,
  show,
  dataLwtId,
  className,
  autoFocusCloseButton = true,
  ...rest
}: ModalProps) => {
  // Handle interacting outside the dialog and pressing
  // the Escape key to close the modal.
  let ref = React.useRef<HTMLDivElement>(null);
  const lastFocus = React.useRef<HTMLElement | null>(null);
  const { setOnClose, setAutoFocusCloseButton } = useContext(ModalContext);

  const [modalDisplayState, setModalDisplayState] = useState("closed");
  const [mounted, setMounted] = useState(false);

  const handleKeydown = (e) => {
    if (e.key === "Escape") {
      onClose && onClose();
    }
    // focus lock for modal
    if (e.key === "Tab") {
      if (ref.current) {
        const focusableElements = ref.current.querySelectorAll(
          "button, [href], input, select, textarea, [tabindex]:not([tabindex='-1'])"
        );
        const firstFocusableElement = focusableElements[0] as HTMLElement;
        const lastFocusableElement =
          focusableElements[focusableElements.length - 1];
        if (document.activeElement === lastFocusableElement) {
          firstFocusableElement.focus();
          e.preventDefault();
        }
      }
    }
  };

  useEffect(() => {
    if (onClose) {
      setOnClose(() => onClose);
    }
  }, [onClose]);

  useEffect(() => {
    // get element that had focus before modal was opened
    const focusedElementBeforeModal = document.activeElement as HTMLElement;
    if (show) {
      lastFocus.current = focusedElementBeforeModal;
      setModalDisplayState("show");
    } else {
      if (mounted) {
        setModalDisplayState("closing");
        if (lastFocus.current) {
          lastFocus.current.focus();
        }
      }
    }
  }, [show]);

  useEffect(() => {
    if (modalDisplayState === "closing") {
      setTimeout(() => {
        setModalDisplayState("closed");
      }, 450);
    }
  }, [modalDisplayState]);

  useEffect(() => {
    setAutoFocusCloseButton(autoFocusCloseButton);
  }, [autoFocusCloseButton]);
  
  useEffect(() => {
    setMounted(true);

    document.body.addEventListener("keydown", handleKeydown);
    return () => {
      document.body.removeEventListener("keydown", handleKeydown);
    };
  }, []);

  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <>
      {modalDisplayState !== "closed" && (
        <StyledOverlay
          closed={modalDisplayState === "closed"}
          closing={modalDisplayState === "closing"}
          className="helix-modal-overlay"
          style={{}}
        >
          <StyledModal
            closed={modalDisplayState === "closed"}
            closing={modalDisplayState === "closing"}
            ref={ref}
            className={`helix-modal ${className ? className : ""}`}
            {...optionalProps}
            {...rest}
          >
            {children}
          </StyledModal>
        </StyledOverlay>
      )}
    </>
  );
};

const Modal = ({ ...rest }: ModalProps) => {
  return (
    <>
      <ModalComponent {...rest} />
    </>
  );
};
export default Modal;
