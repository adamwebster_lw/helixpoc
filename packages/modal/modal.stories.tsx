import React, { useState } from "react";

import { Meta } from "@storybook/react";

import { Modal, ModalHeader, ModalBody, ModalFooter } from "./index";
import { Button } from "@lwt-helix-nextgen/button";
import { DisplayMedium } from "@lwt-helix-nextgen/typography";
export default {
  component: Modal,
  title: "Components/Modal",
} as Meta;

export const Primary: React.VFC<{}> = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <Button dataLwtId="Open Modal" onClick={() => setIsOpen(true)}>
        Open Modal
      </Button>
      <Modal onClose={() => setIsOpen(false)} show={isOpen} title="Modal Title">
        <ModalHeader>
          <DisplayMedium>Modal Header</DisplayMedium>
        </ModalHeader>
        <ModalBody>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id, saepe
          quis deleniti incidunt tenetur nesciunt inventore ex facere ipsam
          eaque. Quia harum deserunt veritatis quod facilis? At fuga aliquid
          laborum.
        </ModalBody>
        <ModalFooter>
          <Button dataLwtId="" onClick={() => setIsOpen(false)}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};
