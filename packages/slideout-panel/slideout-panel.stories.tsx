import { DisplaySmall } from "@lwt-helix-nextgen/typography";
import React, { useState } from "react";
import SlideoutPanel from "./src/SlideoutPanel";
import SlideoutPanelBody from "./src/SlideoutPanelBody";
import SlideoutPanelHeader from "./src/SlideoutPanelHeader";
import SlideoutPanelFooter from "./src/SlideoutPanelFooter";
import { Button } from "@lwt-helix-nextgen/button";
import SlideoutSecondaryPanel from "./src/SlideoutSecondaryPanel";
export default {
  title: "Components/Slideout Panel",
  component: SlideoutPanel,
};

const BWContent = () => (
  <>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore
      molestiae, at enim corrupti saepe voluptatem hic quam nisi, autem non
      quidem molestias dolor dolore amet eos veniam eveniet facilis libero rerum
      atque commodi modi deleniti ab aspernatur. Illum quos temporibus voluptate
      vitae quasi laboriosam maiores libero optio officia quod quisquam
      asperiores odio unde dolore aliquid fugiat, blanditiis adipisci, commodi
      iusto facere incidunt error architecto, dolores aut. Deserunt, aut
      deleniti nulla voluptatibus neque inventore aliquid aliquam provident
      repellendus? Accusamus eveniet consequatur ex esse doloribus error dolorem
      perspiciatis incidunt nam nesciunt. Dolor dolore nihil architecto nostrum
      magni neque quis aperiam quasi repellendus!
    </p>
    <p>
      Doloremque commodi nulla quas mollitia. Facilis, tenetur porro, nihil
      asperiores eaque totam fugit a natus sunt eum quo quisquam voluptatum
      explicabo. Odio cupiditate dolorem sunt placeat velit error impedit
      ducimus quisquam sapiente! Accusamus eos, vero aliquid corrupti est
      pariatur esse dolorum autem doloribus quae odit laboriosam neque qui
      expedita iure adipisci illum eaque, quo dolor nam temporibus ipsam tenetur
      iste atque. Excepturi quas corrupti rerum. Recusandae, rerum nisi ullam
      odit quaerat illum molestias nesciunt similique doloribus magni enim
      inventore accusantium id ab sapiente quia hic possimus modi. Fugiat
      excepturi similique ea! Perspiciatis officiis exercitationem repudiandae
      aperiam voluptatibus dolorem quas distinctio.
    </p>
    <p>
      Sapiente, pariatur quis sed dolor commodi maxime praesentium, sit
      blanditiis, tenetur asperiores molestiae doloremque beatae! Accusantium
      non nam voluptatum voluptates. Unde maxime blanditiis consectetur tempore
      ea cum eos accusantium velit perspiciatis assumenda eaque, fugit quam
      provident officia, sed soluta similique! Tenetur aperiam iste, officiis
      sunt sint architecto fuga aut? Placeat eveniet, molestiae aspernatur quia
      aperiam alias, commodi amet laborum asperiores accusantium doloremque a
      enim culpa illo sapiente facere omnis autem dicta cumque. Consequatur
      doloribus facere cupiditate laboriosam, quae error temporibus obcaecati
      delectus vero? Ipsum nemo tenetur sint enim fuga. Quod dolores ipsam omnis
      quasi placeat veniam nemo. Quidem, repellat fuga.
    </p>
    <p>
      Dolore, delectus officiis! Perferendis sequi quasi iure facere, suscipit
      adipisci vel necessitatibus esse ab delectus iusto repellendus quo
      blanditiis modi fugit maiores quis! Praesentium rerum repellat ipsa
      tempora officiis, eum laboriosam corrupti, sit incidunt id enim debitis
      magnam! Praesentium nam fuga fugiat reiciendis facilis quo illum quisquam
      earum? Harum minima voluptatem in inventore quae nostrum ipsum,
      voluptatibus, dolore sequi doloremque fugiat aliquid quidem nam.
      Asperiores et esse quae vero voluptate hic officiis, natus vel neque
      necessitatibus amet voluptatibus accusantium laudantium, consectetur,
      eligendi explicabo earum maiores. Ratione culpa, provident praesentium
      fuga perferendis dolor consequatur eum odit tempore, sed, soluta aliquid
      voluptatem?
    </p>
    <p>
      Consequatur facere quisquam aliquid itaque. Cumque, nobis deleniti vel
      eius veritatis nostrum voluptatum enim quibusdam beatae possimus
      praesentium necessitatibus, cupiditate provident consectetur. Veritatis,
      atque sapiente explicabo nemo enim maiores molestias placeat quas nobis,
      non ab quam voluptatum odio, incidunt labore sunt. Porro nostrum veritatis
      perferendis excepturi error, officia magni architecto, necessitatibus odit
      earum, consequatur quia deleniti. Quae earum magni animi placeat culpa
      velit numquam minima sapiente est tenetur consequuntur alias impedit
      expedita veritatis saepe, incidunt sunt nam voluptate quo blanditiis
      laborum? Impedit, aliquid! Voluptatem aliquid quidem perspiciatis, cum
      voluptates magni, vero dolorum voluptate pariatur voluptatibus adipisci
      facere, in consequatur voluptas.
    </p>
  </>
);

const CKContent = () => (
  <>
    <p>
      Dolore, delectus officiis! Perferendis sequi quasi iure facere, suscipit
      adipisci vel necessitatibus esse ab delectus iusto repellendus quo
      blanditiis modi fugit maiores quis! Praesentium rerum repellat ipsa
      tempora officiis, eum laboriosam corrupti, sit incidunt id enim debitis
      magnam! Praesentium nam fuga fugiat reiciendis facilis quo illum quisquam
      earum? Harum minima voluptatem in inventore quae nostrum ipsum,
      voluptatibus, dolore sequi doloremque fugiat aliquid quidem nam.
      Asperiores et esse quae vero voluptate hic officiis, natus vel neque
      necessitatibus amet voluptatibus accusantium laudantium, consectetur,
      eligendi explicabo earum maiores. Ratione culpa, provident praesentium
      fuga perferendis dolor consequatur eum odit tempore, sed, soluta aliquid
      voluptatem?
    </p>
    <p>
      Consequatur facere quisquam aliquid itaque. Cumque, nobis deleniti vel
      eius veritatis nostrum voluptatum enim quibusdam beatae possimus
      praesentium necessitatibus, cupiditate provident consectetur. Veritatis,
      atque sapiente explicabo nemo enim maiores molestias placeat quas nobis,
      non ab quam voluptatum odio, incidunt labore sunt. Porro nostrum veritatis
      perferendis excepturi error, officia magni architecto, necessitatibus odit
      earum, consequatur quia deleniti. Quae earum magni animi placeat culpa
      velit numquam minima sapiente est tenetur consequuntur alias impedit
      expedita veritatis saepe, incidunt sunt nam voluptate quo blanditiis
      laborum? Impedit, aliquid! Voluptatem aliquid quidem perspiciatis, cum
      voluptates magni, vero dolorum voluptate pariatur voluptatibus adipisci
      facere, in consequatur voluptas.
    </p>
  </>
);
const Template = (args: any) => {
  const [contentId, setContentId] = useState<undefined | string>(undefined);
  const [panelContent, setPanelContent] = useState({
    header: "Bruce Wayne",
    body: BWContent(),
  });

  return (
    <>
      <Button
        dataLwtId="toggle-panel"
        onClick={() => {
          if (contentId === undefined) {
            setContentId("a");
          } else {
            setContentId(undefined);
          }
        }}
      >
        Toggle Slideout Panel
      </Button>
      {contentId && (
        <Button
          dataLwtId="Change Content"
          onClick={(e) =>
            setPanelContent((prevContent) => {
              return {
                ...prevContent,
                body:
                  prevContent.header === "Bruce Wayne"
                    ? CKContent()
                    : BWContent(),
                header:
                  prevContent.header === "Bruce Wayne"
                    ? "Clark Kent"
                    : "Bruce Wayne",
              };
            })
          }
        >
          Change Slideout Header
        </Button>
      )}
      <SlideoutPanel
        {...args}
        onClose={() => {
          setContentId(undefined);
        }}
        contentId={contentId}
      >
        <SlideoutPanelHeader>
          <DisplaySmall>{panelContent.header}</DisplaySmall>
        </SlideoutPanelHeader>
        <SlideoutPanelBody>{panelContent.body}</SlideoutPanelBody>
        <SlideoutPanelFooter>
          <Button dataLwtId="close" onClick={() => setContentId(undefined)}>
            Close
          </Button>
        </SlideoutPanelFooter>
      </SlideoutPanel>
    </>
  );
};

export const Default = Template.bind({});

export const OpenFromBottomMobile: any = Template.bind({});

OpenFromBottomMobile.args = {
  openFromBottomOnMobile: true,
};

export const SecondaryPanel: any = () => {
  const [contentId, setContentId] = useState<undefined | string>(undefined);
  const [secondaryActive, setSecondaryActive] = useState(false);
  return (
    <>
      <Button dataLwtId="toggle-panel" onClick={() => setContentId("a")}>
        Toggle Slideout Panel
      </Button>
      <SlideoutPanel
        isLarge={false}
        contentId={contentId}
        onClose={() => setContentId(undefined)}
      >
        <SlideoutPanelHeader>
          <DisplaySmall>Secondary Panel</DisplaySmall>
        </SlideoutPanelHeader>
        <SlideoutPanelBody>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sit nisi
          molestiae quaerat odio id iusto maxime expedita exercitationem
          corporis facilis cupiditate unde quisquam quia, pariatur et ducimus
          dignissimos saepe temporibus.
          <Button
            dataLwtId=""
            onClick={() => setSecondaryActive(!secondaryActive)}
          >
            Toggle Scondary Panel
          </Button>
        </SlideoutPanelBody>
        <SlideoutPanelFooter>
          <Button dataLwtId="close" onClick={() => setContentId(undefined)}>
            Close
          </Button>
        </SlideoutPanelFooter>
        <SlideoutSecondaryPanel isActive={secondaryActive}>
          <SlideoutPanelBody>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
            quae, voluptas, quod, voluptate quibusdam voluptates quidem
            voluptatibus quos quia quas nesciunt. Quisquam, quae. Quisquam
            voluptas, quae, quibusdam, quod quos quia quas nesciunt.
          </SlideoutPanelBody>
          <SlideoutPanelFooter>
            <Button dataLwtId="close" onClick={() => setSecondaryActive(false)}>
              Close
            </Button>
          </SlideoutPanelFooter>
        </SlideoutSecondaryPanel>
      </SlideoutPanel>
    </>
  );
};

SecondaryPanel.args = {
  isLarge: false,
};
