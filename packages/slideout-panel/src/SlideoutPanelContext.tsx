import React, { createContext, ReactNode } from "react";

interface SlideoutPanelContextProps {
  secondaryPanelOpen: boolean;
}

const initialState: SlideoutPanelContextProps = {
  secondaryPanelOpen: false,
};

interface ReducerActionProps {
    type: "SET_SLIDEOUT_PANEL_OPEN";
    payload: any;
  }

export const SlideoutPanelContext = createContext({
  state: initialState,
  dispatch: (value: ReducerActionProps | void) => value,
});

export const SlideoutPanelConsumer = SlideoutPanelContext.Consumer;

export const SlideoutPanelReducer = (
  state: SlideoutPanelContextProps,
  action: ReducerActionProps
) => {
  switch (action.type) {
    case "SET_SLIDEOUT_PANEL_OPEN":
      return {
        ...state,
        secondaryPanelOpen: action.payload,
      };
    default:
      return state;
  }
};

export const SlideoutPanelProvider = ({
  children,
}: {
  children: ReactNode;
}) => {
  const [state, dispatch] = React.useReducer(
    SlideoutPanelReducer,
    initialState
  );

  return (
    <SlideoutPanelContext.Provider value={{ state, dispatch }}>
      {children}
    </SlideoutPanelContext.Provider>
  );
};
