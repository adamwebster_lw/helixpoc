import React, {
  ReactNode,
  useContext,
  useEffect,
  HtmlHTMLAttributes,
} from "react";
import { StyledSecondaryPanel } from "./SlideoutPanel.styles";
import { SlideoutPanelContext } from "./SlideoutPanelContext";

interface SlideoutSecondaryPanelProps
  extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  isActive?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const SlideoutSecondaryPanel = ({
  isActive = false,
  children,
  dataLwtId,
  className,
  ...rest
}: SlideoutSecondaryPanelProps) => {
  const { state, dispatch } = useContext(SlideoutPanelContext);

  useEffect(() => {
    dispatch({
      type: "SET_SLIDEOUT_PANEL_OPEN",
      payload: isActive,
    });
  }, [isActive]);
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return isActive ? (
    <StyledSecondaryPanel
      className={`helix-slideout-panel-secondary ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledSecondaryPanel>
  ) : (
    <></>
  );
};

SlideoutSecondaryPanel.displayName = "SlideoutSecondaryPanel";

export default SlideoutSecondaryPanel;
