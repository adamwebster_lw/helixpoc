import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledHeaderContent } from ".//SlideoutPanel.styles";
interface SlideoutPanelHeaderProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const SlideoutPanelHeader = ({
  children,
  dataLwtId,
  className,
  ...rest
}: SlideoutPanelHeaderProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledHeaderContent
      className={`helix-slideout-panel-header ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledHeaderContent>
  );
};

SlideoutPanelHeader.displayName = "SlideoutPanelHeader";

export default SlideoutPanelHeader;
