import React, {
  HtmlHTMLAttributes,
  ReactNode,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import {
  StyledPanelHeader,
  StyledPanelsWrapper,
  StyledSlideoutPanel,
  StyledSlideoutPanelInner,
} from "./SlideoutPanel.styles";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { x } from "@lwt-helix/helix-icon/outlined";
import { ExpandArrow } from "./ExpandArrow";
import {
  SlideoutPanelContext,
  SlideoutPanelProvider,
} from "./SlideoutPanelContext";

export interface SlideoutPanelProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children?: ReactNode;
  /** 
Whether or not space should be left at the top of the page for a navbar */
  pageHasNavbar?: boolean;
  /** whether the panel should be large */
  isLarge?: boolean;
  /** unique id to identify the content (decides whether to refresh the content or close the panel; pass undefined to force-close the panel) */
  contentId?: string;
  /** callback for when the panel is closed. */
  onClose?: () => void;
  /** determines if slideoutpanel will cover space at the top as the user scrolls down */
  repositionIfTopGap?: boolean;
  /** If set the panel will open from the bottom on screens smaller the 576px */
  openFromBottomOnMobile?: boolean;
  /** If the panel is set to open from the bottom on mobile you can set this property to false to disable the expand/collapse feature. */
  expandableOnMobile?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const SlideoutPanel = ({ ...rest }: SlideoutPanelProps) => {
  return (
    <SlideoutPanelProvider>
      <SlideoutPanelInner {...rest} />
    </SlideoutPanelProvider>
  );
};

const SlideoutPanelInner = ({
  children,
  pageHasNavbar = true,
  isLarge = false,
  contentId,
  className,
  repositionIfTopGap = true,
  openFromBottomOnMobile = false,
  expandableOnMobile = true,
  dataLwtId,
  onClose,
  ...rest
}: SlideoutPanelProps) => {
  const [panelState, setPanelState] = useState({
    contentId,
    isActive: contentId !== undefined,
  });
  const { state, dispatch } = useContext(SlideoutPanelContext);
  const [panelHidden, setPanelHidden] = useState(true);
  const [slideoutInitialTop, setSlideoutInitialTop] =
    useState<undefined | number>(undefined);
  const [topState, setTopState] = useState<undefined | number>(undefined);
  const [height, setHeight] = useState<number | undefined>(undefined);
  const [expandedMobile, setExpandedOnMobile] = useState(false);
  const slideoutRef = useRef<HTMLDivElement | null>(null);
  const showSlideoutPanel = () => {
    setPanelState((prevState) => {
      return {
        ...prevState,
        contentId,
        isActive: true,
      };
    });
  };

  const hideSlideoutPanel = () => {
    if (onClose) {
      setPanelState((prevState) => {
        return {
          ...prevState,
          contentId: undefined,
          isActive: false,
        };
      });
    }
  };
  const handleDismissClick = () => {
    if (onClose) onClose();
  };

  const handleScroll = () => {
    if (slideoutRef && slideoutRef.current) {
      let initialTop = slideoutInitialTop;
      if (!initialTop) {
        initialTop = parseInt(
          window.getComputedStyle(slideoutRef.current).getPropertyValue("top"),
          10
        );
        setSlideoutInitialTop(initialTop);
      }
      if (initialTop || initialTop === 0) {
        const top = Math.min(
          Math.max(initialTop - window.pageYOffset, 0),
          initialTop
        );
        const currentTop = parseInt(
          window.getComputedStyle(slideoutRef.current).getPropertyValue("top"),
          10
        );
        if (top !== topState || currentTop !== topState) {
          setTopState(top);
          setHeight(window.innerHeight - top);
        }
      }
    }
  };

  useEffect(() => {
    if (repositionIfTopGap) {
      window.addEventListener("scroll", handleScroll, false);
      return () => {
        window.removeEventListener("scroll", handleScroll, false);
      };
    }
  }, [repositionIfTopGap, slideoutInitialTop]);

  useEffect(() => {
    if (repositionIfTopGap) {
      handleScroll();
    }
  }, [panelState.isActive]);
  useEffect(() => {
    if (contentId === undefined) {
      setTimeout(() => hideSlideoutPanel());
      setTimeout(() => {
        setPanelHidden(true);
      }, 500);
    } else {
      setPanelHidden(false);
      setTimeout(() => showSlideoutPanel(), 100);
    }
  }, [contentId]);

  const optionalProps = {
    ...(dataLwtId && { "data-lwt-id": dataLwtId }),
  };

  return (
    <StyledSlideoutPanel
      className={`helix-slideout-panel ${className ? className : ""}`}
      isActive={panelState.isActive}
      ref={slideoutRef}
      panelHidden={panelHidden}
      isLarge={isLarge}
      top={topState as number}
      panelHeight={height}
      pageHasNavbar={pageHasNavbar}
      openFromBottomOnMobile={openFromBottomOnMobile}
      expandedMobile={expandedMobile}
      secondaryPanelOpen={state.secondaryPanelOpen}
      {...optionalProps}
      {...rest}
    >
      {openFromBottomOnMobile && expandableOnMobile && (
        <div className="panel-expand-icon">
          <button
            aria-label={expandedMobile ? "collapse panel" : "expand panel"}
            onClick={() => {
              setExpandedOnMobile(!expandedMobile);
            }}
          >
            <ExpandArrow />
          </button>
        </div>
      )}
      {React.Children.map(children, (child: any) => {
        if (child.type.displayName === "SlideoutPanelHeader") {
          return (
            <StyledPanelHeader>
              {child}
              <button
                onClick={() => handleDismissClick()}
                className="helix-slideout-panel__close-button"
              >
                <HelixIcon icon={x} />
              </button>
            </StyledPanelHeader>
          );
        }
      })}
      <StyledPanelsWrapper
        isLarge={isLarge}
        secondaryPanelOpen={state.secondaryPanelOpen}
      >
        <StyledSlideoutPanelInner secondaryPanelOpen={state.secondaryPanelOpen}>
          {React.Children.map(children, (child: any) => {
            if (
              child.type.displayName !== "SlideoutSecondaryPanel" &&
              child.type.displayName !== "SlideoutPanelHeader"
            ) {
              return child;
            }
          })}
        </StyledSlideoutPanelInner>
        {React.Children.map(children, (child: any) => {
          if (child.type.displayName === "SlideoutSecondaryPanel") {
            return child;
          }
        })}
      </StyledPanelsWrapper>
    </StyledSlideoutPanel>
  );
};

SlideoutPanel.displayName = "SlideoutPanel";

export default SlideoutPanel;
