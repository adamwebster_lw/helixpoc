import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledPanelFooter } from "./SlideoutPanel.styles";

interface SlideoutPanelFooterProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const SlideoutPanelFooter = ({
  children,
  dataLwtId,
  className,
  ...rest
}: SlideoutPanelFooterProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledPanelFooter
      className={`helix-slideout-panel-footer ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledPanelFooter>
  );
};

SlideoutPanelFooter.displayName = "SlideoutPanelFooter";

export default SlideoutPanelFooter;
