import styled, { css } from "styled-components";

interface StyledSlideoutPanelProps {
  pageHasNavbar: boolean;
  isLarge: boolean;
  isActive: boolean;
  panelHidden: boolean;
  top: number;
  panelHeight?: number;
  openFromBottomOnMobile?: boolean;
  expandedMobile?: boolean;
  secondaryPanelOpen?: boolean;
}
export const StyledSlideoutPanel = styled.div<StyledSlideoutPanelProps>`
  position: fixed;
  display: ${({ panelHidden }) => (panelHidden ? "none" : "flex")};
  width: ${({ isLarge, secondaryPanelOpen }) =>
    secondaryPanelOpen ? "700px" : isLarge ? "850px" : "300px"};
  background-color: ${({ theme }) => theme.slideoutPanel?.backgroundColor};
  box-shadow: ${({ theme }) => theme.slideoutPanel?.boxShadow};
  flex-flow: column;
  height: ${({ panelHeight, pageHasNavbar }) =>
    pageHasNavbar
      ? panelHeight
        ? panelHeight + "px"
        : "calc(100vh - 58px)"
      : "100vh"};
  right: ${({ isActive, isLarge }) =>
    isActive ? "0" : isLarge ? "-850px" : "-350px"};
  top: ${({ pageHasNavbar, top }) => (pageHasNavbar ? `${top}px` : "0")};
  bottom: 0;
  bottom: ${({ pageHasNavbar, top }) => (pageHasNavbar ? `-${top}px` : "0")};
  z-index: 1029;
  transition: display 0.5s, left 0.5s, right 0.5s, width 0.5s, height 0.5s;
  .panel-expand-icon {
    display: none;
  }
  @media screen and (max-width: ${({ theme }) => theme.breakPoints?.small}) {
    ${({ openFromBottomOnMobile, isActive, expandedMobile }) =>
      openFromBottomOnMobile &&
      css`
        transition: display 0.5s, left 0.5s, bottom 0.5s, right 0.5s, width 0.5s,
          max-height 0.5s;
        width: 100vw;
        max-width: 100%;
        right: 0 !important;
        bottom: ${isActive ? "0" : "-100vh"};
        top: auto !important;
        max-height: ${expandedMobile ? "calc(100% - 27px - 2rem)" : " 35vh"};
        .panel-expand-icon {
          display: flex;
          justify-content: center;
          padding-top: 8px;
          button {
            padding: 0;
            background-color: transparent;
            border: none;
            svg {
              ${expandedMobile &&
              css`
                transform: rotate(180deg);
              `}
            }
          }
        }
      `}
  }
`;

interface StyledPanelsWrapperProps {
  secondaryPanelOpen?: boolean;
  isLarge?: boolean;
}

export const StyledPanelsWrapper = styled.div<StyledPanelsWrapperProps>`
  display: grid;
  width: 100%;
  height: 100%;
  grid-template-columns: ${({ isLarge }) => (isLarge ? "1fr" : "300px")};
  overflow: hidden;
  ${({ secondaryPanelOpen }) =>
    secondaryPanelOpen &&
    css`
      grid-template-columns: 300px 400px;
    `}
`;

interface StyledSlideoutPanelHeaderProps {
  secondaryPanelOpen?: boolean;
}

export const StyledSlideoutPanelInner = styled.div<StyledSlideoutPanelHeaderProps>`
  flex-flow: column;
  overflow: hidden;
  flex: 1 1;
  display: flex;
  // add box shadow if secondary panel is open
  ${({ secondaryPanelOpen }) =>
    secondaryPanelOpen &&
    css`
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
    `}
`;

export const StyledPanelHeader = styled.div`
  padding: 16px;
  box-sizing: border-box;
  display: flex;
  .helix-slideout-panel__close-button {
    background-color: transparent;
    border: none;
    cursor: pointer;
  }
`;

export const StyledHeaderContent = styled.div`
  flex: 1 1;
`;
export const StyledPanelBody = styled.div`
  padding: 16px;
  flex: 1;
  box-sizing: border-box;
  overflow: auto;
`;

export const StyledPanelFooter = styled.div`
  padding: 16px;
  box-sizing: border-box;
  min-height: 60px;
  border-top: solid 1px ${({ theme }) => theme.slideoutPanel?.footerBorderColor};
`;

export const StyledSecondaryPanel = styled.div`
  flex: 0 400px;
  display: flex;
  flex-direction: column;
`;
