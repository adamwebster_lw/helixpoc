import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledPanelBody } from "./SlideoutPanel.styles";

interface SlideoutPanelBodyProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const SlideoutPanelBody = ({
  children,
  className,
  dataLwtId,
  ...rest
}: SlideoutPanelBodyProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledPanelBody
      className={`helix-slideout-panel-body ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledPanelBody>
  );
};

SlideoutPanelBody.displayName = "SlideoutPanelBody";

export default SlideoutPanelBody;
