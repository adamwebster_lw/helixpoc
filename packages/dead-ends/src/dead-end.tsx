import React, { ReactNode, HTMLAttributes } from "react";
import { StyledDeadEnd } from "./dead-end.styles";

export interface DeadEndProps extends HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const DeadEnd = ({ children, className, dataLwtId, ...rest }: DeadEndProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledDeadEnd
      className={`lwt-helix-dead-end ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledDeadEnd>
  );
};

DeadEnd.displayName = "DeadEnd";

export default DeadEnd;
