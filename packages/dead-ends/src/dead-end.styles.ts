import styled from 'styled-components';

export const StyledDeadEnd = styled.div`
  background-color: ${({ theme }) => theme.deadEnds?.backgroundColor};
  padding: 24px;
  border-radius: 4px;
`;
