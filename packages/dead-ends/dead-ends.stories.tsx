import { Body, DisplaySmall } from '@lwt-helix-nextgen/typography';
import React from 'react';
import DeadEnd from './src/dead-end';
import { Button } from '@lwt-helix-nextgen/button';

export default {
  title: 'Components/Dead End',
  component: DeadEnd,
};

const Template = (args: any) => (
  <DeadEnd {...args}>
    <DisplaySmall as="h2">Access denied</DisplaySmall>
    <Body as="p">You are not authorized to access this website.</Body>
    <Button color="primary">Go back</Button>
  </DeadEnd>
);

export const Default = Template.bind({});
