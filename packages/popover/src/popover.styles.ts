import styled, { css } from "styled-components";

interface StyledPopoverProps {
  isOpening?: boolean;
  open?: boolean;
}
export const StyledPopover = styled.div<StyledPopoverProps>`
  background-color: ${({ theme }) => theme.popover?.backgroundColor};
  box-shadow: ${({ theme }) => theme.popover?.boxShadow};
  padding: 16px;
  border-radius: 6px;
  max-width: 300px;
  opacity: 0;
  display: ${({ open }) => (open ? "block" : "none")};
  transition: opacity 0.2s ease-in-out;
  ${({ isOpening }) =>
    isOpening &&
    css`
      opacity: 1;
    `}

  &[data-popper-reference-hidden="true"] {
    visibility: hidden;
    pointer-events: none;
  }
`;

export const StyledPopoverTitle = styled.div`
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 8px;
`;
