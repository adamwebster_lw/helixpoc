import React, {
  useState,
  ReactNode,
  useEffect,
  MutableRefObject,
  useImperativeHandle,
  forwardRef,
  useRef,
  HTMLAttributes,
} from "react";
import { createPortal } from "react-dom";
import { DefaultTheme } from "styled-components";
import type { Placement, StrictModifiers } from "@popperjs/core";
import { usePopper } from "react-popper";
import { StyledPopover, StyledPopoverTitle } from "./popover.styles";
export interface PopoverRefProps {
  api: {
    open: () => void;
    close: () => void;
    toggle: () => void;
  };
}

export interface PopoverProps extends HTMLAttributes<HTMLDivElement> {
  /** The placement of the popover */
  placement?: Placement;
  /** The target of the popover.  Can be set to a string or an html element. */
  target: MutableRefObject<any> | HTMLElement | string;
  /** The content of the popover */
  children: ReactNode;
  /** The title of the popover */
  title?: string;
  /** The theme of the popover */
  theme?: DefaultTheme;
  /** The modifiers of the popover see popper.js modifiers*/
  popperModifiers?: StrictModifiers[];
  /** options for for the popover.*/
  popperOptions?: {
    /** Set the strategy to fixed if there are issues with overflow */
    strategy?: "absolute" | "fixed";
  };
  /** If a portal should be used to attach the portal to the dom */
  usePortal?: boolean;
  /** The trigger event for the popover */
  trigger?: "focus" | "click" | "hover";
  /**  If the popover has interactive elements in it such as a button or an input use this property */
  interactive?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const Popover = forwardRef<PopoverRefProps, PopoverProps>(
  (
    {
      placement = "auto" as Placement,
      children,
      target,
      title,
      popperModifiers,
      theme,
      popperOptions = {
        strategy: "absolute",
      },
      usePortal,
      trigger = "focus",
      interactive,
      dataLwtId,
      className,
      ...rest
    },
    ref
  ) => {
    const [referenceElement, setReferenceElement] =
      useState<HTMLElement | null>(null);
    const [popperElement, setPopperElement] =
      useState<HTMLElement | null>(null);
    const [open, setOpen] = useState(false);
    const [isOpening, setIsOpening] = useState(false);
    const popoverInner = useRef<HTMLDivElement>(null);
    const popoverClicked = useRef(false);
    const getPopperModifiers = () => {
      return popperModifiers && popperModifiers.length > 0
        ? popperModifiers
        : [];
    };

    const { styles, attributes, update } = usePopper(
      referenceElement,
      popperElement,
      {
        placement,
        ...popperOptions,
        modifiers: [
          { name: "offset", options: { offset: [0, 8] } },
          ...getPopperModifiers(),
        ],
      }
    );

    const openPopover = () => {
      setOpen(true);
      setIsOpening(true);
    };

    const closePopover = () => {
      setIsOpening(false);
      setTimeout(() => {
        setOpen(false);
      }, 200);
    };

    const handleMouseEnter = () => {
      openPopover();
    };

    const handleMouseLeave = () => {
      closePopover();
    };

    const togglePopover = () => {
      if (open) {
        closePopover();
      } else {
        openPopover();
      }
    };
    const handleClick = () => {
      togglePopover();
    };

    const handleOutsideClick = (e: MouseEvent) => {
      if (
        referenceElement &&
        popoverInner.current &&
        !referenceElement.contains(e.target as Node) &&
        !popoverInner.current.contains(e.target as Node)
      ) {
        closePopover();
      }
    };

    const handleBlur = (e) => {
      if (popoverClicked.current && interactive) return;
      // if the that was focused is the reference element, or the popover itself, or is a child of the popover, do nothing
      if (
        referenceElement &&
        popoverInner.current &&
        (referenceElement.contains(e.relatedTarget) ||
          popoverInner.current.contains(e.relatedTarget))
      ) {
        return;
      }
      closePopover();
    };

    const handlePopoverClick = () => {
      popoverClicked.current = true;
    };

    useEffect(() => {
      if (target) {
        const targetElement =
          typeof target === "string"
            ? document.getElementById(target)
            : (target as MutableRefObject<any>).current;

        setReferenceElement(targetElement);
        if (targetElement) {
          if (trigger === "hover") {
            // add mouse enter and mouse leave event listeners
            targetElement.addEventListener("mouseenter", handleMouseEnter);
            targetElement.addEventListener("mouseleave", handleMouseLeave);
            return () => {
              targetElement.removeEventListener("mouseenter", handleMouseEnter);
              targetElement.removeEventListener("mouseleave", handleMouseLeave);
            };
          }
          if (trigger === "click") {
            targetElement.addEventListener("click", handleClick);
            document.addEventListener("click", handleOutsideClick);
            return () => {
              targetElement.removeEventListener("click", handleClick);
              document.removeEventListener("click", handleOutsideClick);
            };
          }
          if (trigger === "focus") {
            targetElement.addEventListener("focus", handleClick);
            targetElement.addEventListener("blur", handleBlur);
            popoverInner.current?.addEventListener("click", handlePopoverClick);
            document.addEventListener("click", handleOutsideClick);
            return () => {
              targetElement.removeEventListener("focus", handleClick);
              targetElement.removeEventListener("blur", handleBlur);
              popoverInner.current?.removeEventListener(
                "click",
                handlePopoverClick
              );
              document.removeEventListener("click", handleOutsideClick);
            };
          }
        }
      }
    }, [target, open, trigger]);

    useEffect(() => {
      if (update) {
        update();
      }
    }, [update, open]);

    useImperativeHandle(ref, () => ({
      api: {
        open: () => {
          openPopover();
        },
        close: () => {
          closePopover();
        },
        toggle: () => {
          togglePopover();
        },
      },
    }));

    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }
    const popoverElement = (
      <StyledPopover
        className={`helix-popover ${className ? className : ""}`}
        ref={setPopperElement}
        style={styles.popper}
        isOpening={isOpening}
        open={open}
        {...optionalProps}
        {...rest}
        {...attributes.popper}
      >
        <div ref={popoverInner}>
          {title && <StyledPopoverTitle>{title}</StyledPopoverTitle>}
          {children}
        </div>
      </StyledPopover>
    );
    return (
      <>
        {usePortal
          ? createPortal(popoverElement, document.body)
          : popoverElement}
      </>
    );
  }
);

Popover.displayName = "Popover";
export default Popover;
