import { Button } from "@lwt-helix-nextgen/button";
import React from "react";
import { Popover, PopoverRefProps } from "./index";
import { Small } from "@lwt-helix-nextgen/typography";
import { Input } from "@lwt-helix-nextgen/input";
export default {
  title: "Components/Popover",
  component: Popover,
};

const Template = (args: any) => {
  return (
    <>
      <Button dataLwtId="" id="target">
        Click me
      </Button>
      <Popover target="target" {...args}>
        <Small>
          Popovers are small overlays that open on demand. They let users access
          additional content and actions without cluttering the page. This can
          be useful when help-text is needed to give more information about what
          industry-specific terms mean, etc.
        </Small>
      </Popover>
    </>
  );
};

export const Default: any = Template.bind({});

Default.args = {
  title: "Popover title",
};

export const EditableForm = () => {
  const popoverRef = React.useRef<PopoverRefProps>(null);
  const popoverButtonRef = React.useRef<HTMLButtonElement>(null);
  return (
    <>
      <Button ref={popoverButtonRef} dataLwtId="" id="target">
        Click me
      </Button>
      <Popover ref={popoverRef} interactive target="target">
        <div>
          <Input dataLwtId="" />
        </div>
        <div
          style={{
            marginTop: "8px",
            display: "flex",
            justifyContent: "flex-end",
            gap: "8px",
          }}
        >
          <Button
            onClick={() => {
              popoverRef.current?.api.close();
            }}
            dataLwtId="close"
          >
            Close
          </Button>
          <Button color="primary" dataLwtId="save">
            Save
          </Button>
        </div>
      </Popover>
    </>
  );
};

export const ToggleOnMouseEnter = () => {
  const popoverRef = React.useRef<PopoverRefProps>(null);
  return (
    <>
      <div id="target">Hover me</div>
      <Popover
        placement="auto-start"
        trigger="hover"
        ref={popoverRef}
        target="target"
      >
        <Small>
          Popovers are small overlays that open on demand. They let users access
          additional content and actions without cluttering the page. This can
          be useful when help-text is needed to give more information about what
          industry-specific terms mean, etc.
        </Small>
      </Popover>
    </>
  );
};

export const ToggleOnClick = () => {
  return (
    <>
      <span id="target">Click me</span>
      <Popover placement="auto-start" trigger="click" target="target">
        <Small>
          Popovers are small overlays that open on demand. They let users access
          additional content and actions without cluttering the page. This can
          be useful when help-text is needed to give more information about what
          industry-specific terms mean, etc.
        </Small>
      </Popover>
    </>
  );
};

export const PopperTargetRef = () => {
  const targetRef = React.useRef(null);
  return (
    <>
      <span id="target" ref={targetRef}>
        Hover me
      </span>
      <Popover trigger="hover" target={targetRef}>
        <Small>
          Popovers are small overlays that open on demand. They let users access
          additional content and actions without cluttering the page. This can
          be useful when help-text is needed to give more information about what
          industry-specific terms mean, etc.
        </Small>
      </Popover>
    </>
  );
};
