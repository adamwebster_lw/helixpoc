import React from "react";
import DropdownButton from "./src/dropdown-button";
import DropdownButtonItem from "./src/dropdown-button-item";
import DropdownButtonMenu from "./src/dropdown-button-menu";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { play_circle } from "@lwt-helix/helix-icon/outlined";

export default {
  title: "Components/DropdownButton",
  component: DropdownButton,
};

const Template = (args: any) => (
  <DropdownButton {...args} label="Dropdown">
    <DropdownButtonMenu>
      <DropdownButtonItem id="item 1" textLabel="Item 1">
        Item 1 <strong>Strong Text</strong>
      </DropdownButtonItem>
      <DropdownButtonItem id="item 2" textLabel="Item 2">
        <a href="https://google.ca" target="_black">
          Item 2
        </a>
      </DropdownButtonItem>
      <DropdownButtonItem
        onClick={() => alert("Click")}
        id="item 3"
        textLabel="Item 3"
      >
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "20px 1fr",
            gap: "6px",
          }}
        >
          <HelixIcon icon={play_circle} style={{ marginRight: "6px" }} />
          <span>Item 3</span>
        </div>
      </DropdownButtonItem>
    </DropdownButtonMenu>
  </DropdownButton>
);

export const Default = Template.bind({});

export const Primary: any = Template.bind({});
Primary.args = {
  color: "primary",
};

export const RemoveSplit: any = Template.bind({});
RemoveSplit.args = {
  removeSplit: true,
};

export const RemoveSplitPrimary: any = Template.bind({});
RemoveSplitPrimary.args = {
  removeSplit: true,
  color: "primary",
};

export const Disabled: any = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const DisabledPrimary: any = Template.bind({});
DisabledPrimary.args = {
  disabled: true,
  color: "primary",
};

export const DropdownButtonMenuCustomContent: any = () => (
  <DropdownButton label="Dropdown">
    <DropdownButtonMenu>Custom Content</DropdownButtonMenu>
  </DropdownButton>
);
