import React, {
  useState,
  ReactNode,
  useEffect,
  ButtonHTMLAttributes,
} from "react";
import { useDropdownMenu } from "../../hooks/useDropdownMenu";
import { HelixButtonColors } from "../../types";
import { Button } from "@lwt-helix-nextgen/button";
import { DropdownMenu, DropdownMenuItem } from "../../dropdown-menu";
import { HelixIcon } from "@lwt-helix/helix-icon";
import {
  chevron_small_down,
  chevron_small_right,
} from "@lwt-helix/helix-icon/outlined";
import { StyledDropdownButtonWrapper } from "./dropdown-button.styles";
export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  color?: HelixButtonColors;
  dataLwtId?: string;
  removeSplit?: boolean;
  children: ReactNode;
  label: string | ReactNode | (() => ReactNode);
}

export const DropdownButton = ({
  color = "tertiary",
  removeSplit,
  children,
  className,
  dataLwtId,
  disabled,
  label,
  ...rest
}: ButtonProps) => {
  const [dropdownItems, setDropdownMenuItems] = useState([]);

  const {
    isOpen,
    getToggleButtonProps,
    getMenuProps,
    highlightedIndex,
    getItemProps,
    setReferenceElement,
    setPopperElement,
    attributes,
    styles,
    update,
  } = useDropdownMenu({
    placement: "bottom-end",
    items: dropdownItems,
    itemToString: (item: any) => item.label,
  });

  const updateDropdownMenuItems = async () => {
    const items: any = [];
    const search = (children) => {
      React.Children.forEach(children, (child: any) => {
        console.log("child", child);
        if (
          child &&
          child.type &&
          child.type.displayName === "DropdownButtonItem"
        ) {
          console.log("items", items);
          items.push({
            id: child.props.id,
            label: child.props.textLabel,
            onClick: child.props.onClick ? child.props.onClick : () => {},
          });
        } else if (child.props && child.props.children) {
          search(child.props.children);
        }
      });
    };
    await search(children);
    setDropdownMenuItems(items);
  };

  const getDropdownMenuItemById = (id) => {
    // search recursively for the dropdown menu item with the given id
    const search = (children) => {
      let foundItem = null;
      React.Children.forEach(children, (child: any) => {
        if (
          child &&
          child.type &&
          child.type.displayName === "DropdownButtonItem" &&
          child.props.id === id
        ) {
          foundItem = child.props.children;
        }
        if (child.props && child.props.children && !foundItem) {
          foundItem = search(child.props.children);
        }
      });
      return foundItem;
    };
    return search(children);
  };

  // check if children contains DropdownMenuItems reclursively
  const checkIfHasDropdownMenuItems = (children) => {
    let hasDropdownMenuItems = false;
    React.Children.forEach(children, (child) => {
      if (
        child &&
        child.type &&
        child.type.displayName &&
        child.type.displayName === "DropdownButtonItem"
      ) {
        hasDropdownMenuItems = true;
      } else if (child && child.props && child.props.children) {
        hasDropdownMenuItems = checkIfHasDropdownMenuItems(
          child.props.children
        );
      }
    });
    return hasDropdownMenuItems;
  };

  const getChildrenOfDropdownButtonMenu = () => {
    const child: any = React.Children.toArray(children).find(
      (child: any) =>
        child &&
        child.type &&
        child.type.displayName &&
        child.type.displayName === "DropdownButtonMenu"
    );
    return child.props.children;
  };

  const dropdownIcon = () => (
    <HelixIcon icon={isOpen ? chevron_small_right : chevron_small_down} />
  );

  useEffect(() => {
    update && update();
  }, [isOpen]);

  useEffect(() => {
    updateDropdownMenuItems();
  }, [children]);

  return (
    <StyledDropdownButtonWrapper
      removeSplit={removeSplit}
      className={`lwt-helix-dropdown-button ${className ? className : ""}`}
      ref={setReferenceElement}
    >
      {removeSplit ? (
        <Button
          className="helix-dropdown-button__no-split-button"
          color={color}
          disabled={disabled}
          {...rest}
          {...getToggleButtonProps()}
        >
          {label}
          {dropdownIcon()}
        </Button>
      ) : (
        <>
          <Button
            className="helix-dropdown-button__button"
            dataLwtId={dataLwtId}
            color={color}
            disabled={disabled}
            {...rest}
          >
            {label as ReactNode}
          </Button>
          <Button
            className="helix-dropdown-button__split-toggle"
            color={color}
            disabled={disabled}
            {...getToggleButtonProps()}
          >
            {dropdownIcon()}
          </Button>
        </>
      )}
      <DropdownMenu
        maxWidth="300px"
        width="fit-content"
        ref={setPopperElement}
        isOpen={isOpen}
        style={styles.popper}
        {...attributes.popper}
      >
        {checkIfHasDropdownMenuItems(children) ? (
          <ul {...getMenuProps({})}>
            {isOpen &&
              dropdownItems.map((overflowItem: any, index: number) => (
                <DropdownMenuItem
                  highlightedIndex={highlightedIndex}
                  index={index}
                  menuItem={overflowItem}
                  key={`${overflowItem.label}${index}`}
                  getItemProps={getItemProps}
                >
                  {/* filter children to find an item with and id equal to overflowItem.id */}
                  {getDropdownMenuItemById(overflowItem.id)}
                </DropdownMenuItem>
              ))}
          </ul>
        ) : (
          <>{isOpen && getChildrenOfDropdownButtonMenu()}</>
        )}
      </DropdownMenu>
    </StyledDropdownButtonWrapper>
  );
};

export default DropdownButton;
