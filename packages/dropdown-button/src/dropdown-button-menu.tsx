import React, { HtmlHTMLAttributes } from "react";

export interface DropdownMenuProps
  extends HtmlHTMLAttributes<HTMLDivElement> {}

const DropdownButtonMenu = ({ ...rest }: DropdownMenuProps) => {
  return <></>;
};

DropdownButtonMenu.displayName = "DropdownButtonMenu";

export default DropdownButtonMenu;
