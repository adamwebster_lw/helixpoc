import styled from "styled-components";

interface StyledDropdownButtonWrapperProps {
  removeSplit?: boolean;
}
export const StyledDropdownButtonWrapper = styled.div<StyledDropdownButtonWrapperProps>`
  display: inline-flex;
  align-items: center;
  width: fit-content;
  .helix-dropdown-button__no-split-button {
    display: inline-flex;
    align-items: center;
    svg {
      path {
        fill: currentColor !important;
      }
    }
  }
  button:first-child:not(:only-child):not(.helix-dropdown-button__no-split-button) {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
  }
  button + button {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
    margin-left: 1px;
  }
  .helix-dropdown-button__split-toggle {
    padding: 8px;
    box-sizing: border-box;
    display: inline-flex;
    align-items: center;
    svg {
      margin-left: 0;
      path {
        fill: currentColor !important;
      }
    }
  }
`;
