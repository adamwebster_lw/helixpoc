import React, { HtmlHTMLAttributes, ReactNode } from "react";

export interface DropdownButtonItemProps
  extends HtmlHTMLAttributes<HTMLLIElement> {
  id: string;
  textLabel: string;
  children: ReactNode;
}
const DropdownButtonItem = ({
  children,
  textLabel,
  ...rest
}: DropdownButtonItemProps) => {
  return <></>;
};

DropdownButtonItem.displayName = "DropdownButtonItem";

export default DropdownButtonItem;
