import React from "react";
import { DemoPage } from "./index";
import { Small } from "@lwt-helix-nextgen/typography";
export default {
  title: "Samples/Demo Page",
  component: DemoPage,
};

const Template = (args: any) => {
  return (
    <>
      <DemoPage />
    </>
  );
};

export const Default: any = Template.bind({});

Default.args = {};

Default.parameters = {
  layout: "fullscreen",
};
