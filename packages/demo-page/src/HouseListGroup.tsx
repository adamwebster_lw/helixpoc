import React, { useState } from "react";
import { ListGroup, ListGroupItem } from "@lwt-helix-nextgen/list-group";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { checkmark } from "@lwt-helix/helix-icon/outlined";
const HouseListGroup = () => {
  const [houses, setHouse] = useState([
    {
      id: 1,
      address: "123 Main St",
      city: "San Francisco",
      price: 100000,
      active: true,
    },
    {
      id: 2,
      address: "456 Main St",
      city: "San Francisco",
      price: 200000,
      active: false,
    },
    {
      id: 3,
      address: "789 Main St",
      city: "San Francisco",
      price: 300000,
      active: false,
    },
  ]);

  return (
    <ListGroup>
      {houses.map((house, index) => (
        <ListGroupItem
          style={{
            display: "flex",
            justifyContent: "space-between",
            minWidth: "250px",
          }}
          key={house.id}
          active={house.active}
          onClick={() => {
            // set car at current index to active
            const newHouse = houses.map((house, i) => {
              if (i === index) {
                return {
                  ...house,
                  active: true,
                };
              }
              return {
                ...house,
                active: false,
              };
            });
            setHouse(newHouse);
          }}
        >
          <span>
            {house.address} - {house.city}
          </span>
          {house.active && (
            <HelixIcon
              style={{
                marginLeft: "16px",
              }}
              icon={checkmark}
            />
          )}
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

HouseListGroup.displayName = "HouseListGroup";

export default HouseListGroup;
