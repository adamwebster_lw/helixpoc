import React from "react";
import { Navbar } from "@lwt-helix/navbar";
import { DisplayLarge, DisplayMedium } from "@lwt-helix-nextgen/typography";
import { ContentContainer, GlobalStyles } from "./DemoPage.styles";
import {
  DashboardWidget,
  DashboardWidgets,
} from "@lwt-helix-nextgen/dashboard-widget";

import { ButtonToolbar } from "@lwt-helix-nextgen/button-toolbar";

import { Button } from "@lwt-helix-nextgen/button";
import { Tooltip } from "@lwt-helix-nextgen/tooltip";
import { ToastProvider } from "@lwt-helix-nextgen/toasts";
import { Breadcrumb } from "@lwt-helix-nextgen/breadcrumb";
import { Alert } from "@lwt-helix-nextgen/alert";
import NewHouseModal from "./NewHouseModal";
import DemoPageSlideoutPanel from "./DemoPageSlideoutPanel";
import StickyHouseFilterBar from "./StickyHouseFilterBar";
import ExportButton from "./ExportButton";
import HouseTable from "./HouseTable";
const DemoPage = () => {
  const [newHouseModalOpen, setNewHouseModalOpen] = React.useState(false);
  const [slideoutPanelId, setSlideoutPanelId] = React.useState(undefined);
  const [slideoutPanelContent, setSlideoutPanelContent] = React.useState({
    address: "",
    city: "",
    price: "",
    status: "active",
    date: "",
  });

  return (
    <>
      <GlobalStyles />
      <ToastProvider>
        <Navbar dataLwtId="Nav" applicationName="Helix | Demo Page" />
        <StickyHouseFilterBar />
        <ContentContainer>
          <Alert style={{ marginTop: "1rem" }} dataLwtId="" type="warning">
            This is a demo page for the Helix Design System. It is not intended
            for production use.
          </Alert>
          <DisplayLarge as="h1">Demo Page</DisplayLarge>
          <Breadcrumb
            dataLwtId=""
            style={{
              marginBottom: "1rem",
            }}
          >
            <a href="#">Home</a>
            <a href="#">Houses</a>
            <span>New</span>
          </Breadcrumb>
          <DashboardWidgets>
            <DashboardWidget
              label="Dashboard Widget"
              value="100"
              color="primary"
            />
            <DashboardWidget
              label="Dashboard Widget"
              value="100"
              color="danger"
            />
            <DashboardWidget
              label="Dashboard Widget"
              value="100"
              color="success"
            />
          </DashboardWidgets>
          <ButtonToolbar>
            <DisplayMedium as="h2">Houses</DisplayMedium>
            <div>
              <Tooltip placement="top" target="newHouse">
                Click here to add a new house
              </Tooltip>
              <Button
                onClick={() => setNewHouseModalOpen(true)}
                id="newHouse"
                dataLwtId="Button"
                color="primary"
              >
                Add House
              </Button>
              <ExportButton />
            </div>
          </ButtonToolbar>
          <HouseTable
            setSlideoutPanelContent={setSlideoutPanelContent}
            setSlideoutPanelId={setSlideoutPanelId}
          />
        </ContentContainer>
        <NewHouseModal
          isOpen={newHouseModalOpen}
          onClose={() => setNewHouseModalOpen(false)}
        />
        <DemoPageSlideoutPanel
          content={slideoutPanelContent}
          contentId={slideoutPanelId}
          setContentId={(contentId) => setSlideoutPanelId(undefined)}
        />
      </ToastProvider>
    </>
  );
};

DemoPage.displayName = "DemoPage";
export default DemoPage;
