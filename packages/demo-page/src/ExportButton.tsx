import React from "react";
import { DropdownButton, DropdownButtonItem } from "@lwt-helix-nextgen/dropdown-button";
import { useToast } from "@lwt-helix-nextgen/toasts";

const ExportButton = () => {
  const toasts = useToast();
  const handleExport = () => {
    toasts.addToast({
      id: "export",
      text: "Exporting...",
      type: "info",
    });

    setTimeout(() => {
      toasts.addToast({
        id: "export",
        text: "Exported!",
        type: "success",
      });
    }, 2000);
  };
  return (
    <DropdownButton
      label="Export"
      dataLwtId="export_button"
      onClick={handleExport}
    >
      <DropdownButtonItem
        onClick={handleExport}
        textLabel="Export as Excel"
        id="item1"
      >
        Export as Excel
      </DropdownButtonItem>
      <DropdownButtonItem
        onClick={handleExport}
        textLabel="Export as CSV"
        id="export_csv"
      >
        Export as CSV
      </DropdownButtonItem>
      <DropdownButtonItem
        onClick={handleExport}
        textLabel="Export as PDF"
        id="export_pdf"
      >
        Export as PDF
      </DropdownButtonItem>
    </DropdownButton>
  );
};

export default ExportButton;
