import React from "react";
import { AGGridHelix } from "@lwt-helix/ag-grid";
import {
  chevron_small_right,
  chevron_small_left,
  checkmark,
  pencil,
  x,
} from "@lwt-helix/helix-icon/outlined";
import NewPricePopover from "./NewPricePopover";
import { Button } from "@lwt-helix-nextgen/button";
import { Tooltip } from "@lwt-helix-nextgen/tooltip";
import { Pagination, PaginationItem } from "@lwt-helix-nextgen/pagination";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { Badge } from "@lwt-helix-nextgen/badge";
import {
  uniqueNamesGenerator,
  adjectives,
  colors,
  NumberDictionary,
  names,
} from "unique-names-generator";

interface HouseTableProps {
  setSlideoutPanelId: (id: any) => void;
  setSlideoutPanelContent: (content: any) => void;
}

const HouseTable = ({
  setSlideoutPanelId,
  setSlideoutPanelContent,
}: HouseTableProps) => {
  const randomAddress = () => {
    const number = Math.floor(Math.random() * 1000);
    const street = uniqueNamesGenerator({
      dictionaries: [adjectives, colors, names],
      length: 1,
      separator: " ",
    });
    // capitalize first letter of all words
    const capitalizedStreet = street
      .split(" ")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
    return `${number} ${capitalizedStreet}`;
  };

  const status = ["Active", "Pending", "Sold"];

  const randomCity = () => {
    const city = uniqueNamesGenerator({
      dictionaries: [adjectives, colors, names],
      length: 1,
      separator: " ",
    });

    // capitalize first letter
    return city.charAt(0).toUpperCase() + city.slice(1);
  };

  const randomPrice = () => {
    const price = Math.floor(Math.random() * 1000000);
    return `${price.toLocaleString()}`;
  };

  const randomStatus = () => {
    const randomIndex = Math.floor(Math.random() * status.length);
    return status[randomIndex];
  };

  const randomSold = () => {
    const sold = Math.floor(Math.random() * 2);
    return sold === 1 ? "Yes" : "No";
  };

  const randomDate = () => {
    const date = new Date();
    const randomDays = Math.floor(Math.random() * 100);
    date.setDate(date.getDate() - randomDays);
    return date.toLocaleDateString();
  };

  const generateData = () => {
    const data: any = [];
    for (let i = 0; i < 100; i++) {
      data.push({
        id: i,
        address: randomAddress(),
        city: randomCity(),
        price: randomPrice(),
        status: randomStatus(),
        date: randomDate(),
      });
    }
    return data;
  };

  const data = React.useMemo(() => generateData(), []);
  return (
    <div>
      <AGGridHelix
        columnDefs={[
          {
            field: "address",
            flex: 1,
            cellRenderer: (props) => {
              return (
                <Button
                  color="ghost"
                  onClick={() => {
                    setSlideoutPanelId(props.data.address);
                    setSlideoutPanelContent(props.data);
                  }}
                  dataLwtId=""
                >
                  {props.value}
                </Button>
              );
            },
          },
          { field: "city", flex: 1 },
          {
            field: "price",
            flex: 1,
            cellRenderer: (props) => {
              return (
                <span
                  style={{
                    display: "inline-flex",
                    alignItems: "center",
                    height: "100%",
                  }}
                >
                  ${props.value}
                  <span
                    style={{
                      display: "inline-flex",
                      alignItems: "center",
                      height: "100%",
                    }}
                    id={`${props.data.id}-popover`}
                  >
                    <HelixIcon icon={pencil} />
                    <NewPricePopover data={props.data} value={props.value} />
                  </span>
                </span>
              );
            },
          },
          {
            field: "status",
            cellRenderer: (props) => {
              return (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    height: "100%",
                  }}
                >
                  {props.value === "Active" && (
                    <Badge badgeStyle="success">{props.value}</Badge>
                  )}
                  {props.value === "Pending" && (
                    <Badge badgeStyle="warning">{props.value}</Badge>
                  )}
                  {props.value === "Sold" && (
                    <Badge badgeStyle="danger">{props.value}</Badge>
                  )}
                </div>
              );
            },
          },
        ]}
        rowData={data}
      />
      <Pagination
        style={{
          marginTop: "16px",
        }}
      >
        <PaginationItem iconItem>
          <HelixIcon icon={chevron_small_left} />
        </PaginationItem>
        <PaginationItem active>1</PaginationItem>
        <PaginationItem>2</PaginationItem>
        <PaginationItem>3</PaginationItem>
        <PaginationItem>4</PaginationItem>
        <PaginationItem>5</PaginationItem>
        <PaginationItem iconItem>
          <HelixIcon icon={chevron_small_right} />
        </PaginationItem>
      </Pagination>
    </div>
  );
};

export default HouseTable;
