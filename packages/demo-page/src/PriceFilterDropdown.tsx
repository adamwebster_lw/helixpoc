import React from "react";
import { Button } from "@lwt-helix-nextgen/button";
import { Popover } from "@lwt-helix-nextgen/popover";
import PriceListGroup from "./PriceListGroup";

const PriceFilterDropdown = () => {
  return (
    <>
      <Button id="price_filter" dataLwtId="" color="tertiary">
        Price
      </Button>
      <Popover placement="bottom" target="price_filter">
        <PriceListGroup />
      </Popover>
    </>
  );
};

export default PriceFilterDropdown;
