import styled, { css, createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
html{
  overflow: auto!important;
}
  body {
    background-color: #F8F8F8 !important;
  }
`;
export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 16px;
`;
