import React from "react";
import { Modal, ModalBody, ModalFooter, ModalHeader } from "@lwt-helix-nextgen/modal";
import { Button } from "@lwt-helix-nextgen/button";
import { Input } from "@lwt-helix-nextgen/input";
import { Radio } from "@lwt-helix-nextgen/radio";
import { DisplayLarge } from "@lwt-helix-nextgen/typography";
import { useToast } from "@lwt-helix-nextgen/toasts";

import { InputGroup, InputGroupAddon } from "@lwt-helix-nextgen/input-group";
interface NewHouseModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const NewHouseModal = ({ isOpen, onClose }: NewHouseModalProps) => {
  const toasts = useToast();
  return (
    <Modal title="Modal Title" show={isOpen} onClose={onClose}>
      <ModalHeader>
        <DisplayLarge as="h2" style={{ margin: 0 }}>
          Add New House
        </DisplayLarge>
      </ModalHeader>
      <ModalBody>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "1rem",
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="address">Address</label>

          <Input dataLwtId="" id="address" />
        </div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "1rem",
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="model">City</label>
          <Input dataLwtId="" id="city" />
        </div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "1rem",
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="price">Price</label>
          <InputGroup>
            <InputGroupAddon>$</InputGroupAddon>
            <Input dataLwtId="" id="price" />
          </InputGroup>
        </div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "1rem",
          }}
        >
          <span>Sold</span>
          <div>
            <Radio label="Yes" id="yes" name="sold" />
            <Radio label="No" id="no" name="sold" />
          </div>
        </div>
      </ModalBody>
      <ModalFooter>
        <div
          style={{ display: "flex", justifyContent: "flex-end", gap: "12px" }}
        >
          <Button dataLwtId="" onClick={onClose}>
            Cancel
          </Button>
          <Button
            dataLwtId=""
            color="primary"
            onClick={() => {
              toasts.addToast({
                id: "newCar",
                dataLwtId: "newCar",
                text: "Your car has been added",
                type: "success",
              });
              onClose();
            }}
          >
            Save
          </Button>
        </div>
      </ModalFooter>
    </Modal>
  );
};

export default NewHouseModal;
