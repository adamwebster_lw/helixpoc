import React from "react";
import { Button } from "@lwt-helix-nextgen/button";
import { Popover } from "@lwt-helix-nextgen/popover";
import HouseListGroup from "./HouseListGroup";

const HouseFilterDropdown = () => {
  return (
    <>
      <Button id="house_filter" dataLwtId="" color="tertiary">
        House
      </Button>
      <Popover placement="bottom" target="house_filter">
        <HouseListGroup />
      </Popover>
    </>
  );
};

export default HouseFilterDropdown;
