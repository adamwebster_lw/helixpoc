import React, { useState } from "react";
import { ListGroup, ListGroupItem } from "@lwt-helix-nextgen/list-group";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { checkmark } from "@lwt-helix/helix-icon/outlined";
const PriceListGroup = () => {
  const [prices, setPrices] = useState([
    {
      id: 1,
      price: "< $10000",
      active: true,
    },
    {
      id: 2,
      price: "$10000 - $20000",
      active: false,
    },
    {
      id: 3,
      price: "$20000 - $30000",
      active: false,
    },
  ]);

  return (
    <ListGroup>
      {prices.map((price, index) => (
        <ListGroupItem
          style={{
            display: "flex",
            justifyContent: "space-between",
            minWidth: "200px",
          }}
          key={price.id}
          active={price.active}
          onClick={() => {
            // set car at current index to active
            const newPrices = prices.map((price, i) => {
              if (i === index) {
                return {
                  ...price,
                  active: true,
                };
              }
              return {
                ...price,
                active: false,
              };
            });
            setPrices(newPrices);
          }}
        >
          <span>{price.price}</span>
          {price.active && <HelixIcon icon={checkmark} />}
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

PriceListGroup.displayName = "CarListGroup";

export default PriceListGroup;
