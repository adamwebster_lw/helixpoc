import React from "react";
import { Popover, PopoverRefProps } from "@lwt-helix-nextgen/popover";
import { Input } from "@lwt-helix-nextgen/input";
import { useToast } from "@lwt-helix-nextgen/toasts";
import { Button } from "@lwt-helix-nextgen/button";
import { InputGroup, InputGroupAddon } from "@lwt-helix-nextgen/input-group";
const NewPricePopover = (props) => {
  const ref = React.useRef<PopoverRefProps>(null);
  const toasts = useToast();
  return (
    <Popover
      ref={ref}
      usePortal
      trigger="click"
      placement="right"
      target={`${props.data.id}-popover`}
    >
      <div>
        <label htmlFor="new_price">Price</label>
        <div>
          <InputGroup>
            <InputGroupAddon>$</InputGroupAddon>
            <Input id="new_price" dataLwtId="" defaultValue={props.value} />
          </InputGroup>
        </div>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          marginTop: "8px",
          gap: "8px",
        }}
      >
        <Button
          dataLwtId=""
          onClick={() => {
            ref.current?.api?.close();
          }}
        >
          Close
        </Button>
        <Button
          dataLwtId=""
          color="primary"
          onClick={() => {
            ref.current?.api?.close();
            toasts.addToast({
              id: "price-change-success",
              dataLwtId: "price-change-success",
              text: "Price updated",
              type: "success",
            });
          }}
        >
          Save
        </Button>
      </div>
    </Popover>
  );
};

export default NewPricePopover;
