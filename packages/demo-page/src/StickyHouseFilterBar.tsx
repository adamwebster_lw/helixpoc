import React from "react";
import HouseFilterDropdown from "./HouseFilterDropdown";
import PriceFilterDropdown from "./PriceFilterDropdown";

const StickyHouseFilterBar = () => {
  return (
    <div
      className="sticky-top"
      style={{
        position: "sticky",
        display: "flex",
        gap: "8px",
        top: "0",
        zIndex: "1",
        backgroundColor: "white",
        padding: "10px",
        width: "100vw",
        boxSizing: "border-box",
      }}
    >
      <HouseFilterDropdown />
      <PriceFilterDropdown />
    </div>
  );
};

export default StickyHouseFilterBar;
