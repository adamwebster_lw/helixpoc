import React, { ReactNode } from "react";
import { ContextMenuProvider } from "./context-menu-context";
import ContextMenuMenu from "./context-menu-menu";

interface ContextMenuProps {
  children: ReactNode;
}

const ContextMenu = ({ children }: ContextMenuProps) => {
  return (
    <ContextMenuProvider>
      <ContextMenuMenu>{children}</ContextMenuMenu>
    </ContextMenuProvider>
  );
};

ContextMenu.displayName = "ContextMenu";

export default ContextMenu;
