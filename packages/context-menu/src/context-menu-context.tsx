import React, { createContext } from "react";
import { ContextMenuItemProps } from "./context-menu-item";

interface initialStateProps {
  isOpen: boolean;
  focusedItem: { level: number; index: number };
  contextMenuElement: HTMLElement;
  items: any[];
  menuLevel: number;
  submenuItems: [];
  previousMenus: any;
  previousMenusFocusIndexes: number[] | any;
  position: {
    x: number;
    y: number;
  };
}

const initialState = {
  focusedItem: { level: 0, index: 0 },
  contextMenuElement: {} as HTMLElement,
  isOpen: false,
  items: [{}],
  menuLevel: 0,
  submenuItems: [{}],
  previousMenus: [],
  previousMenusFocusIndexes: [],
  position: {
    x: 0,
    y: 0,
  },
};

export const ContextMenuContext = createContext({
  state: initialState,
  dispatch: (value: dispatchValuesProps | void) => value,
});
export const ContextMenuConsumer = ContextMenuContext.Consumer;

const reducer = (state: initialStateProps, action: dispatchValuesProps) => {
  switch (action.type) {
    case "OPEN":
      return {
        ...state,
        isOpen: true,
      };

    case "CLOSE":
      return {
        ...state,
        isOpen: false,
      };
    case "TOGGLE_OPEN": {
      return {
        ...state,
        isOpen: !state.isOpen,
      };
    }
    case "SET_FOCUSED_ITEM": {
      return {
        ...state,
        focusedItem: action.payload,
      };
    }
    case "SET_ITEMS": {
      return {
        ...state,
        items: action.payload,
      };
    }
    case "SET_POSITION":
      return {
        ...state,
        position: action.payload,
      };
    case "SET_SUBMENU_ITEMS":
      return {
        ...state,
        submenuItems: action.payload,
      };
    case "SET_MENU_LEVEL":
      return {
        ...state,
        menuLevel: action.payload,
      };
    case "ADD_PREVIOUS_MENU":
      return {
        ...state,
        previousMenus: [...state.previousMenus, action.payload],
      };
    case "REMOVE_PREVIOUS_MENU":
      return {
        ...state,
        previousMenus: state.previousMenus.slice(0, -1),
      };
    case "CLEAR_PREVIOUS_MENUS":
      return {
        ...state,
        previousMenus: [],
      };
    case "SET_CONTEXT_MENU_ELEMENT":
      return {
        ...state,
        contextMenuElement: action.payload,
      };
    case "SET_PREVIOUS_MENUS_FOCUS_INDEXES":
      return {
        ...state,
        previousMenusFocusIndexes: action.payload,
      };
    case "ADD_PREVIOUS_MENU_FOCUS_INDEXES":
      return {
        ...state,
        previousMenusFocusIndexes: [
          ...state.previousMenusFocusIndexes,
          action.payload,
        ],
      };
    case "REMOVE_PREVIOUS_MENU_FOCUS_INDEXES":
      return {
        ...state,
        previousMenusFocusIndexes: state.previousMenusFocusIndexes.slice(0, -1),
      };
    default:
      return state;
  }
};

interface dispatchValuesProps {
  type:
    | "OPEN"
    | "CLOSE"
    | "TOGGLE_OPEN"
    | "SET_FOCUSED_ITEM"
    | "SET_ITEMS"
    | "SET_POSITION"
    | "SET_SUBMENU_ITEMS"
    | "SET_MENU_LEVEL"
    | "ADD_PREVIOUS_MENU"
    | "REMOVE_PREVIOUS_MENU"
    | "SET_CONTEXT_MENU_ELEMENT"
    | "CLEAR_PREVIOUS_MENUS"
    | "SET_PREVIOUS_MENUS_FOCUS_INDEXES"
    | "ADD_PREVIOUS_MENU_FOCUS_INDEXES"
    | "REMOVE_PREVIOUS_MENU_FOCUS_INDEXES";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  payload?: any;
}

export const ContextMenuProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  return (
    <>
      <ContextMenuContext.Provider value={{ state, dispatch }}>
        {children}
      </ContextMenuContext.Provider>
    </>
  );
};
