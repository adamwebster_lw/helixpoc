import React, { MouseEvent, useState, useEffect, useContext } from "react";
import { ContextMenuContext } from "./context-menu-context";
import { StyledContextMenuItem } from "./context-menu.styles";

export interface ContextMenuItemProps {
  children: React.ReactNode;
  isFocused?: boolean;
  onClick?: (e: MouseEvent<HTMLDivElement>) => void;
  text?: string;
  htmlElement?: HTMLElement;
}

const ContextMenuItem = React.forwardRef<HTMLDivElement, ContextMenuItemProps>(
  ({ children, isFocused, onClick, htmlElement, ...rest }, ref) => {
    const { state, dispatch } = useContext(ContextMenuContext);
    const containsSubMenu = React.Children.toArray(children).some(
      (child: any) =>
        child.type &&
        child.type.displayName &&
        child.type.displayName === "ContextMenuSubmenu"
    );

    const onMouseEnter = (e: MouseEvent<HTMLDivElement>) => {
      if (containsSubMenu) {
        setSubMenuOpen(true);
        dispatch({ type: "SET_MENU_LEVEL", payload: state.menuLevel + 1 });
      }
    };

    const onMouseLeave = (e: MouseEvent<HTMLDivElement>) => {
      if (containsSubMenu) {
        setSubMenuOpen(false);
        dispatch({ type: "SET_MENU_LEVEL", payload: state.menuLevel - 1 });
      }
    };

    const handleClick = (e: MouseEvent<HTMLDivElement>) => {
      onClick && onClick(e);
      // if children contain an item with display name ContextMenuSubmenu
      // Then toggle subMenuOpen between true and false

      if (containsSubMenu) {
        if (
          subMenuOpen &&
          !htmlElement?.contains(e.target as HTMLElement) &&
          subMenuOpen
        ) {
          setSubMenuOpen(false);
          dispatch({ type: "SET_MENU_LEVEL", payload: state.menuLevel - 1 });
        } else {
          setSubMenuOpen(true);
          dispatch({ type: "SET_MENU_LEVEL", payload: state.menuLevel + 1 });
        }
      }
    };

    const [referenceElement, setReferenceElement] =
      useState<HTMLElement | null>(null);
    const [subMenuOpen, setSubMenuOpen] = useState(false);

    const clickOutside = (e: any) => {
      if (subMenuOpen && !referenceElement?.contains(e.target)) {
        setSubMenuOpen(false);
        if (state.menuLevel > 0) {
          dispatch({ type: "SET_MENU_LEVEL", payload: state.menuLevel - 1 });
          const previousMenu: HTMLElement =
            state.previousMenus[state.menuLevel - 1];
          previousMenu && previousMenu.focus();
        }
      }
    };

    useEffect(() => {
      setReferenceElement(htmlElement as HTMLElement);
    }, [htmlElement]);

    useEffect(() => {
      document.addEventListener("click", clickOutside);
      return () => {
        document.removeEventListener("click", clickOutside);
      };
    }, [subMenuOpen, referenceElement]);

    return (
      <StyledContextMenuItem
        onClick={(e) => handleClick(e)}
        onMouseEnter={(e) => onMouseEnter(e)}
        onMouseLeave={(e) => onMouseLeave(e)}
        isFocused={isFocused}
        ref={ref}
        {...rest}
      >
        {React.Children.map(children, (child: any) => {
          if (
            child.type &&
            child.type.displayName &&
            child.type.displayName === "ContextMenuSubmenu"
          ) {
            return (
              <div className="helix-menu--submenu-item">
                {React.cloneElement(child, {
                  referenceHTMLElement: referenceElement,
                  isOpen: subMenuOpen,
                  setSubMenuOpen: setSubMenuOpen,
                })}
              </div>
            );
          }
          return child;
        })}
        {containsSubMenu && (
          <svg
            width="20"
            height="20"
            viewBox="0 0 20 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M7.46967 14.5304C7.17678 14.2375 7.17678 13.7626 7.46967 13.4697L10.9393 10.0001L7.46967 6.53039C7.17678 6.2375 7.17678 5.76262 7.46967 5.46973C7.76256 5.17684 8.23744 5.17684 8.53033 5.46973L12.5303 9.46973C12.671 9.61038 12.75 9.80115 12.75 10.0001C12.75 10.199 12.671 10.3897 12.5303 10.5304L8.53033 14.5304C8.23744 14.8233 7.76256 14.8233 7.46967 14.5304Z"
              fill="#4B5563"
            />
          </svg>
        )}
      </StyledContextMenuItem>
    );
  }
);

ContextMenuItem.displayName = "ContextMenuItem";

export default ContextMenuItem;
