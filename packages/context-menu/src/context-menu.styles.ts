import styled, { css } from "styled-components";

interface StyledContextMenuProps {
  position?: { x: number; y: number };
}
export const StyledContextMenu = styled.div<StyledContextMenuProps>`
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1);
  max-width: 300px;
  position: absolute;
  z-index: 99;
  min-width: 200px;
  &:focus {
    //  outline: none;
  }
  ${(props) =>
    props.position &&
    css`
      left: ${props.position.x}px;
      top: ${props.position.y}px;
    `}
`;

interface StyledContextMenuItemProps {
  isFocused?: boolean;
}
export const StyledContextMenuItem = styled.div<StyledContextMenuItemProps>`
  display: flex;
  align-items: center;
  padding: 0.5rem 1rem;
  cursor: pointer;
  user-select: none;
  ${({ isFocused }) =>
    isFocused &&
    css`
      background-color: #f5f5f5;
    `}
  &:hover {
    background-color: #f5f5f5;
  }
  .helix-menu--submenu-item {
    flex: 1 1;
  }
`;
