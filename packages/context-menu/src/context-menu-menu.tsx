import React, {
  useEffect,
  useState,
  useRef,
  useContext,
  KeyboardEvent,
} from "react";
import { ContextMenuContext } from "./context-menu-context";
import { StyledContextMenu } from "./context-menu.styles";
import { usePopper } from "react-popper";

interface ContextMenuMenuProps {
  children: React.ReactNode;
}

const ContextMenuMenu = React.forwardRef<HTMLDivElement, any>(
  ({ children }: ContextMenuMenuProps, ref) => {
    const { state, dispatch } = useContext(ContextMenuContext);
    const [virtualReference, setVirtualReference] = useState({
      getBoundingClientRect() {
        return {
          top: 0,
          left: 0,
          bottom: 20,
          right: 0,
          width: 90,
          height: 10,
        };
      },
    });
    const [popperElement, setPopperElement] =
      React.useState<HTMLDivElement | null>(null);
    const { styles, attributes } = usePopper(
      virtualReference as any,
      popperElement,
      {
        placement: "top-start",
      }
    );

    const menuItemRefs = useRef<HTMLDivElement[]>([]);

    const handleContextMenu = (e: any) => {
      e.preventDefault();
      dispatch({ type: "TOGGLE_OPEN" });
      dispatch({ type: "SET_FOCUSED_ITEM", payload: { level: 0, index: 0 } });
      dispatch({
        type: "SET_POSITION",
        payload: { x: e.clientX, y: e.clientY },
      });
      setVirtualReference({
        getBoundingClientRect() {
          return {
            top: e.clientY,
            left: e.clientX,
            bottom: 20,
            right: 0,
            width: 90,
            height: 10,
          };
        },
      });
    };

    // reclusive search popperElement for target element
    const containsElement = (element: any, target) => {
      if (element === null) {
        return null;
      }
      if (element.contains(target)) {
        return true;
      }
      return containsElement(element, target);
    };

    // close context menu when user clicks outside of it
    const handleClickOutside = (e: any) => {
      if (
        state.isOpen &&
        popperElement &&
        !containsElement(popperElement, e.target)
      ) {
        dispatch({ type: "CLOSE" });
      }
    };

    const containsSubMenu = (item) => {
      return (
        item &&
        item.props &&
        item.props.children &&
        item.props.children.some &&
        item.props.children.some(
          (child: any) =>
            child.type &&
            child.type.displayName &&
            child.type.displayName === "ContextMenuSubmenu"
        )
      );
    };
    // change focus when user presses arrow keys
    const handleKeyDown = (e: KeyboardEvent) => {
      if (state.isOpen) {
        const item: any = state.items[state.focusedItem.index];
        switch (e.key) {
          case "ArrowUp":
            e.preventDefault();
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: 0, index: state.focusedItem.index - 1 },
            });

            // if we're at the top of the list, go to the bottom
            if (state.focusedItem.index === 0) {
              dispatch({
                type: "SET_FOCUSED_ITEM",
                payload: { level: 0, index: state.items.length - 1 },
              });
            }
            break;
          case "ArrowDown":
            e.preventDefault();
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: 0, index: state.focusedItem.index + 1 },
            });
            // if we're at the bottom of the list, go to the top
            if (state.focusedItem.index === state.items.length - 1) {
              dispatch({
                type: "SET_FOCUSED_ITEM",
                payload: { level: 0, index: 0 },
              });
            }
            break;
          case "Home":
            e.preventDefault();
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: 0, index: 0 },
            });
            break;
          case "End":
            e.preventDefault();
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: 0, index: state.items.length - 1 },
            });
            break;
          case "Enter":
            e.preventDefault();
            // check if item.props.children contains a submenu
            if (
              item &&
              item.props &&
              item.props.children &&
              containsSubMenu(item)
            ) {
              menuItemRefs.current[state.focusedItem.index].click();
            }

            if (item.props.onClick) {
              item.props.onClick();
            } else {
              // find clickable button or link and click it
              const clickable: HTMLElement = menuItemRefs.current[
                state.focusedItem.index
              ].querySelector("button, a") as HTMLElement;
              if (clickable) {
                clickable.click();
              }
            }
            break;
          case "ArrowRight":
            e.preventDefault();
            // check if item.props.children contains a submenu
            if (
              item &&
              item.props &&
              item.props.children &&
              containsSubMenu(item)
            ) {
              dispatch({
                type: "SET_MENU_LEVEL",
                payload: state.menuLevel + 1,
              });
              dispatch({
                type: "ADD_PREVIOUS_MENU_FOCUS_INDEXES",
                payload: state.focusedItem.index,
              });
              menuItemRefs.current[state.focusedItem.index].click();
            }
            break;
          case "Escape":
            e.preventDefault();
            dispatch({ type: "CLOSE" });
            break;
          // on any letter key press focus the item that starts with that letter
          case "a":
          case "b":
          case "c":
          case "d":
          case "e":
          case "f":
          case "g":
          case "h":
          case "i":
          case "j":
          case "k":
          case "l":
          case "m":
          case "n":
          case "o":
          case "p":
          case "q":
          case "r":
          case "s":
          case "t":
          case "u":
          case "v":
          case "w":
          case "x":
          case "y":
          case "z":
            e.preventDefault();
            // find the next item that starts with that letter
            const nextItem = state.items.find(
              (item: any, index) =>
                ((item.props.children.toLowerCase &&
                  item.props.children.toLowerCase().startsWith(e.key)) ||
                  (item.props.text &&
                    item.props.text.toLowerCase &&
                    item.props.text.toLowerCase().startsWith(e.key))) &&
                index > state.focusedItem.index
            );
            if (nextItem) {
              dispatch({
                type: "SET_FOCUSED_ITEM",
                payload: { level: 0, index: state.items.indexOf(nextItem) },
              });
            }

            // if there are no next items that start with that letter check it previous items
            else {
              const previousItem = state.items.find(
                (item: any, index) =>
                  ((item.props.children.toLowerCase &&
                    item.props.children.toLowerCase().startsWith(e.key)) ||
                    (item.props.text &&
                      item.props.text.toLowerCase &&
                      item.props.text.toLowerCase().startsWith(e.key))) &&
                  index < state.focusedItem.index
              );
              if (previousItem) {
                dispatch({
                  type: "SET_FOCUSED_ITEM",
                  payload: {
                    level: 0,
                    index: state.items.indexOf(previousItem),
                  },
                });
              }
            }

            break;
          default:
            break;
        }
      }
    };

    // focus menu when it opens
    useEffect(() => {
      if (state.isOpen) {
        popperElement?.focus();
      }
    }, [state.isOpen, popperElement]);

    useEffect(() => {
      // add context click handler
      document.addEventListener("contextmenu", handleContextMenu);
      // add click handler to close context menu
      document.addEventListener("click", handleClickOutside);
      // remove context click handler
      return () => {
        document.removeEventListener("contextmenu", handleContextMenu);
        document.removeEventListener("click", handleClickOutside);
      };
    }, [state.isOpen, popperElement]);

    useEffect(() => {
      // dispatch menu item children to context

      dispatch({
        type: "SET_ITEMS",
        payload: React.Children.toArray(children).filter(
          (child: any) => child.type.displayName === "ContextMenuItem"
        ),
      });
    }, [children]);

    useEffect(() => {
      dispatch({
        type: "SET_CONTEXT_MENU_ELEMENT",
        payload: popperElement as HTMLElement,
      });
    }, [popperElement]);

    useEffect(() => {
      if (!state.isOpen) {
        dispatch({
          type: "SET_FOCUSED_ITEM",
          payload: {
            level: 0,
            item: 0,
          },
        });
        dispatch({
          type: "SET_MENU_LEVEL",
          payload: 0,
        });
        dispatch({
          type: "SET_PREVIOUS_MENUS_FOCUS_INDEXES",
          payload: [],
        });
      }
    }, [state.isOpen]);

    return (
      <>
        {JSON.stringify(state.focusedItem)}
        {JSON.stringify(state.menuLevel)}
        {JSON.stringify(state.previousMenusFocusIndexes)}
        {state.isOpen && (
          <StyledContextMenu
            //  position={state.position}
            onKeyDown={(e) => handleKeyDown(e)}
            tabIndex={0}
            ref={setPopperElement}
            style={styles.popper}
            {...attributes.popper}
          >
            {React.Children.map(children, (child: any, index) => {
              if (child.type.displayName === "ContextMenuItem") {
                return React.cloneElement(child, {
                  isFocused:
                    (state.focusedItem.level === 0 &&
                      state.focusedItem.index === index) ||
                    state.previousMenusFocusIndexes[0] === index,
                  id: `context-menu-item-${index}`,
                  ref: (ref: any) => {
                    menuItemRefs.current[index] = ref;
                  },
                  htmlElement: menuItemRefs.current[index],
                });
              }
              return child;
            })}
          </StyledContextMenu>
        )}
      </>
    );
  }
);

export default ContextMenuMenu;
