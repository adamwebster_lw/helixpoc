import React, {
  ReactNode,
  useEffect,
  useState,
  KeyboardEvent,
  useContext,
  useRef,
} from "react";
import { usePopper } from "react-popper";
import { ContextMenuContext } from "./context-menu-context";
import { StyledContextMenu } from "./context-menu.styles";

interface ContextMenuSubmenuProps {
  children: ReactNode;
  referenceHTMLElement?: HTMLElement;
  isOpen?: boolean;
  setSubMenuOpen?: (isOpen: boolean) => void;
}

const ContextMenuSubmenu = ({
  children,
  referenceHTMLElement,
  isOpen,
  setSubMenuOpen,
}: ContextMenuSubmenuProps) => {
  const [referenceElement, setReferenceElement] =
    useState<HTMLDivElement | null>(null);
  const [popperElement, setPopperElement] =
    useState<HTMLDivElement | null>(null);

  const { state, dispatch } = useContext(ContextMenuContext);
  const menuItemRefs = useRef<HTMLDivElement[]>([]);
  const submenuItems = useRef<any[]>([]);
  const [menuLevel, setMenuLevel] = useState(state.menuLevel + 1);
  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    placement: "right-start",
    modifiers: [
      {
        name: "flip",
        options: {
          fallbackPlacements: ["left-start", "right-end", "left-end"],
        },
      },
      {
        name: "offset",
        options: {
          offset: [0, -8],
        },
      },
    ],
  });

  const containsSubMenu = (item) => {
    return (
      item &&
      item.props &&
      item.props.children &&
      item.props.children.some &&
      item.props.children.some(
        (child: any) =>
          child.type &&
          child.type.displayName &&
          child.type.displayName === "ContextMenuSubmenu"
      )
    );
  };

  const handleKeyDown = (e: KeyboardEvent) => {
    if (isOpen) {
      const item: any = submenuItems.current[state.focusedItem.index];
      switch (e.key) {
        case "ArrowUp":
          e.preventDefault();
          e.stopPropagation();
          dispatch({
            type: "SET_FOCUSED_ITEM",
            payload: {
              level: menuLevel,
              index: state.focusedItem.index - 1,
            },
          });

          // if we're at the top of the list, go to the bottom
          if (
            state.focusedItem.level === menuLevel &&
            state.focusedItem.index === 0
          ) {
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: {
                level: menuLevel,
                index: submenuItems.current.length - 1,
              },
            });
          }

          break;
        case "ArrowDown":
          e.preventDefault();
          e.stopPropagation();
          dispatch({
            type: "SET_FOCUSED_ITEM",
            payload: {
              level: menuLevel,
              index: state.focusedItem.index + 1,
            },
          });
          // if we're at the bottom of the list, go to the top
          if (
            (state.focusedItem.level === menuLevel,
            state.focusedItem.index === submenuItems.current.length - 1)
          ) {
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: menuLevel, index: 0 },
            });
          }
          break;
        case "Home":
          e.preventDefault();
          e.stopPropagation();
          dispatch({
            type: "SET_FOCUSED_ITEM",
            payload: { level: menuLevel, index: 0 },
          });
          break;
        case "End":
          e.preventDefault();
          e.stopPropagation();
          dispatch({
            type: "SET_FOCUSED_ITEM",
            payload: {
              level: menuLevel,
              index: submenuItems.current.length - 1,
            },
          });
          break;
        case "Enter":
          e.preventDefault();
          e.stopPropagation();
          // check if focused item has an onClick function and call it
          if (
            item &&
            item.props &&
            item.props.children &&
            containsSubMenu(item)
          ) {
            menuItemRefs.current[state.focusedItem.index].click();
          }
          if (item.props.onClick) {
            item.props.onClick();
          } else {
            // find clickable button or link and click it
            const clickable: HTMLElement = menuItemRefs.current[
              state.focusedItem.index
            ].querySelector("button, a") as HTMLElement;
            if (clickable) {
              clickable.click();
            }
          }
          break;
        case "ArrowRight":
          e.preventDefault();
          e.stopPropagation();

          // check if item.props.children contains a submenu
          if (
            item &&
            item.props &&
            item.props.children &&
            containsSubMenu(item)
          ) {
            menuItemRefs.current[state.focusedItem.index].click();
            dispatch({
              type: "SET_MENU_LEVEL",
              payload: state.menuLevel + 1,
            });
            dispatch({
              type: "ADD_PREVIOUS_MENU_FOCUS_INDEXES",
              payload: state.focusedItem.index,
            });
          }
          break;
        case "ArrowLeft":
          e.preventDefault();
          e.stopPropagation();
          if (isOpen) {
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: { level: state.focusedItem.level - 1, index: 0 },
            });
            setSubMenuOpen && setSubMenuOpen(false);

            if (state.previousMenus) {
              const previousMenu: HTMLElement = [...state.previousMenus][
                state.menuLevel - 2
              ];
              if (previousMenu) {
                dispatch({
                  type: "REMOVE_PREVIOUS_MENU_FOCUS_INDEXES",
                });
                dispatch({
                  type: "REMOVE_PREVIOUS_MENU",
                });
                previousMenu.focus && previousMenu.focus();
              } else {
                state.contextMenuElement && state.contextMenuElement.focus();
                dispatch({
                  type: "SET_PREVIOUS_MENUS_FOCUS_INDEXES",
                  payload: [],
                });
                dispatch({ type: "CLEAR_PREVIOUS_MENUS" });
              }
              dispatch({
                type: "SET_MENU_LEVEL",
                payload: state.menuLevel - 1,
              });
            }
          }
          break;
        // on any letter key press focus the item that starts with that letter
        case "a":
        case "b":
        case "c":
        case "d":
        case "e":
        case "f":
        case "g":
        case "h":
        case "i":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "o":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "u":
        case "v":
        case "w":
        case "x":
        case "y":
        case "z":
          e.preventDefault();
          e.stopPropagation();
          // find the next item that starts with that letter
          const nextItem = submenuItems.current.find(
            (item: any, index) =>
              ((item.props.children.toLowerCase &&
                item.props.children.toLowerCase().startsWith(e.key)) ||
                (item.props.text &&
                  item.props.text.toLowerCase &&
                  item.props.text.toLowerCase().startsWith(e.key))) &&
              index > state.focusedItem.index
          );
          if (nextItem) {
            dispatch({
              type: "SET_FOCUSED_ITEM",
              payload: {
                level: menuLevel,
                index: submenuItems.current.indexOf(nextItem),
              },
            });
          }

          // if there are no next items that start with that letter check it previous items
          else {
            const previousItem = submenuItems.current.find(
              (item: any, index) =>
                ((item.props.children.toLowerCase &&
                  item.props.children.toLowerCase().startsWith(e.key)) ||
                  (item.props.text &&
                    item.props.text.toLowerCase &&
                    item.props.text.toLowerCase().startsWith(e.key))) &&
                index < state.focusedItem.index
            );
            if (previousItem) {
              dispatch({
                type: "SET_FOCUSED_ITEM",
                payload: {
                  level: menuLevel,
                  index: submenuItems.current.indexOf(previousItem),
                },
              });
            }
          }
          break;
        default:
          break;
      }
    }
  };
  useEffect(() => {
    setReferenceElement(referenceHTMLElement as HTMLDivElement);
  }, [referenceHTMLElement]);

  useEffect(() => {
    if (isOpen) {
      popperElement?.focus();
      dispatch({
        type: "SET_FOCUSED_ITEM",
        payload: { level: menuLevel, index: 0 },
      });
      if (popperElement) {
        // check if item has a submenu
        dispatch({
          type: "ADD_PREVIOUS_MENU",
          payload: popperElement,
        });
      }
    }
  }, [isOpen, popperElement]);

  useEffect(() => {
    // set menu item children to ref
    submenuItems.current = React.Children.toArray(children).filter(
      (child: any) => child.type.displayName === "ContextMenuItem"
    );
  }, [children]);

  return (
    <>
      {isOpen && (
        <StyledContextMenu
          ref={setPopperElement}
          style={styles.popper}
          tabIndex={0}
          onKeyDown={(e) => handleKeyDown(e)}
          {...attributes.popper}
        >
          {menuLevel}
          {React.Children.map(children, (child: any, index) => {
            if (child.type.displayName === "ContextMenuItem") {
              return React.cloneElement(child, {
                isFocused:
                  (state.focusedItem.level === menuLevel &&
                    state.focusedItem.index === index) ||
                  state.previousMenusFocusIndexes[menuLevel] === index,

                id: `context-submenu-item-${index}`,
                ref: (ref: any) => {
                  menuItemRefs.current[index] = ref;
                },
                htmlElement: menuItemRefs.current[index],
              });
            }
            return child;
          })}
        </StyledContextMenu>
      )}
    </>
  );
};

ContextMenuSubmenu.displayName = "ContextMenuSubmenu";

export default ContextMenuSubmenu;
