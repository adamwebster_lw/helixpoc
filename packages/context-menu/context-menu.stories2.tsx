import React from "react";
import ContextMenu from "./src/context-menu";
import ContextMenuItem from "./src/context-menu-item";
import ContextMenuSubmenu from "./src/context-menu-submenu";

export default {
  title: "Components/Context Menu",
  component: ContextMenu,
};

const Template = (args: any) => (
  <ContextMenu aria-label="Actions">
    <ContextMenuItem key="one" onClick={() => alert("Clicked")}>
      One (onClick)
    </ContextMenuItem>
    <ContextMenuItem text="Two" key="two">
      <a href="https://google.ca">Two</a>
    </ContextMenuItem>
    <ContextMenuItem key="three">Three</ContextMenuItem>
    <ContextMenuItem key="four" text="Four">
      Four
      <ContextMenuSubmenu>
        <ContextMenuItem onClick={() => alert("Four One")} key="four-one">
          Four One
        </ContextMenuItem>
        <ContextMenuItem key="four-two" text="four-two">
          <a href="https://microsoft.com">Four Two</a>
        </ContextMenuItem>
        <ContextMenuItem key="four-three">
          Four Three
          <ContextMenuSubmenu>
            <ContextMenuItem
              onClick={() => alert("Four Three One")}
              key="four-three-one"
            >
              Four Three One
            </ContextMenuItem>
            <ContextMenuItem key="four-three-two" text="four-two">
              <a href="https://apple.com">Four Three Two</a>
            </ContextMenuItem>
            <ContextMenuItem key="four-three-three">
              Four Three Three
              <ContextMenuSubmenu>
                <ContextMenuItem
                  onClick={() => alert("Four Three One")}
                  key="four-three-one"
                >
                  Four Three Three One
                </ContextMenuItem>
                <ContextMenuItem key="four-three-two" text="four-two">
                  <a href="https://apple.com">Four There Three Two</a>
                </ContextMenuItem>
                <ContextMenuItem key="four-three-three">
                  Four Three Three Three
                </ContextMenuItem>
              </ContextMenuSubmenu>
            </ContextMenuItem>
          </ContextMenuSubmenu>
        </ContextMenuItem>
      </ContextMenuSubmenu>
    </ContextMenuItem>
    <ContextMenuItem key="five">Five</ContextMenuItem>
    <ContextMenuItem key="six">Six</ContextMenuItem>
    <ContextMenuItem key="seven">
      Seven
      <ContextMenuSubmenu>
        <ContextMenuItem key="seven-one">Seven One</ContextMenuItem>
        <ContextMenuItem key="seven-two">Seven Two</ContextMenuItem>
        <ContextMenuItem key="seven-three">
          Seven Three
          <ContextMenuSubmenu>
            <ContextMenuItem key="seven-three-one">
              Seven Three One
            </ContextMenuItem>
            <ContextMenuItem key="seven-three-two">
              Seven Three Two
            </ContextMenuItem>
            <ContextMenuItem key="seven-three-three">
              Seven Three Three
            </ContextMenuItem>
          </ContextMenuSubmenu>
        </ContextMenuItem>
      </ContextMenuSubmenu>
    </ContextMenuItem>
    <ContextMenuItem key="eight">Eight</ContextMenuItem>
    <ContextMenuItem key="nine">Nine</ContextMenuItem>
    <ContextMenuItem key="ten">Ten</ContextMenuItem>
  </ContextMenu>
);

export const Default = Template.bind({});
