import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledListGroup } from "./list-group.styles";

export interface ButtonGroupProps extends HtmlHTMLAttributes<HTMLUListElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const ListGroup = ({
  className,
  children,
  dataLwtId,
  ...rest
}: ButtonGroupProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledListGroup
      className={`helix-list-group ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledListGroup>
  );
};

ListGroup.displayName = 'ListGroup';

export default ListGroup;
