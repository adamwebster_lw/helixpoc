import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledListGroupItem } from "./list-group.styles";

export interface ListGroupItemProps extends HtmlHTMLAttributes<HTMLLIElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** If the item is active or not */
  active?: boolean;
}

const ListGroupItem = ({
  children,
  dataLwtId,
  className,
  active,
  ...rest
}: ListGroupItemProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledListGroupItem
      className={`helix-list-group-item ${className ? className : ""}`}
      active={active}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledListGroupItem>
  );
};

ListGroupItem.displayName = "ListGroupItem";

export default ListGroupItem;
