import styled, { css } from "styled-components";

export const StyledListGroup = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;

interface StyledListGroupItemProps {
  active?: boolean;
}

export const StyledListGroupItem = styled.li<StyledListGroupItemProps>`
  padding: 8px;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: ${({ theme }) =>
      theme.listGroup?.itemHoverBackgroundColor};
  }

  ${({ active, theme }) =>
    active &&
    css`
      background-color: ${theme.listGroup?.itemActiveBackgroundColor};
    `}
`;
