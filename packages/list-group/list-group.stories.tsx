import React from "react";
import ButtonGroup from "./src/list-group";
import ListGroup from "./src/list-group";
import ListGroupItem from "./src/list-group-item";

export default {
  title: "Components/List Group",
  component: ListGroup,
};

const Template = (args: any) => {
  return (
    <ListGroup {...args}>
      <ListGroupItem active>Cras justo odio</ListGroupItem>
      <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
      <ListGroupItem>Morbi leo risus</ListGroupItem>
      <ListGroupItem>Porta ac consectetur ac</ListGroupItem>
      <ListGroupItem>Vestibulum at eros</ListGroupItem>
    </ListGroup>
  );
};

export const Default = Template.bind({});

export const Primary: any = Template.bind({});
Primary.args = {
  color: "primary",
};
