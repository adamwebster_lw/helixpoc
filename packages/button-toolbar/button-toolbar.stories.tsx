import React from "react";
import ButtonToolbar from "./src/button-toolbar";
import { Button } from "@lwt-helix-nextgen/button";
import { DropdownButton, DropdownButtonItem } from "@lwt-helix-nextgen/dropdown-button";
import { DisplaySmall } from "@lwt-helix-nextgen/typography";

export default {
  title: "Components/Button Toolbar",
  component: ButtonToolbar,
};

const Template = (args: any) => (
  <>
  <br />
  <br />
    <ButtonToolbar {...args}>
      <DisplaySmall>Button Toolbar</DisplaySmall>
      <div>
        <Button dataLwtId="button1">Button</Button>
        <DropdownButton label="Dropdown" dataLwtId="dropdown_button">
          <DropdownButtonItem textLabel="item1" id="item1">
            Item 1
          </DropdownButtonItem>
          <DropdownButtonItem textLabel="item2" id="item2">
            Item 2
          </DropdownButtonItem>
          <DropdownButtonItem textLabel="item3" id="item3">
            Item 3
          </DropdownButtonItem>
        </DropdownButton>
      </div>
    </ButtonToolbar>
  </>
);

export const Default = Template.bind({});

export const Primary: any = Template.bind({});
Primary.args = {
  color: "primary",
};
