import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledButtonToolbar } from "./button-toolbar.styles";

export interface ButtonGroupProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const ButtonGroup = ({
  className,
  children,
  dataLwtId,
  ...rest
}: ButtonGroupProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledButtonToolbar
      className={`lwt-helix-button-toolbar ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledButtonToolbar>
  );
};

// ButtonGroup.displayName = 'ButtonGroup';

export default ButtonGroup;
