import styled from "styled-components";

export const StyledButtonToolbar = styled.div`
  button + button,
  button + .lwt-helix-dropdown-button,
  .lwt-helix-dropdown-button + button {
    margin-left: 12px;
  }

  .lwt-helix-dropdown-button {
    button + button {
      margin-left: 1px;
    }
  }
  
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;
