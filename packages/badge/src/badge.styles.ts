import styled, { css } from "styled-components";
import { BadgeSizes, BadgeStyles } from "./badge";

interface SBProps {
  size: BadgeSizes;
  badgeStyle: BadgeStyles;
}

const getFontSize = (size: BadgeSizes) => {
  switch (size) {
    case "small":
      return "14px";
    case "tiny":
      return "13px";
    case "medium":
    default:
      return "16";
  }
};

const getPadding = (size: BadgeSizes) => {
  switch (size) {
    case "small":
      return "2px 8px";
    case "tiny":
      return "2px 8px";
    case "medium":
    default:
      return "2px 8px";
  }
};

const getLineHeight = (size: BadgeSizes) => {
  switch (size) {
    case "small":
      return "16px";
    case "tiny":
      return "13px";
    case "medium":
    default:
      return "20px";
  }
};
export const StyledBadge = styled.span<SBProps>`
  background-color: ${({ theme, badgeStyle }) =>
    theme.badge![badgeStyle]!.backgroundColor};
  border-radius: 99px;
  ${({ size }) =>
    size &&
    css`
      font-size: ${getFontSize(size)};
      padding: ${getPadding(size)};
      line-height: ${getLineHeight(size)};
    `}
  color: ${({ theme, badgeStyle }) => theme.badge![badgeStyle]!.textColor};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  .dismiss-option-button {
    background-color: transparent;
    border: none;
    margin-left: 10px;
    display: inline-flex;
    padding: 0;
    cursor: pointer;
  }
`;
