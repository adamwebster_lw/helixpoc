import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledBadge } from "./badge.styles";

export type BadgeSizes = "medium" | "small" | "tiny";
export type BadgeStyles =
  | "default"
  | "inverted"
  | "danger"
  | "warning"
  | "success"
  | "info";

export interface BadgeProps extends HtmlHTMLAttributes<HTMLSpanElement> {
  children: ReactNode;
  /** The size of the badge */
  size?: BadgeSizes;
  /** The style of the badge */
  badgeStyle?: BadgeStyles;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

export const Badge = ({
  children,
  size = "medium",
  badgeStyle = "default",
  dataLwtId,
  className,
  ...rest
}: BadgeProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledBadge
      badgeStyle={badgeStyle}
      size={size}
      className={`helix-badge ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledBadge>
  );
};

export default Badge;
