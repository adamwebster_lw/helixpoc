import React from 'react';
import Badge from './src/badge';

export default {
  title: 'Components/Badge',
  component: Badge,
};

const Template = (args: any) => <Badge {...args}>Badge</Badge>;

export const Default = Template.bind({});
