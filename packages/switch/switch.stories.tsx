import React from 'react';
import { Switch } from './';

export default {
  title: 'Components/Switch',
  component: Switch,
};

const Template = (args: any) => (
  <>
    <Switch {...args} />
  </>
);

export const Default = Template.bind({});

Default.args = {};
