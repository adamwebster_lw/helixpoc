import styled, { css } from 'styled-components';

export const StyledSwitch = styled.div`
  &:focus-within {
    .switch-input {
      outline: 3px solid ${({ theme }) => theme.switch?.focusColor};
    }
  }
  .switch-input {
    width: 44px;
    height: 24px;
    position: relative;
    display: inline-flex;
    margin-right: 11px;
    background-color: ${({ theme }) => theme.switch?.backgroundColor};
    border-radius: 24px;
    padding: 4px;
    box-sizing: border-box;
    box-shadow: solid 1.5px ${({ theme }) => theme.switch?.boxShadow};
  }
  .switch-indicator {
    background-color: ${({ theme }) => theme.switch?.toggleBackgroundColor};
    width: 16px;
    border-radius: 50%;
    height: 16px;
    transform: translateX(0);
    transition: all 300ms;
  }
  label {
    display: inline-flex;
    align-items: center;
  }
  input[type='checkbox'] {
    width: 0;
    height: 0;
    -webkit-appearance: none;
  }
  input[type='checkbox']:checked + .switch-input {
    background-color: ${({ theme }) => theme.switch?.checkedBackgroundColor};
    .switch-indicator {
      transform: translate(20px);
    }
  }

  input[type='checkbox']:disabled + .switch-input,
  input[type='checkbox']:disabled + .switch-input {
    filter: grayscale(1) opacity(0.5);
  }
`;

interface StyledSwitchLabelProps {
  disabled?: boolean;
}
export const StyledSwitchLabel = styled.label<StyledSwitchLabelProps>`
  ${({ disabled }) =>
    disabled &&
    css`
      filter: grayscale(1) opacity(0.5);
    `}
`;
