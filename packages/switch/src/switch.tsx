import React, { InputHTMLAttributes } from "react";
import { StyledSwitch, StyledSwitchLabel } from "./switch.styles";

interface SwitchProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  /** If the input is checked or not */
  checked?: boolean;
  /** if the checkbox is disabled or not */
  disabled?: boolean;
  /** if the label should be positioned on the right or the left side.  The default is left. */
  labelPosition?: "right" | "left";
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const Switch = React.forwardRef<HTMLInputElement, SwitchProps>(
  (
    {
      label,
      labelPosition = "right",
      checked,
      disabled,
      dataLwtId,
      className,
      ...rest
    },
    ref
  ) => {
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }
    return (
      <StyledSwitch className={`helix-switch ${className ? className : ""}`}>
        <label>
          {labelPosition === "left" && (
            <StyledSwitchLabel disabled={disabled} className="switch-label">
              {label}
            </StyledSwitchLabel>
          )}
          <input
            checked={checked}
            disabled={disabled}
            ref={ref}
            type="checkbox"
            {...optionalProps}
            {...rest}
          />
          <div className="switch-input">
            <div className="switch-indicator" />
          </div>
          {labelPosition === "right" && (
            <StyledSwitchLabel disabled={disabled} className="switch-label">
              {label}
            </StyledSwitchLabel>
          )}
        </label>
      </StyledSwitch>
    );
  }
);

Switch.displayName = "Switch";
export default Switch;
