import React, { HtmlHTMLAttributes } from "react";
import {
  StyledProgressBar,
  StyledProgressBarFill,
  StyledProgressInfo,
  StyledProgressLabel,
  StyleProgressValue,
} from "./progress-bar.styles";

export interface ProgressBarProps extends HtmlHTMLAttributes<HTMLDivElement> {
  value: string;
  label?: string;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const ProgressBar = ({
  value,
  label,
  dataLwtId,
  className,
  ...rest
}: ProgressBarProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <>
      <div
        className={`helix-progress-bar ${className ? className : ""}`}
        {...optionalProps}
        {...rest}
      >
        <StyledProgressInfo>
          {label && <StyledProgressLabel>{label}</StyledProgressLabel>}
          {value && <StyleProgressValue>{value}</StyleProgressValue>}
        </StyledProgressInfo>
        <StyledProgressBar>
          <StyledProgressBarFill value={value} />
        </StyledProgressBar>
      </div>
    </>
  );
};

ProgressBar.displayName = "ProgressBar";
export default ProgressBar;
