import styled from "styled-components";

export const StyledProgressBar = styled.div`
  background-color: ${({ theme }) => theme.progressBar?.backgroundColor};
  border-radius: 4px;
  height: 6px;
  overflow: hidden;
  width: 100%;
`;

interface StyledProgressBarFillProps {
  value: string;
}

export const StyledProgressBarFill = styled.div<StyledProgressBarFillProps>`
  background-color: ${({ theme }) => theme.progressBar?.barBackgroundColor};
  height: 100%;
  border-radius: 6px;
  width: ${({ value }) => value}%;
`;

export const StyledProgressInfo = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2px;
  font-size: 12px;
  font-weight: 400;
`;

export const StyledProgressLabel = styled.div`
  margin-top: 8px;
  display: flex;
  flex: 1 1;
`;

export const StyleProgressValue = styled.div`
  margin-top: 8px;
  display: flex;
  justify-content: flex-end;
  flex: 1 1;
`;
