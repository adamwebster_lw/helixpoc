import React from "react";
import { ProgressBar } from ".";
export default {
  title: "Components/Progress Bar",
  component: ProgressBar,
};

const Template = (args: any) => {
  return <ProgressBar {...args} />;
};

export const Default: any = Template.bind({});

Default.args = {
  value: "50",
  label: "Progress",
};
