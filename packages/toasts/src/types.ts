import { ReactNode } from 'react';

export type ToastTypes = 'secondary' | 'success' | 'warning' | 'danger' | 'info';
export interface ToastProps {
  id: string;
  type: ToastTypes;
  dataLwtId?: string;
  text?: ReactNode;
  labelText?: string;
  dismissible?: boolean;
  autoDismiss?: boolean;
  dismissTime?: number;
  hideIcon?: boolean;
}

export type ToastPositions = 'bottom-right' | 'bottom-left' | 'top-right' | 'top-left';
