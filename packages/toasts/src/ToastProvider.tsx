import React, { createContext, ReactNode, useReducer } from 'react';
import Toasts from './toasts';
import { ToastPositions } from './types';

const initialState = {
  toasts: [],
  position: 'bottom-right',
};

export interface dispatchValuesProps {
  type: 'ADD_TOAST' | 'SET_TOASTS' | 'SET_POSITION';
  payload: any;
}

export const ToastContext = createContext({
  toastState: initialState,
  dispatchToast: (value: dispatchValuesProps | void) => value,
});

export const ToastConsumer = ToastContext.Consumer;

const reducer = (state: any, action: any) => {
  const { type, payload }: dispatchValuesProps = action;
  switch (type) {
    case 'ADD_TOAST': {
      const newItems: any = [...state.toasts, payload];
      return {
        ...state,
        toasts: newItems,
      };
    }
    case 'SET_TOASTS': {
      return {
        ...state,
        toasts: payload,
      };
    }
    case 'SET_POSITION': {
      return {
        ...state,
        position: payload,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

interface ToastContextProviderProps {
  children: ReactNode;
  position?: ToastPositions;
}

export const ToastProvider = ({ children, position }: ToastContextProviderProps) => {
  const [toastState, dispatchToast] = useReducer(reducer, initialState);

  return (
    <ToastContext.Provider value={{ toastState, dispatchToast }}>
      {children}
      <Toasts position={position} />
    </ToastContext.Provider>
  );
};
