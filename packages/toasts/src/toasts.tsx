import React, { useContext, useEffect } from 'react';
import { ToastContext } from './ToastProvider';
import Toast from './toast';
import { ToastPositions, ToastProps } from './types';
import { StyledToastContainer } from './toast.styles';

interface ToastsProps {
  position?: ToastPositions;
}
const Toasts = ({ position = 'bottom-right' }: ToastsProps) => {
  const { toastState, dispatchToast } = useContext(ToastContext);
  useEffect(() => {
    dispatchToast({ type: 'SET_POSITION', payload: position });
  }, [position]);
  return (
    <StyledToastContainer position={position}>
      {toastState.toasts.map((toast: ToastProps) => (
        <Toast
          key={toast.id}
          id={toast.id}
          type={toast.type}
          dataLwtId={toast.dataLwtId}
          labelText={toast.labelText}
          text={toast.text}
          autoDismiss={toast.autoDismiss}
          hideIcon={toast.hideIcon}
        />
      ))}
    </StyledToastContainer>
  );
};

Toasts.displayName = 'Toasts';
export default Toasts;
