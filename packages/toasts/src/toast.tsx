import React, { useContext, useEffect, useState } from "react";
import { StyledToast } from "./toast.styles";
import { ToastPositions, ToastProps, ToastTypes } from "./types";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { x } from "@lwt-helix/helix-icon/outlined";
import {
  warning,
  diamond_exclamation,
  circle_info,
  circle_checkmark,
} from "@lwt-helix/helix-icon/solid";
import { ToastContext } from "./ToastProvider";

const Toast = ({
  type = "secondary",
  dataLwtId,
  labelText,
  text = "Alert",
  autoDismiss = true,
  dismissTime = 5000,
  hideIcon = false,
  ...rest
}: ToastProps) => {
  const [visible, setVisible] = useState(true);
  const [closing, setClosing] = useState(false);
  const { toastState } = useContext(ToastContext);
  useEffect(() => {
    if (autoDismiss) {
      setTimeout(() => {
        setClosing(true);
      }, dismissTime);
    }
  }, []);

  useEffect(() => {
    setTimeout(() => {
      if (closing) {
        setVisible(false);
      }
    }, 400);
  }, [closing]);

  const getIcon = (type: ToastTypes) => {
    switch (type) {
      case "danger":
        return diamond_exclamation;
      case "warning":
        return warning;
      case "info":
        return circle_info;
      case "success":
        return circle_checkmark;
      default:
        return warning;
    }
  };
  return visible ? (
    <StyledToast
      position={toastState.position as ToastPositions}
      closing={closing}
      type={type}
      data-lwt-id={dataLwtId}
      {...rest}
    >
      {!hideIcon && (
        <span className="helix-toast__icon">
          <HelixIcon icon={getIcon(type)} />
        </span>
      )}
      {labelText && (
        <span className="helix-toast__label-text">{labelText}: &nbsp;</span>
      )}
      {text && <span className="helix-toast__text">{text}</span>}
      <button
        className="helix-toast__dismiss-button"
        aria-label="Dismiss"
        onClick={() => setClosing(true)}
      >
        <HelixIcon icon={x} />
      </button>
    </StyledToast>
  ) : (
    <></>
  );
};

Toast.displayName = "Toast";

export default Toast;
