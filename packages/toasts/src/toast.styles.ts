import styled, { css } from "styled-components";
import { ToastPositions } from "./types";

interface StyledToastProps {
  type: "secondary" | "success" | "warning" | "danger" | "info";
  closing: boolean;
  position: ToastPositions;
}

const animateByPosition = (position: ToastPositions) => {
  switch (position) {
    case "bottom-left":
    case "top-left":
      return {
        from: css`
          opacity: 0;
          left: -300px;
        `,
        to: css`
          opacity: 1;
          left: 00px;
        `,
      };
    case "top-right":
    case "bottom-right":
      return {
        from: css`
          opacity: 0;
          right: -300px;
        `,
        to: css`
          opacity: 1;
          right: 00px;
        `,
      };
  }
};

export const StyledToast = styled.div<StyledToastProps>`
  width: 300px;
  display: flex;
  position: relative;
  ${({ type, theme }) => css`
    background-color: ${theme?.toast![type]?.backgroundColor};
    color: ${theme?.toast![type]?.textColor}!important;
  `}
  border-radius: 6px;
  box-shadow: ${({ theme }) => theme.toast?.boxShadow};
  padding: 16px;
  animation-name: ${({ closing }) =>
    closing ? "fadeOutToast" : "fadeInToast"};
  animation-duration: 0.45s;
  .helix-toast__icon {
    padding: 0 12px 0 0;
  }
  .helix-toast__label-text {
    font-weight: 600;
  }
  .helix-toast__text {
    flex: 1 1;
  }
  .helix-toast__dismiss-button {
    background-color: transparent;
    border: none;
    cursor: pointer;
    display: inline-flex;
    &:hover {
      opacity: 0.5;
    }
  }
  .material-icons {
    margin-right: 18px;
  }
  &:not(:last-child) {
    margin-bottom: 12px;
  }

  svg path {
    fill: ${({ theme, type }) => theme.toast![type]?.iconColor}!important;
  }

  @keyframes fadeInToast {
    from {
      ${({ position }) => animateByPosition(position).from}
    }
    to {
      ${({ position }) => animateByPosition(position).to}
    }
  }

  @keyframes fadeOutToast {
    from {
      ${({ position }) => animateByPosition(position).to}
    }
    to {
      ${({ position }) => animateByPosition(position).from}
    }
  }
`;

const getContainerPosition = (position: ToastPositions) => {
  switch (position) {
    case "bottom-left":
      return css`
        bottom: 12px;
        left: 12px;
      `;
    case "bottom-right":
      return css`
        bottom: 12px;
        right: 12px;
      `;
    case "top-left":
      return css`
        top: 12px;
        left: 12px;
      `;
    case "top-right":
      return css`
        top: 12px;
        right: 12px;
      `;
  }
};

interface StyledToastContainerProps {
  position: ToastPositions;
}

export const StyledToastContainer = styled.div<StyledToastContainerProps>`
  position: fixed;
  z-index: 1052;
  ${({ position }) => getContainerPosition(position)}
`;
