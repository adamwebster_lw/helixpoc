import { useContext } from "react";
import { ToastContext } from "./ToastProvider";
import { ToastProps } from "./types";

const useToast = () => {
  const { dispatchToast } = useContext(ToastContext);
  return {
    addToast: ({
      dataLwtId,
      type,
      labelText,
      text,
      autoDismiss,
      hideIcon = false,
    }: ToastProps) => {
      dispatchToast({
        type: "ADD_TOAST",
        payload: { dataLwtId, type, labelText, text, autoDismiss, hideIcon },
      });
    },
  };
};

export default useToast;
