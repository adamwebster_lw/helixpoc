import { Button } from '@lwt-helix-nextgen/button';
import React from 'react';

import { Toast, ToastProvider, useToast } from '.';

export default {
  title: 'Components/Toast',
  component: ToastProvider,
};

const ToastComponentTest = () => {
  const { addToast } = useToast();

  return (
    <>
      <Button
        dataLwtId="AddToast"
        onClick={() => addToast({ id: '1', dataLwtId: 'toast', type: 'secondary', text: 'Text', labelText: 'label' })}
      >
        Add Toast
      </Button>
      <Button
        dataLwtId="AddToast"
        onClick={() =>
          addToast({
            id: '2',
            dataLwtId: 'toast',
            type: 'danger',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer maximus imperdiet magna quis accumsan. Donec maximus ullamcorper nisl, sit amet vulputate neque consectetur vitae. Morbi felis enim, viverra vitae eros ultricies, efficitur auctor neque.',
            labelText: 'label',
            autoDismiss: false,
          })
        }
      >
        Add Danger Toast (autoDismiss false)
      </Button>
      <Button
        dataLwtId="AddToast"
        onClick={() =>
          addToast({ id: '2', dataLwtId: 'toast', type: 'info', text: 'Text', labelText: 'label', hideIcon: true })
        }
      >
        Add Info Toast
      </Button>
      <Button
        dataLwtId="AddToast"
        onClick={() => addToast({ id: '2', dataLwtId: 'toast', type: 'success', text: 'Text', labelText: 'label' })}
      >
        Add Success Toast
      </Button>
      <Button
        dataLwtId="AddToast"
        onClick={() => addToast({ id: '2', dataLwtId: 'toast', type: 'warning', text: 'Text', labelText: 'label' })}
      >
        Add Waring Toast
      </Button>
    </>
  );
};

export const Provider = ({ ...args }) => {
  return (
    <ToastProvider {...args}>
      <ToastComponentTest />
    </ToastProvider>
  );
};

Provider.args = {
  position: 'bottom-right',
};
