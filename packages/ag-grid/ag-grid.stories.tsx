import React, { useCallback } from "react";
import { AGGridHelix, AgGridColumnProps } from ".";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { add } from "@lwt-helix/helix-icon/solid";
import { uniqueNamesGenerator, names } from "unique-names-generator";

export default {
  title: "Components/AGGrid",
  component: AGGridHelix,
};

const randomNameFunc = () => {
  const randomName = uniqueNamesGenerator({
    dictionaries: [names, names],
    length: 2,
    separator: " ",
  });
  return randomName;
};

const randomAgeFunc = () => {
  return Math.floor(Math.random() * 100);
};

const columns: AgGridColumnProps[] = [
  { field: "name", flex: 1 },
  { field: "age", flex: 1 },
];

const generateData = (numRows: number) => {
  const data: any = [];
  for (let i = 0; i < numRows; i++) {
    data.push({
      name: randomNameFunc(),
      age: randomAgeFunc(),
    });
  }
  return data;
};

const rowData = generateData(100);

export const AGGrid = () => {
  const getMainMenuItems = useCallback((params) => {
    switch (params.column.colId) {
      case "name": {
        const nameItems = params.defaultItems.slice(0);
        nameItems.push({
          name: "Name Menu Item",
          action: () => {
            alert("Name Menu Item Clicked");
          },
          icon: <HelixIcon icon={add} />,
        });
        return nameItems;
      }
      case "age": {
        const ageItems = params.defaultItems.slice(0);
        ageItems.push({
          name: "Age Menu Item",
          action: () => {
            alert("Age Menu Item Clicked");
          },
        });
        return ageItems;
      }
      default:
        return params.defaultItems;
    }
  }, []);
  return (
    <>
      <HelixIcon icon={add} />
      <AGGridHelix
        columnDefs={columns}
        rowData={rowData}
        getMainMenuItems={getMainMenuItems}
      />
    </>
  );
};

export const WithDateFilter = (props: any) => {
  const generateData = (numRows: number) => {
    const data: any = [];
    for (let i = 0; i < numRows; i++) {
      data.push({
        name: randomNameFunc(),
        age: randomAgeFunc(),
        date: new Date(),
      });
    }
    return data;
  };

  const columnDefs: AgGridColumnProps[] = [
    {
      field: "name",
      flex: 1,
      filter: "agTextColumnFilter",
      floatingFilter: true,
    },
    {
      field: "age",
      flex: 1,
      filter: "agNumberColumnFilter",
      floatingFilter: true,
    },

    {
      field: "date",
      flex: 1,
      filter: "agDateColumnFilter",
      floatingFilter: true,
      floatingFilterComponent: "datePickerFilter",
      floatingFilterComponentParams: { suppressFilterButton: true },
    },
  ];

  const rowData = generateData(100);

  return (
    <AGGridHelix
      columnDefs={columnDefs}
      rowData={rowData}
      pagination
      paginationPageSize={10}
    />
  );
};
