# AG Grid

The AG grid is a advanced table component that allows features such as freezing column and rows, resizing columns, advanced filters and more.

## Installation

```sh
npm install @lwt-helix/ag-grid
```

## Projects using this module

```
Design Systems Catalog - exemplifies how to use the module
```

## Props

```ts
interface AgGridHelixProps {
    sizePerPageList?: number[];
    width?: string;
    height?: string;
}
```
To see more features of AGGrid visit https://www.ag-grid.com/react-data-grid/
