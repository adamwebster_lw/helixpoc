import React, { createContext } from 'react';
import { LicenseManager } from 'ag-grid-enterprise';

export const AGGridContext = createContext({});

export const AGGridConsumer = AGGridContext.Consumer;

interface AgGridProviderProps {
    children: React.ReactNode;
    license: string;
}

export const AGGridProvider = ({ license, children }: AgGridProviderProps) => {
    LicenseManager.setLicenseKey(license);
    return <AGGridContext.Provider value={{}}>{children}</AGGridContext.Provider>;
};
