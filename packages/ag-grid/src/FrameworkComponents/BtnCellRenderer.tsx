import React from 'react';
import { Button, ButtonProps } from '@lwt-helix/buttons';

interface BtnCellRendererProps extends ButtonProps<HTMLButtonElement> {
    clicked: ({ value, data }: any) => void;
    value: string;
    data: any;
}
const BtnCellRenderer = ({ clicked, value, data, color, ...rest }: BtnCellRendererProps) => {
    return (
        <Button dataLwtId='button-cell-renderer-button-id' color={color} onClick={() => clicked({ value, data })}>
            {value}
        </Button>
    );
};

export default BtnCellRenderer;
