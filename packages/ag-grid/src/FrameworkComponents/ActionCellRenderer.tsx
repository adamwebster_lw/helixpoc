import React from 'react';
import { Button } from '@lwt-helix/buttons';

const ActionCellRenderer = (props: any) => {
    const { actions } = props;
    return (
        <div>
            {actions.map((action: any) => {
                return (
                    <Button
                        dataLwtId='action-cell-renderer-button-id'
                        color={action.color}
                        className='btn'
                        key={action.name}
                        onClick={() => action.click()}
                    >
                        {action.name}
                    </Button>
                );
            })}
        </div>
    );
};

export default ActionCellRenderer;
