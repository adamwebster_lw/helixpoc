import { DatePicker } from "@lwt-helix-nextgen/date-picker";
import React, {
  forwardRef,
  useImperativeHandle,
  useState,
  useRef,
} from "react";
import { AnyStyledComponent } from "styled-components";

const DatePickerFilter = forwardRef<any, AnyStyledComponent>((props, ref) => {
  const [currentValue, setCurrentValue] = useState<number | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);

  // expose AG Grid Filter Lifecycle callbacks
  useImperativeHandle(ref, () => {
    return {
      onParentModelChanged(parentModel: any) {
        // When the filter is empty we will receive a null value here
        if (!inputRef.current) return;
        if (!parentModel) {
          setCurrentValue(null);
        } else {
          setCurrentValue(parentModel.filter);
        }
      },
    };
  });

  const normalizeDate = (date: Date) => {
    const newD = date;
    newD.setTime(newD.getTime() + newD.getTimezoneOffset() * 60 * 1000);
    return newD;
  };

  const onDatePickerChange = (date: any) => {
    if (!date) {
      // clear the filter
      props.parentFilterInstance((instance: any) => {
        instance.onFloatingFilterChanged(null, null);
      });
      return;
    }

    const normalizedDate = normalizeDate(new Date(date));
    const normalizedDateToString = normalizedDate.setHours(0, 0, 0, 0);

    props.parentFilterInstance((instance: any) => {
      instance.onFloatingFilterChanged(
        "equals",
        new Date(normalizedDateToString)
      );
    });
  };

  return (
    <div style={{ margin: "0 2px" }}>
      <DatePicker
        usePortal
        value={currentValue?.toString()}
        onChange={(date: any) => onDatePickerChange(date)}
      />
    </div>
  );
});

DatePickerFilter.displayName = "DatePickerFilter";
export default DatePickerFilter;
