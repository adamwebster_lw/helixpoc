import React, {
  useState,
  useImperativeHandle,
  useRef,
  useEffect,
  useCallback,
} from "react";
import { DatePicker } from "@lwt-helix-nextgen/date-picker";
import { ICellEditorParams } from "ag-grid-community";

interface DatePickerEditorProps extends ICellEditorParams {
  backEndFormat?: string;
  displayFormat?: string;
  placeholderText?: string;
}

const DatePickerEditor = React.forwardRef(
  (props: DatePickerEditorProps, ref) => {
    const [value, setValue] = useState<any>(props.value);
    const wrapperRef = useRef<HTMLDivElement>(null);
    /* Component Editor Lifecycle methods */
    useImperativeHandle(ref, () => {
      return {
        // the final value to send to the grid, on completion of editing
        getValue() {
          // this simple editor doubles any value entered into the input
          return value;
        },

        // Gets called once before editing starts, to give editor a chance to
        // cancel the editing before it even starts.
        isCancelBeforeStart() {
          return false;
        },

        // Gets called once when editing is finished (eg if Enter is pressed).
        // If you return true, then the result of the edit will be ignored.
        isCancelAfterEnd() {
          return false;
        },
      };
    });

    const handleKeydown = useCallback((e: KeyboardEvent) => {
      if (e.key === "Enter") {
        // added to fix keyboard control issue when selecting a date with enter
        e.preventDefault();
      }
    }, []);

    useEffect(() => {
      if (wrapperRef.current) {
        const wrapperElement = wrapperRef.current;
        wrapperElement.addEventListener("keydown", handleKeydown);
        return () => {
          wrapperElement.removeEventListener("keydown", handleKeydown);
        };
      }
    }, [wrapperRef, value, handleKeydown]);

    return (
      <div ref={wrapperRef}>
        <DatePicker
          value={value}
          displayFormat={props.displayFormat}
          backEndFormat={props.backEndFormat}
          placeholderText={props.placeholderText}
          onChange={(date: string) => {
            setValue(date);
            if (date) {
              setTimeout(() => {
                props.stopEditing();
              }, 0);
            }
          }}
        />
      </div>
    );
  }
);

DatePickerEditor.displayName = "DatePickerEditor";

export default DatePickerEditor;
