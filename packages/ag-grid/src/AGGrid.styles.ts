/* eslint-disable max-len */
import styled from "styled-components";
import { AgGridReact } from "ag-grid-react";

export const StyledAgGrid = styled(AgGridReact)`
  --ag-border-color: #e6e8f0;
  --ag-alpine-active-color: #2563eb;
  --ag-selected-row-background-color: #e9effc;
  --ag-row-hover-color: #f9fafb;
  .ag-root-wrapper {
    border-radius: 4px;
    border: none;
    box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.2), 0px 1px 2px rgba(0, 0, 0, 0.1);
  }
  .ag-header-group-cell-label {
    justify-content: center;
    font-weight: normal;
  }
  .ag-header-row-column-group {
    border: none !important;
    outline-color: purple !important;
  }

  .ag-header {
    background-color: white;
  }
  .ag-menu {
    background-color: white;
    border-radius: 6px;
    box-shadow: none;
  }
  .ag-popup-child {
    box-shadow: none !important;
    border: none !important;
    filter: drop-shadow(0px 0px 1px rgba(0, 0, 0, 0.2))
      drop-shadow(0px 4px 8px rgba(0, 0, 0, 0.1));
  }
  .ag-tabs-header {
    background-color: white;
    border: none;
  }
  .ag-tab {
    &.ag-tab-selected {
      color: #2563eb;
      border-color: #2563eb;
    }
  }

  .ag-header-cell-text {
    font-style: normal;
    font-weight: 600;
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.04em;
    text-transform: uppercase;
    color: #6b7280;
  }

  .ag-checkbox-input-wrapper {
    width: 14px;
    height: 14px;
  }
  .ag-checkbox-input-wrapper.ag-checked::after {
    content: "";
    width: 14px;
    height: 14px;
    background-image: url("data:image/svg+xml,%3Csvg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Crect width='14' height='14' rx='4' fill='%232563EB'/%3E%3Crect width='14' height='14' rx='4' stroke='%232563EB'/%3E%3Cpath d='M4 7L6 9L10 5' stroke='white' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round'/%3E%3C/svg%3E%0A");
  }
  .ag-checkbox-input-wrapper::after {
    content: "";
    background-image: url("data:image/svg+xml,%3Csvg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Crect x='0.75' y='0.75' width='12.5' height='12.5' rx='3.25' fill='white' stroke='%239CA3AF' stroke-width='1.5'/%3E%3C/svg%3E%0A");
    width: 20px;
    height: 14px;
    width: 14px;
  }

  .ag-input-field-input,
  .ag-picker-field-wrapper,
  .react-datepicker__input-container input {
    display: block;
    width: 100%;
    padding: 8px 12px !important;
    font-size: 14px;
    line-height: 1;
    color: #5c656d;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da !important;
    border-radius: 6px !important;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    .ag-picker-field-display {
      margin: 0;
    }
    &:focus {
      color: #5c656d;
      background-color: #fff;
      border-color: #d1d5db;
      outline: 0 !important;
      box-shadow: 0 0 0 2px #2563eb !important;
    }
  }

  .ag-picker-field-wrapper {
    display: flex;
  }

  .ag-cell-inline-editing {
    border: solid 2px #2563eb !important;
    .react-datepicker-wrapper,
    .react-datepicker__input-container {
      width: 100%;
    }
    input,
    .ag-picker-field-wrapper {
      border: none !important;
      border-radius: 0 !important;
      box-shadow: none;
      width: 100%;
      &:focus {
        box-shadow: none !important;
      }
    }
  }

  .ag-floating-filter-full-body {
    .react-datepicker-wrapper,
    .react-datepicker__input-container {
      width: 100%;
      display: flex;
    }
  }
`;

export const StyledPaginationSection = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 16px;
  flex-wrap: wrap;
  &:last-child{
    text-align: right;
  }
`;
