/* eslint-disable max-len */
import React, {
  useCallback,
  useRef,
  useState,
  useMemo,
  useImperativeHandle,
  Ref,
} from "react";
import { AgGridReactProps, AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import "ag-grid-enterprise";
import { StyledAgGrid, StyledPaginationSection } from "./AGGrid.styles";
import BtnCellRenderer from "./FrameworkComponents/BtnCellRenderer";
import ActionCellRenderer from "./FrameworkComponents/ActionCellRenderer";
import DatePickerFilter from "./Filters/DatePickerFilter";
import { Pagination, PaginationItem } from "@lwt-helix-nextgen/pagination";
import {
  DropdownButton,
  DropdownButtonItem,
  DropdownButtonMenu,
} from "@lwt-helix-nextgen/dropdown-button";
import DatePickerEditor from "./EditorCells/DatePickerEditor";
import { HelixIcon } from "@lwt-helix/helix-icon";
import {
  chevron_small_right,
  chevron_small_left,
  chevron_left,
  chevron_right,
} from "@lwt-helix/helix-icon/outlined";
export interface AgGridHelixProps extends AgGridReactProps {
  sizePerPageList?: number[];
  width?: string;
  height?: string;
}

export const AGGridHelix = React.forwardRef<AgGridReact, AgGridHelixProps>(
  (
    {
      paginationPageSize = 10,
      sizePerPageList = [10, 25, 30, 50],
      pagination = false,
      rowSelection = "multiple",
      suppressMovableColumns = true,
      suppressCsvExport = true,
      suppressExcelExport = true,
      suppressCellFocus = true,
      width = "100%",
      height = "400px",
      domLayout,
      ...rest
    },
    ref
  ) => {
    const gridRef = useRef<AgGridReact>();
    const [currentPage, setCurrentPage] = useState(1);
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [, setTotalPage] = useState(0);

    useImperativeHandle(ref, () => gridRef.current as AgGridReact);

    const icons = useMemo(() => {
      return {
        filter: `<svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M0.25 1C0.25 0.585786 0.585786 0.25 1 0.25H17C17.4142 0.25 17.75 0.585786 17.75 1C17.75 1.41421 17.4142 1.75 17 1.75H1C0.585786 1.75 0.25 1.41421 0.25 1ZM3.25 5C3.25 4.58579 3.58579 4.25 4 4.25H14C14.4142 4.25 14.75 4.58579 14.75 5C14.75 5.41421 14.4142 5.75 14 5.75H4C3.58579 5.75 3.25 5.41421 3.25 5ZM6.25 9C6.25 8.58579 6.58579 8.25 7 8.25H11C11.4142 8.25 11.75 8.58579 11.75 9C11.75 9.41421 11.4142 9.75 11 9.75H7C6.58579 9.75 6.25 9.41421 6.25 9Z" fill="#4B5563"/>
      </svg>      
      `,
        sortAscending: `<svg width="10" height="8" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M5.82979 0.883691C5.43074 0.418916 4.7114 0.418916 4.31235 0.88369L0.417873 5.41965C-0.138922 6.06816 0.321846 7.07107 1.17659 7.07107L8.96554 7.07107C9.82029 7.07107 10.2811 6.06816 9.72426 5.41965L5.82979 0.883691Z" fill="#4B5563"/>
      </svg>      
      `,
        sortDescending: `<svg width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M4.31236 6.18735C4.71141 6.65213 5.43076 6.65213 5.8298 6.18736L9.72428 1.6514C10.2811 1.00289 9.8203 -2.18189e-05 8.96556 -2.22211e-05L1.17661 -2.2017e-05C0.321864 -2.24191e-05 -0.138909 1.00289 0.417887 1.6514L4.31236 6.18735Z" fill="#4B5563"/>
      </svg>            
      `,
        clipboardCopy: `<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.5556 6.5H8.44444C7.87491 6.5 7.5 6.93379 7.5 7.36364V15.6364C7.5 16.0662 7.87491 16.5 8.44444 16.5H14.5556C15.1251 16.5 15.5 16.0662 15.5 15.6364V7.36364C15.5 6.93379 15.1251 6.5 14.5556 6.5ZM8.44444 5C7.09442 5 6 6.05824 6 7.36364V15.6364C6 16.9418 7.09441 18 8.44444 18H14.5556C15.9056 18 17 16.9418 17 15.6364V7.36364C17 6.05824 15.9056 5 14.5556 5H8.44444Z" fill="#4B5563"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M3.21967 2.21967C3.36032 2.07902 3.55109 2 3.75001 2L10.7503 2.00009C11.1645 2.00009 11.5003 2.33588 11.5003 2.75009C11.5003 3.16431 11.1645 3.50009 10.7503 3.50009L4.50002 3.50001L4.5002 11.25C4.50021 11.6642 4.16443 12 3.75022 12C3.336 12 3.00021 11.6642 3.0002 11.25L3 2.75002C3 2.5511 3.07901 2.36033 3.21967 2.21967Z" fill="#4B5563"/>
      </svg>
      `,
      };
    }, []);

    const handleNext = (e: React.MouseEvent) => {
      e.preventDefault();
      gridRef.current?.api.paginationGoToNextPage();
    };

    const handlePrevious = (e: React.MouseEvent) => {
      e.preventDefault();
      gridRef.current?.api.paginationGoToPreviousPage();
    };

    const handleFirst = (e: React.MouseEvent) => {
      e.preventDefault();
      gridRef.current?.api.paginationGoToFirstPage();
    };

    const handleLast = (e: React.MouseEvent) => {
      e.preventDefault();
      gridRef.current?.api.paginationGoToLastPage();
    };

    const onPaginationChanged = useCallback(() => {
      setCurrentPage(gridRef.current?.api.paginationGetCurrentPage() as number);
      setTotalPage(gridRef.current?.api.paginationGetTotalPages() as number);
    }, []);

    const handlePageClick = (e: React.MouseEvent, page: number) => {
      e.preventDefault();
      gridRef.current?.api.paginationGoToPage(page);
    };

    // generate pages to only show 5 pages at a time
    const pages = () => {
      if (!gridRef.current?.api) return [];
      const pageCount = gridRef.current.api.paginationGetTotalPages();
      // if page count is 5 or less, show all pages
      if (pageCount <= 5) {
        return [...Array(pageCount).keys()];
      }
      const currentPage = gridRef.current.api.paginationGetCurrentPage() - 1;
      const startPage = currentPage >= 2 ? currentPage - 1 : currentPage - 2;
      const endPage = currentPage >= 2 ? currentPage + 3 : 4;
      const pages: number[] = [];
      for (let i = startPage; i <= endPage; i++) {
        if (i > -1 && i <= pageCount - 1) {
          pages.push(i);
        }
      }
      // if less then 5 pages add previous page numbers until pages equal 5
      if (pages.length < 5 && pageCount) {
        const startPage = currentPage - 2;
        const endPage = currentPage - 2 + 5 - pages.length;
        for (let i = startPage; i <= endPage; i++) {
          if (i > 0 && i <= pageCount) {
            // only unshift if not already in array
            if (!pages.includes(i)) {
              pages.unshift(i);
            }
            // if on the last page then unshift an extra page
            if (i === endPage && pages.length < 5) {
              pages.unshift(currentPage - pages.length + 1);
            }
          }
        }
      }
      return pages;
    };

    return (
      <>
        <div
          className="ag-theme-alpine"
          style={{ height: domLayout === "autoHeight" ? "" : height, width }}
        >
          <StyledAgGrid
            ref={gridRef as Ref<AgGridReact>}
            domLayout={domLayout}
            rowSelection={rowSelection}
            suppressRowClickSelection
            columnHoverHighlight={false}
            icons={icons}
            paginationPageSize={paginationPageSize}
            pagination={pagination}
            suppressMovableColumns={suppressMovableColumns}
            onPaginationChanged={onPaginationChanged}
            suppressCellFocus={suppressCellFocus}
            suppressExcelExport={suppressExcelExport}
            suppressCsvExport={suppressCsvExport}
            components={{
              btnCellRenderer: BtnCellRenderer,
              actionCellRenderer: ActionCellRenderer,
              datePickerFilter: DatePickerFilter,
              datePickerEditor: DatePickerEditor,
            }}
            noRowsOverlayComponentParams={{
              noRowsMessageFunc: () => "No data to display",
            }}
            suppressPaginationPanel
            {...rest}
          />
        </div>
        {pagination && (
          <StyledPaginationSection>
            <div>
              <DropdownButton
                removeSplit
                label={
                  gridRef.current &&
                  gridRef.current.api &&
                  gridRef.current.api.paginationGetPageSize()
                }
              >
                <DropdownButtonMenu>
                  {sizePerPageList.map((sizePerPage) => (
                    <DropdownButtonItem
                      key={sizePerPage}
                      id={sizePerPage.toString()}
                      textLabel={sizePerPage.toString()}
                      onClick={() => {
                        gridRef.current?.api.paginationSetPageSize(sizePerPage);
                      }}
                    >
                      {sizePerPage}
                    </DropdownButtonItem>
                  ))}
                </DropdownButtonMenu>
              </DropdownButton>
            </div>
            <div className="col-md-6 col-xs-6 col-sm-6 col-lg-6">
              <Pagination className="float-right">
                <PaginationItem
                  disabled={
                    gridRef.current &&
                    gridRef.current.api &&
                    gridRef.current.api.paginationGetCurrentPage() === 0
                  }
                >
                  <a onClick={(e) => handleFirst(e)} href="#">
                    <HelixIcon icon={chevron_left} />
                  </a>
                </PaginationItem>
                <PaginationItem
                  iconItem
                  disabled={
                    gridRef.current &&
                    gridRef.current.api &&
                    gridRef.current.api.paginationGetCurrentPage() === 0
                  }
                >
                  <a onClick={(e) => handlePrevious(e)} href="#">
                    <HelixIcon icon={chevron_small_left} />
                  </a>
                </PaginationItem>
                {pages().map((page) => (
                  <PaginationItem key={page} active={page === currentPage}>
                    <a onClick={(e) => handlePageClick(e, page)} href="#">
                      {page + 1}
                    </a>
                  </PaginationItem>
                ))}
                <PaginationItem
                  disabled={
                    gridRef.current &&
                    gridRef.current.api &&
                    gridRef.current.api.paginationGetTotalPages() ===
                      gridRef.current.api.paginationGetCurrentPage() + 1
                  }
                  iconItem
                >
                  <a onClick={(e) => handleNext(e)} href="#">
                    <HelixIcon icon={chevron_small_right} />
                  </a>
                </PaginationItem>
                <PaginationItem
                  disabled={
                    gridRef.current &&
                    gridRef.current.api &&
                    gridRef.current.api.paginationGetTotalPages() ===
                      gridRef.current.api.paginationGetCurrentPage() + 1
                  }
                >
                  <a onClick={(e) => handleLast(e)} href="#">
                    <HelixIcon icon={chevron_right} />
                  </a>
                </PaginationItem>
              </Pagination>
            </div>
          </StyledPaginationSection>
        )}
      </>
    );
  }
);

AGGridHelix.displayName = "AGGridHelix";

export default AGGridHelix;
