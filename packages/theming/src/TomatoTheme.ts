import makeTheme from './makeTheme';

const TomatoTheme = makeTheme({ themeName: 'tomato', themeOptions: { primaryColor: 'tomato' } });
export default TomatoTheme;
