import makeTheme from "./makeTheme";
const RemaxTheme = makeTheme({
  themeName: "remax",
  themeOptions: {
    primaryColor: "#0054a4",
    primaryButtonBackgroundColor: "#0054a4",
    primaryButtonHoverBackgroundColor: "red",
    primaryButtonOutlineBorderColor: "#0054a4",
    primaryButtonOutlineTextColor: "#0054a4",
    primaryButtonOutlineHoverBorderColor: "red",
    primaryButtonOutlineHoverTextColor: "red",
  },
});
export default RemaxTheme;
