import { colors } from "./colors";
import { ThemeOptions } from "./theme";

export const DefaultThemeOptions: ThemeOptions = {
  primaryColor: colors.blue![600],
  primaryForegroundColor: colors.white,
  primaryButtonBackgroundColor: colors.blue![600],
  primaryButtonTextColor: colors.gray![100],
  primaryButtonHoverTextColor: colors.gray![100],
  primaryButtonHoverBackgroundColor: colors.blue![700],
  primaryButtonActiveBackgroundColor: colors.blue![800],
  primaryButtonOutlineBorderColor: colors.blue![600],
  primaryButtonOutlineTextColor: colors.blue![600],
  primaryButtonOutlineHoverTextColor: colors.blue![700],
  primaryButtonOutlineHoverBorderColor: colors.blue![700],
  primaryButtonOutlineActiveBorderColor: colors.blue![800],
};
