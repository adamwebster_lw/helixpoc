import { DefaultTheme } from "styled-components";
import makeTheme from "./makeTheme";

const MainTheme: DefaultTheme = makeTheme({
  themeName: "default",
  themeOptions: {},
});

export default MainTheme;
