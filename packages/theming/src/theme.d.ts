// import original module declaration
import "styled-components";

export interface ThemeOptions {
  primaryColor?: string;
  primaryForegroundColor?: string;
  primaryButtonBackgroundColor?: string;
  primaryButtonTextColor?: string;
  primaryButtonHoverTextColor?: string;
  primaryButtonHoverBackgroundColor?: string;
  primaryButtonActiveBackgroundColor?: string;
  primaryButtonOutlineBorderColor?: string;
  primaryButtonOutlineTextColor?: string;
  primaryButtonOutlineHoverTextColor?: string;
  primaryButtonOutlineHoverBorderColor?: string;
  primaryButtonOutlineActiveBorderColor?: string;
}

export interface ColorIntervals {
  50?: string;
  100?: string;
  200?: string;
  300?: string;
  400?: string;
  500?: string;
  600?: string;
  700?: string;
  800?: string;
  900?: string;
}

export interface HelixColors {
  white?: string;
  black?: string;
  "white-alpha"?: ColorIntervals;
  gray?: ColorIntervals;
  red?: ColorIntervals;
  orange?: ColorIntervals;
  yellow?: ColorIntervals;
  green?: ColorIntervals;
  teal?: ColorIntervals;
  cyan?: ColorIntervals;
  "light-blue"?: ColorIntervals;
  blue?: ColorIntervals;
  indigo?: ColorIntervals;
  purple?: ColorIntervals;
  pink?: ColorIntervals;
  rose?: ColorIntervals;
  primary?: string;
}

export interface AlertStyles {
  backgroundColor?: string;
  textColor?: string;
  borderColor?: string;
  iconColor?: string;
}

export interface AlertTypes {
  secondary?: AlertStyles;
  success?: AlertStyles;
  warning?: AlertStyles;
  danger?: AlertStyles;
  info?: AlertStyles;
}

export interface ToastStyles {
  backgroundColor?: string;
  textColor?: string;
  iconColor?: string;
}

interface BadgeStyleStyles {
  backgroundColor?: string;
  textColor?: string;
}

interface DashboardWidgetStyles {
  value?: {
    textColor?: string;
  };
}

// and extend it
declare module "styled-components" {
  export interface DefaultTheme {
    themeName: string;
    themeOptions?: ThemeOptions;
    colors?: HelixColors;
    backgroundColor?: string;
    fontFamily?: string;
    fontFamilyMonospace?: string;
    baseFontSize?: string;
    baseTextColor?: string;
    alert?: AlertTypes;
    toast?: {
      boxShadow?: string;
      secondary?: ToastStyles;
      success?: ToastStyles;
      warning?: ToastStyles;
      danger?: ToastStyles;
      info?: ToastStyles;
    };
    comboBox?: {
      backgroundColor?: string;
    };
    breakPoints?: {
      small?: string;
    };
    badge?: {
      default?: BadgeStyleStyles;
      inverted?: BadgeStyleStyles;
      danger?: BadgeStyleStyles;
      success?: BadgeStyleStyles;
      info?: BadgeStyleStyles;
      warning?: BadgeStyleStyles;
    };
    input?: {
      backgroundColor?: string;
      borderColor?: string;
      focusColor?: string;
      invalidBorderColor?: string;
      validBorderColor?: string;
      prefilledTextColor?: string;
    };
    inputGroup?: {
      backgroundColor?: string;
      borderColor?: string;
      focusBorderColor?: string;
    };
    dashboardWidget?: {
      backgroundColor?: string;
      boxShadow?: string;
      default?: DashboardWidgetStyles;
      primary?: DashboardWidgetStyles;
      danger?: DashboardWidgetStyles;
      warning?: DashboardWidgetStyles;
      success?: DashboardWidgetStyles;
      label?: {
        textColor?: string;
      };
    };
    button?: {
      tertiary?: {
        backgroundColor?: string;
        hoverBackgroundColor?: string;
        textColor?: string;
        hoverTextColor?: string;
        activeBackgroundColor?: string;
        outlineBorderColor?: string;
        outlineTextColor?: string;
        outlineHoverTextColor?: string;
        outlineHoverBorderColor?: string;
        outlineActiveBorderColor?: string;
      };
      primary?: {
        backgroundColor?: string;
        hoverBackgroundColor?: string;
        textColor?: string;
        hoverTextColor?: string;
        activeBackgroundColor?: string;
        outlineBorderColor?: string;
        outlineTextColor?: string;
        outlineHoverTextColor?: string;
        outlineHoverBorderColor?: string;
        outlineActiveBorderColor?: string;
      };
      secondary?: {
        backgroundColor?: string;
        hoverBackgroundColor?: string;
        borderColor?: string;
        borderHoverColor?: string;
        textColor?: string;
        hoverTextColor?: string;
        activeBackgroundColor?: string;
        activeBorderColor?: string;
      };
      ghost?: {
        backgroundColor?: string;
        hoverBackgroundColor?: string;
        textColor?: string;
        hoverTextColor?: string;
        activeBackgroundColor?: string;
      };
    };
    card?: {
      backgroundColor?: string;
      boxShadow?: string;
    };
    dropdownMenu?: {
      borderColor?: string;
      backgroundColor?: string;
      boxShadow?: string;
      sectionHeaderColor?: string;
    };
    dropdownMenuItem?: {
      highlightColor?: string;
      selectedColor?: string;
      descriptionFontColor?: string;
    };
    checkbox?: {
      backgroundColor?: string;
      borderColor?: string;
      checkedBackgroundColor?: string;
      checkedColor?: string;
      focusColor?: string;
    };
    radio?: {
      backgroundColor?: string;
      borderColor?: string;
      checkedBackgroundColor?: string;
      checkedBorderColor?: string;
      checkedColor?: string;
      focusColor?: string;
    };
    switch?: {
      backgroundColor?: string;
      boxShadow?: string;
      checkedBackgroundColor?: string;
      toggleBackgroundColor?: string;
      focusColor?: string;
    };
    breadcrumb?: {
      textColor?: string;
      activeTextColor?: string;
      hoverTextColor?: string;
    };
    deadEnds?: {
      backgroundColor?: string;
    };
    datePicker?: {
      dropdownMenuBackgroundColor?: string;
      dropdownMenuTextColor?: string;
      dropdownMenuBoxShadow?: string;
      dateHoverColor?: string;
      dateSelectedColor?: string;
    };
    entryTable?: {
      backgroundColor?: string;
      itemBoxShadowColor?: string;
    };
    clientSwitcher?: {
      rowBoxShadowColor?: string;
      clientIdTextColor?: string;
    };
    slideoutPanel?: {
      backgroundColor?: string;
      boxShadow?: string;
      footerBorderColor?: string;
    };
    modal?: {
      backgroundColor?: string;
      boxShadow?: string;
    };
    pagination?: {
      backgroundColor?: string;
      textColor?: string;
      borderColor?: string;
      activeBackgroundColor?: string;
      activeTextColor?: string;
      activeBorderColor?: string;
      hoverBackgroundColor?: string;
      hoverTextColor?: string;
      disabledBackgroundColor?: string;
      disabledTextColor?: string;
    };
    popover?: {
      backgroundColor?: string;
      boxShadow?: string;
    };
    progressBar?: {
      backgroundColor?: string;
      barBackgroundColor?: string;
      barBorderColor?: string;
    };
    tooltip?: {
      backgroundColor?: string;
      textColor?: string;
    };
    listGroup?: {
      itemBackgroundColor?: string;
      itemHoverBackgroundColor?: string;
      itemActiveBackgroundColor?: string;
    };
    tabs?: {
      textColor?: string;
      activeTextColor?: string;
      activeBorderColor?: string;
      hoverTextColor?: string;
    };
  }
}
