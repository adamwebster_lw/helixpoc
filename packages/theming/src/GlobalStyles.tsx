import { createGlobalStyle } from "styled-components";
import { colors } from "./colors";
export const GlobalStyles = createGlobalStyle`
  body{
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
   
  }
  a{
    color: ${({ theme }) =>
      theme.themeOptions?.primaryColor || theme.colors?.primary};
    text-decoration: none;
    &:hover{
      text-decoration: underline;
    }
  }
  :root {
  // crete css variables for colors
  ${Object.keys(colors).map((color) =>
    typeof colors[color] === "object"
      ? Object.keys(colors[color]).map(
          (shade) => `--${color}-${shade}: ${colors[color][shade]};`
        )
      : `--${color}: ${colors[color]};`
  )}
  }
 `;
