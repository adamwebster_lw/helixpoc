import React, { ReactNode } from "react";
import { ThemeProvider } from "styled-components";
import MainTheme from "./DefaultTheme";
import { GlobalStyles } from "./GlobalStyles";

export interface Props {
  theme?: any;
  children?: ReactNode;
}

export const HelixTheme = React.createContext({});

export const HelixThemeProvider = ({ theme = MainTheme, children }: Props) => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {children}
    </ThemeProvider>
  );
};

export const HelixThemeConsumer = HelixTheme.Consumer;
