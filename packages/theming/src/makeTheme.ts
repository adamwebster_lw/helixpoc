import { DefaultTheme } from "styled-components";
import { ThemeOptions } from "./theme";
import { DefaultThemeOptions } from "./theme-options";
import { colors } from "./colors";

interface MakeThemeProps {
  themeName: string;
  themeOptions?: ThemeOptions;
  defaultTheme?: DefaultTheme;
}
const makeTheme = ({
  themeName = "default",
  themeOptions = DefaultThemeOptions,
}: MakeThemeProps) => {
  return {
    themeName: themeName,
    themeOptions: {
      ...DefaultThemeOptions,
      ...themeOptions,
    },
    baseFontSize: "100%",
    breakPoints: {
      small: "576px",
    },
    fontFamily:
      '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Open Sans", "Ubuntu", "Droid Sans", "Helvetica Neue", sans-serif',
    fontFamilyMonospace:
      'SFMono-Medium, "SF Mono", "Segoe UI Mono", "Roboto Mono", "Ubuntu Mono", Menlo, Courier, monospace',
    colors: { ...colors },
    get alert() {
      return {
        secondary: {
          backgroundColor: this.colors!.gray![50],
          textColor: this.colors!.gray![900],
          borderColor: "transparent",
          iconColor: this.colors!.gray![600],
        },
        success: {
          backgroundColor: this.colors!.green![50],
          textColor: this.colors!.green![900],
          borderColor: "transparent",
          iconColor: this.colors!.green![600],
        },
        warning: {
          backgroundColor: this.colors!.yellow![50],
          textColor: this.colors!.yellow![900],
          borderColor: "transparent",
          iconColor: this.colors!.yellow![600],
        },
        danger: {
          backgroundColor: this.colors!.red![50],
          textColor: this.colors!.red![900],
          borderColor: "transparent",
          iconColor: this.colors!.red![600],
        },
        info: {
          backgroundColor: this.colors!.blue![50],
          textColor: this.colors!.blue![900],
          borderColor: "transparent",
          iconColor: this.colors!.blue![600],
        },
      };
    },
    get backgroundColor() {
      return this.colors!.gray![50];
    },
    get baseTextColor() {
      return this.colors!.gray![900];
    },
    get badge() {
      return {
        default: {
          backgroundColor: this.themeOptions.primaryColor,
          textColor: this.colors!.gray![100],
        },
        inverted: {
          backgroundColor: this.colors!.gray![200],
          textColor: this.colors!.gray![800],
        },
        success: {
          backgroundColor: this.colors!.green![100],
          textColor: this.colors!.green![900],
        },
        warning: {
          backgroundColor: this.colors!.orange![100],
          textColor: this.colors!.yellow![900],
        },
        danger: {
          backgroundColor: this.colors!.red![100],
          textColor: this.colors!.red![900],
        },
        info: {
          backgroundColor: this.colors!["light-blue"]![100],
          textColor: this.colors!["light-blue"]![900],
        },
      };
    },
    get comboBox() {
      return {
        backgroundColor: this.colors!.gray![200],
      };
    },
    get dashboardWidget() {
      return {
        backgroundColor: this.colors!.white,
        boxShadow: "0px 0px 1px rgba(0, 0, 0, 0.36)",
        label: {
          textColor: this.colors!.gray![600],
        },
        default: {
          value: {
            textColor: this.colors!.black,
          },
        },
        primary: {
          value: {
            textColor: this.themeOptions.primaryColor
              ? this.themeOptions.primaryColor
              : this.colors!.primary,
          },
        },
        danger: {
          value: {
            textColor: this.colors!.red![600],
          },
        },
        warning: {
          value: {
            textColor: this.colors!.yellow![600],
          },
        },
        success: {
          value: {
            textColor: this.colors!.green![600],
          },
        },
      };
    },
    get input() {
      return {
        borderColor: this.colors!.gray![300],
        focusColor: this.colors!.blue![500],
        invalidBorderColor: this.colors?.red![500],
        validBorderColor: this.colors?.green![500],
        prefilledTextColor: this.colors?.purple![600],
      };
    },
    get inputGroup() {
      return {
        backgroundColor: this.colors!.white,
        borderColor: this.colors!.gray![300],
        focusBorderColor: this.colors!.blue![500],
      };
    },
    get button() {
      return {
        tertiary: {
          backgroundColor: this.colors!.gray![200],
          hoverBackgroundColor: this.colors?.gray![100],
          textColor: this.colors!.gray![900],
          hoverTextColor: this.colors!.gray![700],
          activeBackgroundColor: this.colors?.gray![300],
          outlineBorderColor: this.colors!.gray![300],
          outlineTextColor: this.colors!.gray![900],
          outlineHoverTextColor: this.colors!.gray![700],
          outlineHoverBorderColor: this.colors!.gray![400],
          outlineActiveBorderColor: this.colors!.gray![500],
        },
        primary: {
          backgroundColor: this.themeOptions?.primaryButtonBackgroundColor,
          hoverBackgroundColor:
            this.themeOptions?.primaryButtonHoverBackgroundColor,
          textColor: this.themeOptions?.primaryButtonTextColor,
          hoverTextColor: this.themeOptions?.primaryButtonHoverTextColor,
          activeBackgroundColor:
            this.themeOptions?.primaryButtonActiveBackgroundColor,
          outlineBorderColor:
            this.themeOptions?.primaryButtonOutlineBorderColor,
          outlineTextColor: this.themeOptions?.primaryButtonOutlineTextColor,
          outlineHoverTextColor:
            this.themeOptions?.primaryButtonOutlineHoverTextColor,
          outlineHoverBorderColor:
            this.themeOptions?.primaryButtonOutlineHoverBorderColor,
          outlineActiveBorderColor:
            this.themeOptions?.primaryButtonOutlineActiveBorderColor,
        },
        secondary: {
          backgroundColor: "transparent",
          hoverBackgroundColor: "transparent",
          borderColor: this.colors?.gray![900],
          borderHoverColor: this.colors?.gray![600],
          textColor: this.colors?.gray![900],
          hoverTextColor: this.colors?.gray![700],
          activeBackgroundColor: this.colors?.gray![100],
        },
        ghost: {
          backgroundColor: "transparent",
          textColor: this.colors?.gray![900],
          activeBackgroundColor: this.colors?.gray![300],
        },
      };
    },
    get card() {
      return {
        backgroundColor: "#fff",
        boxShadow: "0px 0px 1px rgba(0, 0, 0, 0.2)",
      };
    },
    get dropdownMenu() {
      return {
        backgroundColor: "#fff",
        borderColor: this.colors!.gray![300],
        boxShadow:
          "0px 0px 1px rgba(0, 0, 0, 0.2), 0px 4px 8px rgba(0, 0, 0, 0.1)",
        sectionHeaderColor: this.colors!.gray![600],
      };
    },
    get dropdownMenuItem() {
      return {
        highlightColor: this.colors!.gray![100],
        selectedColor: this.colors!.gray![200],
        descriptionFontColor: this.colors!.gray![600],
      };
    },
    get checkbox() {
      return {
        backgroundColor: this.colors?.white,
        borderColor: this.colors?.gray![300],
        checkedBackgroundColor: this.themeOptions.primaryColor
          ? this.themeOptions.primaryColor
          : this.colors!.primary,
        checkColor: this.colors?.white,
        focusColor: this.colors?.gray![500],
      };
    },
    get radio() {
      return {
        backgroundColor: this.colors?.white,
        borderColor: this.colors?.gray![300],
        checkedBackgroundColor: this.themeOptions.primaryColor
          ? this.themeOptions.primaryColor
          : this.colors!.primary,
        checkedBorderColor: this.themeOptions.primaryColor
          ? this.themeOptions.primaryColor
          : this.colors!.primary,
        checkColor: this.colors?.white,
        focusColor: this.colors?.gray![500],
      };
    },
    get switch() {
      return {
        backgroundColor: this.colors?.gray[400],
        boxShadow:
          "inset 0px 0px 1px rgba(0, 0, 0, 0.2), inset 0px 1px 2px rgba(0, 0, 0, 0.1);",
        checkedBackgroundColor: this.colors?.gray![900],
        toggleBackgroundColor: this.colors?.white,
        focusColor: this.colors?.gray![400],
      };
    },
    get clientSwitcher() {
      return {
        rowBoxShadowColor: "rgba(0, 0, 0, 0.05)",
        clientIdTextColor: this.colors?.gray![600],
      };
    },
    get slideoutPanel() {
      return {
        backgroundColor: this.colors?.white,
        boxShadow:
          "0px 0px 1px rgba(0, 0, 0, 0.2), 0px 4px 8px rgba(0, 0, 0, 0.1)",
        footerBorderColor: this.colors.gray![300],
      };
    },
    get modal() {
      return {
        backgroundColor: this.colors?.white,
        boxShadow:
          "0px 0px 1px rgba(0, 0, 0, 0.2), 0px 12px 24px rgba(0, 0, 0, 0.1)",
      };
    },
    get breadcrumb() {
      return {
        textColor: this.colors?.gray![600],
        activeTextColor: this.colors?.blue![600],
        hoverTextColor: this.colors?.blue![700],
      };
    },
    get toast() {
      return {
        boxShadow:
          " 0px 0px 1px rgba(0, 0, 0, 0.2), 0px 4px 8px rgba(0, 0, 0, 0.1);",
        secondary: {
          backgroundColor: this.colors!.gray![50],
          textColor: this.colors!.gray![900],
          iconColor: this.colors!.gray![600],
        },
        success: {
          backgroundColor: this.colors!.green![700],
          textColor: this.colors!.white,
          iconColor: this.colors!.white,
        },
        warning: {
          backgroundColor: this.colors!.yellow![700],
          textColor: this.colors!.white,
          iconColor: this.colors!.white,
        },
        danger: {
          backgroundColor: this.colors!.red![700],
          textColor: this.colors!.white,
          iconColor: this.colors!.white,
        },
        info: {
          backgroundColor: this.colors!.blue![700],
          textColor: this.colors!.white,
          iconColor: this.colors!.white,
        },
      };
    },
    get datePicker() {
      return {
        dropdownMenuBackgroundColor: this.colors?.white,
        dropdownMenuTextColor: this.colors?.gray![900],
        dropdownMenuBoxShadow:
          "0px 0px 1px rgba(0, 0, 0, 0.2), 0px 4px 8px rgba(0, 0, 0, 0.1)",
        dateHoverColor: this.colors?.gray![100],
        dateSelectedColor: this.colors?.gray![900],
      };
    },
    get pagination() {
      return {
        backgroundColor: this.colors?.white,
        borderColor: this.colors?.gray![200],
        textColor: this.colors?.gray![600],
        hoverBackgroundColor: this.colors?.gray![100],
        hoverTextColor: this.colors?.gray![600],
        activeBackgroundColor: this.colors?.blue![50],
        activeTextColor: this.colors?.blue![600],
        activeBorderColor: this.colors?.blue![600],
        disabledBackgroundColor: this.colors?.gray![200],
        disabledTextColor: this.colors?.gray![300],
      };
    },
    get popover() {
      return {
        backgroundColor: this.colors?.white,
        boxShadow:
          "0px 0px 1px rgba(0, 0, 0, 0.2), 0px 4px 8px rgba(0, 0, 0, 0.1)",
      };
    },
    get progressBar() {
      return {
        backgroundColor: this.colors?.gray![200],
        barBackgroundColor: this.themeOptions.primaryColor
          ? this.themeOptions.primaryColor
          : this.colors!.primary,
      };
    },
    get tooltip() {
      return {
        backgroundColor: "rgba(17, 24, 39, 0.8)",
        textColor: this.colors?.white,
      };
    },
    get listGroup() {
      return {
        itemBackgroundColor: this.colors?.white,
        itemHoverBackgroundColor: this.colors?.gray![100],
        itemActiveBackgroundColor: this.colors?.gray![200],
      };
    },
    get tabs() {
      return {
        textColor: this.colors?.gray![600],
        activeTextColor: this.themeOptions.primaryColor ? this.themeOptions.primaryColor : this.colors!.primary,
        hoverTextColor: this.colors?.gray![800],
        activeBorderColor: this.themeOptions.primaryColor ? this.themeOptions.primaryColor : this.colors!.primary,
      }
    },
  };
};

export default makeTheme;
