import {
  Body,
  DisplayXLarge,
  DisplayXSmall,
} from "@lwt-helix-nextgen/typography";
import React from "react";
import Jumbotron from "./src/jumbotron";
import { Button } from "@lwt-helix-nextgen/button";

export default {
  title: "Components/Jumbotron",
  component: Jumbotron,
};

const Template = (args: any) => (
  <Jumbotron {...args}>
    <DisplayXLarge as="h2">Hello, world.</DisplayXLarge>
    <DisplayXSmall as="h3">
      This is a simple hero unit, a simple Jumbotron-style component for calling
      extra attention to featured content or information.
    </DisplayXSmall>
    <Body as="p">
      It uses utility classes for typography and spacing to space content out
      within the larger container.
    </Body>
    <Button color="primary">Go back</Button>
  </Jumbotron>
);

export const Default = Template.bind({});
