import React, { ReactNode, HTMLAttributes } from "react";
import { StyledJumbotron } from "./jumbotron.styles";

export interface JumbotronProps extends HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const Jumbotron = ({
  children,
  dataLwtId,
  className,
  ...rest
}: JumbotronProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledJumbotron
      className={`lwt-helix-jumbotron ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledJumbotron>
  );
};

Jumbotron.displayName = "Jumbotron";

export default Jumbotron;
