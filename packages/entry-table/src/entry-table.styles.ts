import styled, { css } from 'styled-components';

interface StyledEntryTableProps {
  showItemBorder?: boolean;
}

export const StyledEntryTableItem = styled.div`
  padding: 8px 12px;
`;

export const StyledEntryTable = styled.div<StyledEntryTableProps>`
  background-color: ${({ theme }) => theme.entryTable?.backgroundColor};
  border-radius: 6px;
  padding: 24px;
  box-sizing: border-box;
  ${({ showItemBorder }) =>
    showItemBorder &&
    css`
      ${StyledEntryTableItem} {
        box-shadow: inset 0px 1px 0px ${({ theme }) => theme.entryTable?.itemBoxShadowColor};
        &:last-child {
          box-shadow: inset 0px -1px 0px ${({ theme }) => theme.entryTable?.itemBoxShadowColor},
            inset 0px 1px 0px ${({ theme }) => theme.entryTable?.itemBoxShadowColor};
        }
      }
    `}
`;
