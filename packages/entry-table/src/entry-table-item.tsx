import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledEntryTableItem } from "./entry-table.styles";

interface EntryTableItemProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const EntryTableItem = ({
  children,
  className,
  dataLwtId,
  ...rest
}: EntryTableItemProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledEntryTableItem
      className={`lwt-helix-entry-table-item ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledEntryTableItem>
  );
};

EntryTableItem.displayName = "EntryTableItem";

export default EntryTableItem;
