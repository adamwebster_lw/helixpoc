import React, {
  Children,
  HtmlHTMLAttributes,
  InputHTMLAttributes,
  ReactNode,
} from "react";
import { StyledEntryTable } from "./entry-table.styles";

interface EntryTableProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Shows a border above and below the item */
  showItemBorder?: boolean;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}

const EntryTable = ({
  children,
  showItemBorder = false,
  dataLwtId,
  className,
  ...rest
}: EntryTableProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledEntryTable
      className={`lwt-helix-entry-table ${className ? className : ""}`}
      showItemBorder={showItemBorder}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledEntryTable>
  );
};

EntryTable.displayName = "EntryTable";

export default EntryTable;
