import React from 'react';
import EntryTable from './src/entry-table';
import EntryTableItem from './src/entry-table-item';
import { Heading, DisplaySmall, SubHeading, Body } from '@lwt-helix-nextgen/typography';
export default {
  title: 'Components/Entry Table',
  component: EntryTable,
};

const Template = (args: any) => (
  <EntryTable {...args}>
    <DisplaySmall as="h2">Transaction Details</DisplaySmall>
    {[...Array(3)].map((item) => (
      <EntryTableItem>
        <Heading as="div"> Number: 45921</Heading>
        <SubHeading as="div">Entered by: Thom White</SubHeading>
        <Body as="p">
          Description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id eros arcu. Nullam varius elit
          tempus sem eleifend ullamcorper. Phasellus egestas, diam eu tempor maximus, purus lacus dignissim felis, et
          interdum orci libero a est. Etiam euismod felis dolor. Sed egestas rhoncus imperdiet. Maecenas eu arcu
          interdum odio cursus feugiat. Aenean malesuada in eros in volutpat. Phasellus porttitor erat vestibulum tortor
          dignissim, quis vestibulum dolor cursus. Cras sed massa vitae metus pharetra dictum sed et orci. Morbi pretium
          hendrerit tortor a egestas. Quisque ac venenatis lectus, eget posuere est.
        </Body>
      </EntryTableItem>
    ))}
  </EntryTable>
);

export const Default = Template.bind({});
