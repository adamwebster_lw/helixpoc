import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledPaginationItem } from "./pagination.styles";

interface PaginationItemProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** Active state */
  active?: boolean;
  /** Disabled state */
  disabled?: boolean;
  /** Icon item */
  iconItem?: boolean;
  /** Click event */
  onClick?: () => void;
}

const PaginationItem = ({
  children,
  active,
  disabled,
  onClick,
  iconItem,
  dataLwtId,
  className,
  ...rest
}: PaginationItemProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }

  return (
    <StyledPaginationItem
      className={`lwt-helix-pagination-item ${className ? className : ""}`}
      iconItem={iconItem}
      active={active}
      disabled={disabled}
      onClick={onClick}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledPaginationItem>
  );
};

PaginationItem.displayName = "PaginationItem";

export default PaginationItem;
