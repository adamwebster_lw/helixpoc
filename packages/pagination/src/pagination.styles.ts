import styled, { css } from "styled-components";

export const StyledPagination = styled.div`
  display: flex;
`;

interface StyledPaginationItemProps {
  active?: boolean;
  disabled?: boolean;
  iconItem?: boolean;
}
export const StyledPaginationItem = styled.div<StyledPaginationItemProps>`
  background-color: ${({ theme }) => theme.pagination?.backgroundColor};
  border-top: solid 1px ${({ theme }) => theme.pagination?.borderColor};
  border-bottom: solid 1px ${({ theme }) => theme.pagination?.borderColor};
  border-right: solid 1px ${({ theme }) => theme.pagination?.borderColor};
  :first-child {
    border-left: solid 1px ${({ theme }) => theme.pagination?.borderColor};
    border-radius: 4px 0px 0px 4px;
  }

  :last-child {
    border-radius: 0px 4px 4px 0px;
  }
  display: flex;
  min-width: 35px;
  min-height: 38px;
  align-items: center;
  justify-content: center;
  color: ${({ theme }) => theme.pagination?.textColor};
  a {
    color: ${({ theme }) => theme.pagination?.textColor};
    text-decoration: none;
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1 1;
    width: 100%;
    height: 100%;
  }
  ${({ active, theme }) =>
    active &&
    css`
      background-color: ${theme.pagination?.activeBackgroundColor};
      color: ${theme.pagination?.activeTextColor};
      border-color: ${theme.pagination?.activeBorderColor};
      border-left: solid 1px ${theme.pagination?.activeBorderColor};
      a {
        color: ${theme.pagination?.activeTextColor};
        cursor: default;
      }
    `}
  ${({ disabled, theme }) =>
    disabled &&
    css`
      background-color: ${theme.pagination?.disabledBackgroundColor};
      color: ${theme.pagination?.disabledTextColor};
    `}
    ${({ active, disabled }) =>
    !active &&
    !disabled &&
    css`
    &:hover:not(:disabled) {
    cursor: pointer;
    background-color: ${({ theme }) => theme.pagination?.hoverBackgroundColor};
    color: ${({ theme }) => theme.pagination?.hoverTextColor};
    `}
`;
