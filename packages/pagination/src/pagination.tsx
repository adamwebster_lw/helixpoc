import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledPagination } from "./pagination.styles";
interface PaginationProps extends HtmlHTMLAttributes<HTMLDivElement> {
  children?: ReactNode;
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
}
const Pagination = ({
  children,
  className,
  dataLwtId,
  ...rest
}: PaginationProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledPagination
      className={`lwt-helix-pagination ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledPagination>
  );
};

Pagination.displayName = "Pagination";
export default Pagination;
