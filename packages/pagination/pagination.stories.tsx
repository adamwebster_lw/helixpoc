import React from "react";
import Pagination from "./src/pagination";
import PaginationItem from "./src/pagination-item";
import { HelixIcon } from "@lwt-helix/helix-icon";
import {
  chevron_small_right,
  chevron_small_left,
} from "@lwt-helix/helix-icon/outlined";
export default {
  title: "Components/Pagination",
  component: Pagination,
};

const Template = (args: any) => (
  <>
    <Pagination {...args}>
      <PaginationItem disabled iconItem>
        <HelixIcon icon={chevron_small_left} />
      </PaginationItem>
      <PaginationItem active>
        <a href="#">1</a>
      </PaginationItem>
      <PaginationItem>
        <a href="#">2</a>
        </PaginationItem>
      <PaginationItem>
        <a href="#">3</a>
      </PaginationItem>
      <PaginationItem>
        <a href="#">4</a>
      </PaginationItem>
      <PaginationItem>
        <a href="#">5</a>
      </PaginationItem>
      <PaginationItem iconItem>
        <HelixIcon icon={chevron_small_right} />
      </PaginationItem>
    </Pagination>
  </>
);

export const Default = Template.bind({});
