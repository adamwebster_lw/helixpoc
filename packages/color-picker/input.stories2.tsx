import React from "react";
import ColorPicker from "./src/color-picker";

export default {
  title: "Components/Color Picker",
  component: ColorPicker,
};

const Template = (args: any) => (
  <>
    <ColorPicker {...args} />
  </>
);

export const Default = Template.bind({});

Default.args = {};
