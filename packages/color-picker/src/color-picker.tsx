import React, { InputHTMLAttributes } from "react";
import { StyledInput } from "./color-picker.styles";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  /** if the checkbox is disabled or not */
  disabled?: boolean;
  invalid?: boolean;
  inputSize?: "medium" | "large";
  valid?: boolean;
  prefilled?: boolean;
  dataLwtId: string;
}
const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (
    {
      disabled,
      dataLwtId,
      invalid,
      valid,
      prefilled,
      inputSize = "medium",
      ...rest
    },
    ref
  ) => {
    return (
      <StyledInput
        disabled={disabled}
        prefilled={prefilled}
        data-lwt-id={dataLwtId}
        ref={ref}
        inputSize={inputSize}
        invalid={invalid}
        valid={valid}
        {...rest}
      />
    );
  }
);

Input.displayName = "Input";
export default Input;
