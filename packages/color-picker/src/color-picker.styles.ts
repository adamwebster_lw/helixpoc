import styled, { css } from 'styled-components';

interface StyledInputProps {
  invalid?: boolean;
  valid?: boolean;
  prefilled?: boolean;
  inputSize?: 'medium' | 'large';
}
export const StyledInput = styled.input<StyledInputProps>`
  background-color: ${({ theme }) => theme.input?.backgroundColor};
  padding: 8px 12px;
  border-radius: 6px;
  border: solid 1px ${({ theme }) => theme.input?.borderColor};
  font-size: 14px;
  &:focus {
    outline: solid 2px ${({ theme }) => theme.input?.focusColor};
  }
  ${({ invalid }) =>
    invalid &&
    css`
      border: solid 1px ${({ theme }) => theme.input?.invalidBorderColor};
      &:focus {
        outline: solid 1px ${({ theme }) => theme.input?.invalidBorderColor};
      }
    `}
  ${({ valid }) =>
    valid &&
    css`
      border: solid 1px ${({ theme }) => theme.input?.validBorderColor};
      &:focus {
        outline: solid 1px ${({ theme }) => theme.input?.validBorderColor};
      }
    `}

    ${({ prefilled }) =>
    prefilled &&
    css`
      color: ${({ theme }) => theme.input?.prefilledTextColor};
    `}

    ${({ inputSize }) =>
    inputSize === 'large' &&
    css`
      padding: 13px 12px;
      font-size: 16px;
    `}
`;
