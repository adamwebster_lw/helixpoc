import React from "react";
import InputGroup from "./src/input-group";
import { Story } from "@storybook/react";
import InputGroupAddon from "./src/input-group-addon";
import { Input } from "@lwt-helix-nextgen/input";
export default {
  title: "Components/Input Group",
  component: InputGroup,
};

const Template = (args: any) => (
  <>
    <InputGroup {...args}>
      <InputGroupAddon>$</InputGroupAddon>
      <Input />
    </InputGroup>
  </>
);

export const Default: Story = Template.bind({});

Default.args = {};

export const Append: Story = () => {
  return (
    <InputGroup>
      <Input />
      <InputGroupAddon>%</InputGroupAddon>
    </InputGroup>
  );
};
