import React, { HtmlHTMLAttributes, ReactNode } from "react";
import { StyledInputGroupAddon } from "./input-group.styles";

export interface InputGroupAddonProps
  extends HtmlHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  dataLwtId?: string;
}
const InputGroupAddon = ({
  children,
  className,
  dataLwtId,
  ...rest
}: InputGroupAddonProps) => {
  return (
    <StyledInputGroupAddon
      data-lwt-id={dataLwtId}
      className={`lwt-input-group-addon ${className ? className : ""}`}
      {...rest}
    >
      {children}
    </StyledInputGroupAddon>
  );
};

InputGroupAddon.displayName = "InputGroupAddon";

export default InputGroupAddon;
