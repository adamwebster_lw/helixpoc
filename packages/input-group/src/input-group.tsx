import React, { ReactNode, InputHTMLAttributes } from "react";
import { StyledInputGroup } from "./input-group.styles";

export interface InputGroupProps extends InputHTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  dataLwtId?: string;
}

const InputGroup = ({
  children,
  dataLwtId,
  className,
  ...rest
}: InputGroupProps) => {
  const optionalProps = {};
  if (dataLwtId) {
    optionalProps["data-lwt-id"] = dataLwtId;
  }
  return (
    <StyledInputGroup
      className={`helix-input-group ${className ? className : ""}`}
      {...optionalProps}
      {...rest}
    >
      {children}
    </StyledInputGroup>
  );
};

InputGroup.displayName = "InputGroup";

export default InputGroup;
