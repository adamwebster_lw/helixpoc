import styled, { css } from "styled-components";

export const StyledInputGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  min-height: 32px;
  border: 1px solid ${({ theme }) => theme.inputGroup?.borderColor};
  border-radius: 4px;
  background-color: ${({ theme }) => theme.inputGroup?.backgroundColor};
  box-sizing: border-box;
  transition: border-color 0.2s ease-in-out;
  input {
    flex: 1;
    border: none;
    border-radius: 0;
    &:focus {
      outline: none;
    }
  }
  &:focus-within {
    outline: solid 2px ${({ theme }) => theme.inputGroup?.focusBorderColor};
  }
`;

export const StyledInputGroupAddon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 32px;
  padding: 0 16px;
  box-sizing: border-box;
  border-right: 1px solid ${({ theme }) => theme.inputGroup?.borderColor};
  &:last-child {
    border-right: none;
    border-left: solid 1px ${({ theme }) => theme.inputGroup?.borderColor};
  }
`;
