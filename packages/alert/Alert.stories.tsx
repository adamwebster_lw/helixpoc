import React, { useRef } from "react";

import { Alert, AlertAPIProps } from "./src/Alert";

export default {
  title: "Components/Alert",
  component: Alert,
};

const Template = (args: any) => <Alert {...args}>Hello</Alert>;

export const Default: any = Template.bind({});

Default.args = {
  labelText: "Label",
};

export const ExternalDismiss: any = (args) => {
  const alertRef = useRef<AlertAPIProps>(null);
  return (
    <>
      <button onClick={() => alertRef.current?.api.dismiss()}>Dismiss</button>
      <Alert ref={alertRef} {...args}>
        Hello
      </Alert>
    </>
  );
};
