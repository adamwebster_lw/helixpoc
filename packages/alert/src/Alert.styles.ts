import styled, { css } from "styled-components";

interface StyledAlertProps {
  type: "secondary" | "success" | "warning" | "danger" | "info";
  dismissing?: boolean;
}

export const StyledAlert = styled.div<StyledAlertProps>`
  display: flex;
  align-items: center;
  opacity: 1;
  ${({ type, theme }) => css`
    background-color: ${theme?.alert![type]?.backgroundColor};
    color: ${theme?.alert![type]?.textColor}!important;
    border-color: ${theme?.alert![type]?.borderColor}!important;
  `}
  justify-content: center;
  padding: 18px;
  .alert__label-text {
    font-weight: 600;
  }
  .material-icons {
    margin-right: 18px;
  }

  .helix-alert__content {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
  }

  .helix-alert__close {
    margin-left: auto;
    cursor: pointer;
    background-color: transparent;
    border: none;
    -webkit-appearance: none;
  }

  transition: opacity 0.3s ease-in-out;

  ${({ dismissing }) =>
    dismissing &&
    css`
      opacity: 0;
    `}
`;
