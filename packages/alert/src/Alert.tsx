import React, {
  HtmlHTMLAttributes,
  useState,
  useEffect,
  forwardRef,
  ReactNode,
  useImperativeHandle,
} from "react";
import { HelixIcon } from "@lwt-helix/helix-icon";
import { x } from "@lwt-helix/helix-icon/outlined";
import { StyledAlert } from "./Alert.styles";

export interface AlertAPIProps {
  api: {
    dismiss: () => void;
  };
}

export interface AlertProps extends HtmlHTMLAttributes<HTMLDivElement> {
  /** The type of alert */
  type: "secondary" | "success" | "warning" | "danger" | "info";
  /** Custom attribute for QA 'data-lwt-id' */
  dataLwtId?: string;
  /** The label for the alert */
  labelText?: string;
  /** If the alert is dismissible set this to true */
  dismissible?: boolean;
  /** The callback function for when the alert is dismissed */
  onDismiss?: () => void;
  /** if the alert is open */
  isOpen?: boolean;
  children?: ReactNode;
}

export const Alert = forwardRef<AlertAPIProps, AlertProps>(
  (
    {
      type = "secondary",
      dataLwtId,
      labelText,
      children,
      className,
      dismissible = true,
      onDismiss,
      isOpen = true,
      ...rest
    },
    ref
  ) => {
    const [show, setShow] = useState(isOpen);
    const [dismissing, setDismissing] = useState(false);
    const optionalProps = {};
    if (dataLwtId) {
      optionalProps["data-lwt-id"] = dataLwtId;
    }

    const dismiss = () => {
      setDismissing(true);
      setTimeout(() => {
        setShow(false);
      }, 300);
      if (onDismiss) {
        onDismiss();
      }
    };

    useImperativeHandle(ref, () => ({
      api: {
        dismiss,
      },
    }));

    useEffect(() => {
      setShow(isOpen);
    }, [isOpen]);

    return (
      <>
        {show && (
          <StyledAlert
            type={type}
            dismissing={dismissing}
            className={`helix-alert ${className ? className : ""}`}
            {...optionalProps}
            {...rest}
          >
            <div className="helix-alert__content">
              {labelText && (
                <span className="alert__label-text">{labelText}: &nbsp;</span>
              )}

              {children}
            </div>
            {dismissible && (
              <button
                className="helix-alert__close"
                onClick={dismiss}
                aria-label="Close"
              >
                <HelixIcon icon={x} />
              </button>
            )}
          </StyledAlert>
        )}
      </>
    );
  }
);

export default Alert;
