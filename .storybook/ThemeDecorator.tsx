import React, { useEffect, useState } from "react";
import {
  HelixThemeProvider,
  MainTheme,
  RemaxTheme,
} from "@lwt-helix-nextgen/theming";

const ThemeDecorator = (StoryFn, context) => {
  const [theme, setTheme] = useState(MainTheme);
  const themeName = context.parameters.themes || context.globals.themes;
  useEffect(() => {
    if (themeName === "remax") {
      setTheme(RemaxTheme);
    } else {
      setTheme(MainTheme);
    }
  }, [themeName]);
  return <HelixThemeProvider theme={theme}>{StoryFn()}</HelixThemeProvider>;
};

export default ThemeDecorator;
