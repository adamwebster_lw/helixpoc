# Helix POC

This is a POC of the next major version of helix. This POC removes all dependency on Reactstrap and uses styled-components for the styling and theming of the components.

## Getting Started

1. Run `yarn install` in the root directory
2. Run `yarn storybook` in the root directory

## Building a Component 

1. Go to the directory you would like of the component you would like to build for example `cd packages/modal`
2. Run `yarn build`

## Questions or comments

For any questions or comments about the Helix POC feel free to contact Adam Webster on teams or through email  at awebster@lwolf.com.

## Contributing

To contribute to the project contact Adam Webster through teams or through email at awebster@lwolf.com

